<?php
/*
Template Name: Test Eduprize Schools Product
*/

get_header();

$style = "";
if (!Intro(get_the_title()))
{
	$style = "style='margin-top: 0px;'";
}
?><style>.woocommerce .quantity, .woocommerce #content .quantity, .woocommerce-page .quantity, .woocommerce-page #content .quantity{float:left!important;}.woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page a.button.alt, .woocommerce-page button.button.alt, .woocommerce-page input.button.alt, .woocommerce-page #respond input#submit.alt, .woocommerce-page #content input.button.alt {    margin-left: 10px;}</style>

	<div class="container">
        <div id="main" <?php echo $style; ?>>
			<div class="row">
				<div class="span12">
				<table>
<tr>
<td>
				
				<?php 
				if (have_posts()) 
				{ 
					while (have_posts()) 
					{ 
						the_post(); 
				?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
						<?php echo RemoveLastMarginOfFullWidthPages(_get_the_content()); ?>
					</div>
				<?php
					}
				}
				comments_template('', true);
				?>		
</td>
	</tr>
	</table>
				
				</div>
			</div>
		</div>
	</div>



<ul>
<?php
debug_print_backtrace();

$included_files = get_included_files();

foreach ($included_files as $filename) {
    echo "<li>$filename\n</li>";
}

?>
<!-- </ul>	 -->
	
	
<?php get_footer(); ?>