<?php get_header(); ?>

<?php
	if (have_posts()){ 
		$pageTitle    = '';
		$post = $posts[0]; // Hack. Set $post so that the_date() works.
		
		if (is_category()) {
			$pageTitle = sprintf(__('All posts in %s', TEXTDOMAIN), single_cat_title('',false));
		}
		elseif( is_tag() ){
			$pageTitle = sprintf(__('All posts tagged %s', TEXTDOMAIN), single_tag_title('',false));
		}
		elseif( is_day() ){
			$pageTitle = sprintf(__('Archive for %s', TEXTDOMAIN), get_the_time('F jS, Y'));
		}
		elseif( is_month() ){
			$pageTitle = sprintf(__('Archive for %s', TEXTDOMAIN), get_the_time('F, Y'));
		}
		elseif ( is_year() ){
			$pageTitle = sprintf(__('Archive for %s', TEXTDOMAIN), get_the_time('Y') );
		}
		elseif ( is_author() ){
			/* Get author data */
			if(get_query_var('author_name'))
				$curauth = get_user_by('login', get_query_var('author_name'));
			else
				$curauth = get_userdata(get_query_var('author'));

			$pageTitle = sprintf(__('Posts by %s', TEXTDOMAIN), $curauth->display_name );
		}
		elseif ( isset( $_GET['paged'] ) && !empty( $_GET['paged'] ) ){
			$pageTitle = __('Blog Archives', TEXTDOMAIN);
		}
	
		$style = "";
		if (!Intro($pageTitle, NULL, true))
		{
			$style = "style='margin-top: 0px;'";
		}
	}//if have_posts() ?>

	<div class="container">
		<!--Content-->
        <div id="main" <?php echo $style; ?>>
			<div class="row">
				<?php 
					$blogClass = 'span8';
					
					if(opt('blog_sidebar_position') == 0)
						$blogClass = 'span12';
					if(opt('blog_sidebar_position') == 1)
						$blogClass .= ' blog_right';

					
					if(isset($_GET['type']) && $_GET['type']=='event'){
					DisplayCurrentBlogPostsEvents($blogClass);
					}else{
					DisplayCurrentBlogPosts($blogClass);
					}
					
					

					if(opt('blog_sidebar_position') != 0)
						get_sidebar(); 
				?>
			</div>			
		</div>
        <!--End Content-->
	</div>	
    
<?php get_footer(); ?>