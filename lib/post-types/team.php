<?php

require_once('post-type.php');

class Team extends PostType
{

    function __construct()
    {
        parent::__construct('team');
    }

    function CreatePostType()
    {
	    $p = new JesGS_PostType(array(
	        'name'       => $this->postType,
	        'singlename' => __( 'Team Member', TEXTDOMAIN ),
	        'pluralname' => __( 'Team Members', TEXTDOMAIN ),
	        'arguments'  => array(
				'public' => true,
				'capability_type' => 'post',
				'has_archive' => true,
				'hierarchical' => false,
				'menu_icon' => THEME_IMAGES_URI . '/teammembers.png',
	            'supports' => array('title', 'editor', 'thumbnail'),           
	    		'rewrite' => array('slug' => $this->postType),
	        ),
	    ));
	    
	    new JesGS_Taxonomy(array(
			'name' => 'job-title',
	        'singlename' =>  __( "Job Title",  TEXTDOMAIN ), 
	        'pluralname' =>  __( "Job Titles",  TEXTDOMAIN ), 
	        'objects' => array($this->postType),
	    	'hierarchical' => true,
	        'arguments' => array(
				'label' => __( "Job Titles", TEXTDOMAIN ), 
	    		'rewrite' => array('slug' => 'job-title', 'hierarchical' => true)
			)    
	    ));
    }

    /*function RegisterScripts()
    {
        wp_register_script('portfolio', THEME_LIB_URI . '/post-types/js/portfolio.js', array('jquery'), THEME_VERSION);

        parent::RegisterScripts();
    }*/

    function EnqueueScripts()
    {
        //wp_enqueue_script('hoverIntent');
        wp_enqueue_script('jquery-easing');

        wp_enqueue_style('theme-admin');
        wp_enqueue_script('theme-admin');

        //wp_enqueue_script('portfolio');
    }

    /*function OnProcessFieldForStore($post_id, $key, $settings)
    {
		$images = $_POST["image"];

        //Filter results
        $images = array_filter( array_map( 'trim', $images ), 'strlen' );
        //ReIndex
        $images = array_values($images);

        update_post_meta( $post_id, "image", $images );
    	
        return true;
    }*/

    protected function GetOptions()
    {
        $fields = array(
			'social_facebook_address' => array(
				'type'  => 'text',
        		'placeholder'  => __('Facebook Address', TEXTDOMAIN),
			),
			"social_twitter_address" => array(
				'placeholder'  => __('Twitter Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_dribbble_address" => array(
				'placeholder'  => __('Dribbble Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_vimeo_address" => array(
				'placeholder'  => __('Vimeo Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_youtube_address" => array(
				'placeholder'  => __('Youtube Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_googleplus_address" => array(
				'placeholder'  => __('Googleplus Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_digg_address" => array(
				'placeholder'  => __('Digg Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_tumblr_address" => array(
				'placeholder'  => __('Tumblr Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_linkedin_address" => array(
				'placeholder'  => __('Linkedin Address',TEXTDOMAIN),	
				'type'  => 'text',
			),
			"social_forrst_address" => array(
				'placeholder'  => __('Forrst Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"rss_feed_address" => array(
				'placeholder'  => __('Rss feed Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_lastfm_address" => array(
				'placeholder'  => __('Lastfm Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_flickr_address" => array(
				'placeholder'  => __('Flickr Address',TEXTDOMAIN),
				'type'  => 'text',
			),
			"social_myspace_address" => array(
				'placeholder'  => __('Myspace Address',TEXTDOMAIN),
				'type'  => 'text',
			),			
            
        );

        //Option sections
        $options = array(
            'social_network_address' => array(
                'title'   => __('Social Network Links', TEXTDOMAIN),
                'tooltip' => __('...', TEXTDOMAIN),
                'fields'  => array(
					'social_facebook_address' => $fields['social_facebook_address'],
					"social_twitter_address" => $fields["social_twitter_address"],
					"social_dribbble_address" => $fields["social_dribbble_address"],
					"social_vimeo_address" => $fields["social_vimeo_address"],
					"social_youtube_address" => $fields["social_youtube_address"],
					"social_googleplus_address" => $fields["social_googleplus_address"],
					"social_digg_address" => $fields["social_digg_address"],
					"social_tumblr_address" => $fields["social_tumblr_address"],
					"social_linkedin_address" => $fields["social_linkedin_address"],
					"social_forrst_address" => $fields["social_forrst_address"],
					"rss_feed_address" => $fields["rss_feed_address"],
					"social_lastfm_address" => $fields["social_lastfm_address"],
					"social_flickr_address" => $fields["social_flickr_address"],
					"social_myspace_address" => $fields["social_myspace_address"],
                )
            ),//images sec
        );

        return array(
            array(
                'id' => 'team_meta_box',
                'title' => __('Team Member Options', TEXTDOMAIN),
                'context' => 'normal',
                'priority' => 'default',
                'options' => $options,
            )//Meta box
        );
    }
}

new Team();