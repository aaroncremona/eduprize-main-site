<?php wp_nonce_field( 'theme-post-meta-form', THEME_NAME_SEO . '_post_nonce' ); ?>

<div id="px-container" class="post-meta">
    <div id="px-main">
        <?php
            $this->SetWorkingDirectory(path_combine(THEME_LIB, 'forms/templates'));
            echo $this->GetTemplate('section.php', $vars);
        ?>
    </div>
</div>
