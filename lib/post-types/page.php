<?php

require_once('post-type.php');

class PageMeta extends PostType
{
    private $metaKeys = array(
    	'intro'=> array('show_intro'),
    	'sidebar'=> array('sidebar'),
        'image'=> array('about_page_title', 'about_page_logos'),
        'portfolio'=> array('portfolio_categories', 'portfolio_posts_page')
    );

    private $pageFeatures = array(
        'default' => array('intro'/*, 'slider'*/),
        'template-contact.php' => array('intro'),
        'template-full-width.php' => array('intro'),
        'template-home.php' => array('intro'),
        'template-portfolio.php' => array('intro', 'portfolio'),
        'template-portfolio-2.php' => array('intro', 'portfolio'),
    	'template-about.php' => array('image', 'intro'),
    );

    function __construct()
    {
		parent::__construct('page');
    }

    function RegisterScripts()
    {
		wp_enqueue_script('page-meta', THEME_ADMIN_URI  .'/../post-types/js/page-meta.js', array('jquery'), THEME_VERSION);
		wp_register_script('jquery-easing', THEME_JS_URI  .'/jquery.easing.1.3.js', array('jquery'), '1.3.0');
		
		wp_register_style( 'nouislider', THEME_ADMIN_URI . '/css/nouislider.css', false, '2.1.4', 'screen' );
		wp_register_script('nouislider', THEME_ADMIN_URI  .'/scripts/jquery.nouislider.min.js', array('jquery'), '2.1.4');
		//wp_enqueue_script('nouislider', THEME_ADMIN_URI  .'/scripts/jquery.nouislider.js', array('jquery'), '2.1.4');
		
		wp_register_style( 'colorpicker0', THEME_ADMIN_URI . '/css/colorpicker.css', false, '1.0.0', 'screen' );
		wp_register_script('colorpicker0', THEME_ADMIN_URI  .'/scripts/colorpicker.js', array('jquery'), '1.0.0');
		
		wp_register_style( 'chosen', THEME_ADMIN_URI . '/css/chosen.css', false, '1.0.0', 'screen' );
		wp_register_script('chosen', THEME_ADMIN_URI  .'/scripts/chosen.jquery.min.js', array('jquery'), '1.0.0');
		
		wp_register_style( 'theme-admin-css', THEME_ADMIN_URI . '/css/style.css', false, '1.0.0', 'screen' );
		wp_register_script('theme-admin-script', THEME_ADMIN_URI  .'/scripts/admin.js', array('jquery'), '1.0.0');
    	
        parent::RegisterScripts();
    }

    function EnqueueScripts()
    {
		wp_enqueue_script('jquery');  
		wp_enqueue_script('thickbox');  	
		
		wp_enqueue_style('thickbox');  
		wp_enqueue_script('media-upload');
		//wp_enqueue_script('hoverIntent');
		wp_enqueue_script('jquery-easing');
		wp_enqueue_style('nouislider');
		wp_enqueue_script('nouislider');
		wp_enqueue_style('colorpicker0');
		wp_enqueue_script('colorpicker0');
		wp_enqueue_style('chosen');
		wp_enqueue_script('chosen');
		wp_enqueue_style('theme-admin-css');
		wp_enqueue_script('theme-admin-script');
    	
		/*wp_enqueue_script('hoverIntent');
        wp_enqueue_script('jquery-easing');

        wp_enqueue_style('theme-admin');
        wp_enqueue_script('theme-admin');*/

        wp_enqueue_script('page-meta');
    }

    function OnProcessFieldForStore($post_id, $key, $settings)
    {
        $template = $_POST['page_template'];
        $features = array();

        if(array_key_exists($template, $this->pageFeatures))
            $features = $this->pageFeatures[$template];
            
        $arrLegal = array();
        foreach($features as $feature)
        {
            $arrLegal = array_merge($arrLegal, $this->metaKeys[$feature]);
        }
            
        $isLegal = in_array($key, $arrLegal);
        if ($isLegal)
        {
        	if ($key != "image")
        	{
        		return false;
        	}
        }
        	
    	if (!$isLegal)
    	{
    		delete_post_meta($post_id, $key);
    	}
    	else if ($key == "image")
    	{
			$images = $_POST["image"];
	
	        //Filter results
	        $images = array_filter( array_map( 'trim', $images ), 'strlen' );
	        //ReIndex
	        $images = array_values($images);
	
	        update_post_meta( $post_id, "image", $images );
    	}
        
        return true;
    }	
	
    protected function GetOptions()
    {
        $fields = array(
            'portfolio_posts_page' => array(
                'type' => 'text',
            	'placeholder'	=>__('Posts Per Page', TEXTDOMAIN),
            ),
            'portfolio_categories' => array(
                'type' => 'multi-select',
                'class'	=> 'chosen',
            	'label'	=>__('Show Categories', TEXTDOMAIN),
                'placeholder' => __('Choose a Category', TEXTDOMAIN),
                'options' => GetTermsForAdmin('skill-type'),
            ),
            'show_intro' => array(
				'type'   => 'switch',            
                'state0' => __('Off', TEXTDOMAIN),
                'state1' => __('On', TEXTDOMAIN),
                'value'  => '1'
            ),
			'about_page_title' => array(
            	'type' => 'text',
            	'placeholder' =>__('Slogan', TEXTDOMAIN),
            	'class' => 'field-spacer',
            	'value'	=> '',
			),
            'about_page_carousel' => array(
            	'type' => 'editor',
                'title' => __('Logo ', TEXTDOMAIN),
			    'id' => 'about-page-carousel',
            ),
			'about_page_logos' => array(
            	'type'  => 'upload',
                'placeholder' => __('Logo ', TEXTDOMAIN),
            	'referer' => 'px-settings-logo',
                'meta'  => array('array'=>true, 'dontsave'=>false),//This will indirectly get saved
			),
        );
        
        //Option sections
        $options = array(
            'portfolio' => array(
                'title'   => __('Portfolio Options', TEXTDOMAIN),
                'tooltip' => __('Select portfolio category to be shown on this page. If not selected, all categories will be displayed', TEXTDOMAIN),
                'fields'  => array(
                    'portfolio_posts_page' => $fields['portfolio_posts_page'],
                    'portfolio_categories' => $fields['portfolio_categories']
        		)
            ),//portfolio
            'intro' => array(
                'title'   => __('Intro Options', TEXTDOMAIN),
                'tooltip' => __('Enable or disable intro here.', TEXTDOMAIN),
                'fields'  => array(
                    'show_intro' => $fields['show_intro'],
        		)
            ),//intro			
            'image' => array(
                'title'   => __('About Options', TEXTDOMAIN),
                'tooltip' => __('Set the options of about page here.', TEXTDOMAIN),
                'fields'  => array(
                    'about_page_title' => $fields['about_page_title'],
                	'about_page_logos' => $fields['about_page_logos'],
            	)
            ), //about
		);

        return array(
            array(
                'id' => 'page_meta_box',
                'title' => __('Page Options', TEXTDOMAIN),
                'context' => 'normal',
                'priority' => 'default',
                'options' => $options,
            )//Meta box
        );
    }
	
}

new PageMeta();
