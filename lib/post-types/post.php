<?php

require_once('post-type.php');

class _Post extends PostType
{

    function __construct()
    {
        parent::__construct('post');
    }

    function CreatePostType()
    {
    }

    function RegisterScripts()
    {
        wp_register_script('portfolio', THEME_LIB_URI . '/post-types/js/portfolio.js', array('jquery'), THEME_VERSION);
		wp_register_style( 'nouislider', THEME_ADMIN_URI . '/css/nouislider.css', false, '2.1.4', 'screen' );
		wp_register_script('nouislider', THEME_ADMIN_URI  .'/scripts/jquery.nouislider.min.js', array('jquery'), '2.1.4');
		//wp_enqueue_script('nouislider', THEME_ADMIN_URI  .'/scripts/jquery.nouislider.js', array('jquery'), '2.1.4');
		
		wp_register_style( 'chosen', THEME_ADMIN_URI . '/css/chosen.css', false, '1.0.0', 'screen' );
		wp_register_script('chosen', THEME_ADMIN_URI  .'/scripts/chosen.jquery.min.js', array('jquery'), '1.0.0');
		
		wp_register_style( 'theme-admin-css', THEME_ADMIN_URI . '/css/style.css', false, '1.0.0', 'screen' );
		wp_register_script('theme-admin-script', THEME_ADMIN_URI  .'/scripts/admin.js', array('jquery'), '1.0.0');
    	
		parent::RegisterScripts();
    }

    function EnqueueScripts()
    {
		wp_enqueue_script('jquery');  

        wp_enqueue_script('jquery-easing');
		
        wp_enqueue_script('thickbox');  
		wp_enqueue_style('thickbox');  
		wp_enqueue_style('nouislider');
		wp_enqueue_script('nouislider');
		wp_enqueue_style('chosen');
		wp_enqueue_script('chosen');
		wp_enqueue_style('theme-admin-css');
		wp_enqueue_script('theme-admin-script');
    }

    protected function GetOptions()
    {
        $fields = array(
            'show_intro' => array(
				'type'   => 'switch',            
                'state0' => __('Off', TEXTDOMAIN),
                'state1' => __('On', TEXTDOMAIN),
                'value'  => 1
            ),            
        );

        //Option sections
        $options = array(
            'intro' => array(
                'title'   => __('Intro Options', TEXTDOMAIN),
                'tooltip' => __('', TEXTDOMAIN),
                'fields'  => array(
                    'show_intro' => $fields['show_intro'],
        		)
            ),//intro       
        );

        return array(
            array(
                'id' => 'post_meta_box',
                'title' => __('Post Options', TEXTDOMAIN),
                'context' => 'normal',
                'priority' => 'default',
                'options' => $options,
            )//Meta box
        );
    }
}

new _Post();