<?php
/**
 * WordPress_PostType_Framework
 *
 * @package WordPress_PostType_Framework
 * @subpackage JesGS_Taxonomy
 * @author Jess Green <jgreen@psy-dreamer.com>
 * @version $Id$
 */
/**
 * JesGS_Taxonomy
 *
 * @package JesGS_Taxonomy
 * @author Jess Green <jgreen@psy-dreamer.com>
 */
class JesGS_Taxonomy extends JesGS_FrameWork_Helper
{

    /**
     * Objects that support this taxonomy
     * @var array
     */
    protected $_object_types  = array('post');

    /**
     * Object arguments
     *
     * @var array
     */
    protected $_args = array(
        'labels'                => '',
        'public'                => true,
        'can_export'            => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => false,
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'query_var'             => true,
        'capabilities'          => array(),
    );

    /**
     * Init object
     * @return void
     */
    public function init()
    {
        register_taxonomy($this->_name, $this->_object_types, $this->_args);
    }

    /**
     * Set object arguments
     *
     * @param array $args
     * @return JesGS_Taxonomy
     */
    public function set_arguments($args = array())
    {
        global $plugin_dir;

        $args = array_merge($this->_args, $args);
        extract($args);

        $args
            = array(
                'labels' => array(
                    'name'                       => $this->_label_plural,
                    'singular_name'              => $this->_label_single,
                    'search_items'               => __('Search '               , TEXTDOMAIN). $this->_label_plural,
                    'popular_items'              => __('Popular '              , TEXTDOMAIN). $this->_label_plural,
                    'all_items'                  => __('All '                  , TEXTDOMAIN). $this->_label_plural,
                    'parent_item'                => __('Parent '               , TEXTDOMAIN). $this->_label_single,
                    'parent_item_colon'          => __('Parent '               , TEXTDOMAIN). $this->_label_single. ':: ',
                    'edit_item'                  => __('Edit '                 , TEXTDOMAIN). $this->_label_single,
                    'update_item'                => __('Update '               , TEXTDOMAIN). $this->_label_single,
                    'add_new_item'               => __('Add New '              , TEXTDOMAIN). $this->_label_single,
                    'new_item_name'              => __('New '                  , TEXTDOMAIN). $this->_label_single .__(' name', TEXTDOMAIN),
                    'separate_items_with_commas' => __('Separate '             , TEXTDOMAIN). $this->_label_plural .__(' with commas', TEXTDOMAIN),
                    'add_or_remove_items'        => __('Add or remove '        , TEXTDOMAIN). $this->_label_plural,
                    'choose_from_most_used'      => __('Choose from most used ', TEXTDOMAIN). $this->_label_plural,
                ),
                'public'                => $public,
                'can_export'            => $can_export,
                'show_in_nav_menus'     => $show_in_nav_menus,
                'show_ui'               => $show_ui,
                'show_tagcloud'         => $show_tagcloud,
                'hierarchical'          => $hierarchical,
                'update_count_callback' => '',
                'rewrite'               => $rewrite,
                'query_var'             => $query_var,
                'capabilities'          => $capabilities,
            );

        $this->_args = $args;

        return $this;
    }

    /**
     * Set taxonomy objects
     *
     * @param array $object_types
     * @return JesGS_Taxonomy
     */
    public function set_objects($object_types)
    {
        $this->_object_types  = $object_types;

        return $this;
    }
}
