<?php

require_once('post-type.php');

class Portfolio extends PostType
{

    function __construct()
    {
        parent::__construct('portfolio');
    }

    function CreatePostType()
    {
	    $p = new JesGS_PostType(array(
	        'name'       => $this->postType,
	        'singlename' => __( 'Portfolio', TEXTDOMAIN ),
	        'pluralname' => __( 'Portfolios', TEXTDOMAIN ),
	        'arguments'  => array(
				'public' => true,
				'capability_type' => 'post',
				'has_archive' => true,
				'hierarchical' => false,
				'menu_icon' => THEME_IMAGES_URI . '/gallery_icon.png',
	            'supports' => array('title', 'editor', 'thumbnail'),           
	    		'rewrite' => array('slug' => $this->postType, 'with_front' => true),
	        ),
	    ));
        
	    new JesGS_Taxonomy(array(
			'name' => 'skill-type',
	        'singlename' =>  __( "Skill Type",  TEXTDOMAIN ), 
	        'pluralname' =>  __( "Skill Types",  TEXTDOMAIN ), 
	        'objects' => array($this->postType),
	        'arguments' => array(
				'label' => __( "Skill Types", TEXTDOMAIN ), 
	            'hierarchical' => true,
	    		'rewrite' => false
			)    
	    ));
        
    }

    function RegisterScripts()
    {
        wp_register_script('portfolio', THEME_LIB_URI . '/post-types/js/portfolio.js', array('jquery'), THEME_VERSION);
		wp_register_style( 'nouislider', THEME_ADMIN_URI . '/css/nouislider.css', false, '2.1.4', 'screen' );
		wp_register_script('nouislider', THEME_ADMIN_URI  .'/scripts/jquery.nouislider.min.js', array('jquery'), '2.1.4');
		//wp_enqueue_script('nouislider', THEME_ADMIN_URI  .'/scripts/jquery.nouislider.js', array('jquery'), '2.1.4');
		
		wp_register_style( 'chosen', THEME_ADMIN_URI . '/css/chosen.css', false, '1.0.0', 'screen' );
		wp_register_script('chosen', THEME_ADMIN_URI  .'/scripts/chosen.jquery.min.js', array('jquery'), '1.0.0');
		
		wp_register_style( 'theme-admin-css', THEME_ADMIN_URI . '/css/style.css', false, '1.0.0', 'screen' );
		wp_register_script('theme-admin-script', THEME_ADMIN_URI  .'/scripts/admin.js', array('jquery'), '1.0.0');
        
        parent::RegisterScripts();
    }

    function EnqueueScripts()
    {
		wp_enqueue_script('jquery');  
		wp_enqueue_script('thickbox');  
		wp_enqueue_style('thickbox');  
		wp_enqueue_style('nouislider');
		wp_enqueue_script('nouislider');
		wp_enqueue_style('chosen');
		wp_enqueue_script('chosen');
		wp_enqueue_style('theme-admin-css');
		wp_enqueue_script('theme-admin-script');
    	    	
    	wp_enqueue_script('hoverIntent');
        wp_enqueue_script('jquery-easing');

        wp_enqueue_script('portfolio');
    }

    function OnProcessFieldForStore($post_id, $key, $settings)
    {
    	if ($key != "image")
    		return false;
    	
		$images = $_POST["image"];

        //Filter results
        $images = array_filter( array_map( 'trim', $images ), 'strlen' );
        //ReIndex
        $images = array_values($images);

        update_post_meta( $post_id, "image", $images );
    	
        return true;
    }

    protected function GetOptions()
    {
        $fields = array(
            'image' => array(
                'type'  => 'upload',
                'placeholder' => __('Portfolio Image', TEXTDOMAIN),
                'referer' => 'px-portfolio-image',
                'meta'  => array('array'=>true, 'dontsave'=>false),//This will indirectly get saved
            ),
            'show_intro' => array(
				'type'   => 'switch',            
                'state0' => __('Off', TEXTDOMAIN),
                'state1' => __('On', TEXTDOMAIN),
                'value'  => 1
            ),            
        );

        //Option sections
        $options = array(
            'intro' => array(
                'title'   => __('Intro Options', TEXTDOMAIN),
                'tooltip' => __('', TEXTDOMAIN),
                'fields'  => array(
                    'show_intro' => $fields['show_intro'],
        		)
            ),//intro       
        	'image' => array(
                'title'   => __('Portfolio Images', TEXTDOMAIN),
                'tooltip' => __('Upload your portfolio Image(s) here. If you upload more than one image it will be shown as slider', TEXTDOMAIN),
                'fields'  => array(
                    'image' => $fields['image']
        		)
            ),//images sec
        );

        return array(
            array(
                'id' => 'portfolio_meta_box',
                'title' => __('Portfolio Options', TEXTDOMAIN),
                'context' => 'normal',
                'priority' => 'default',
                'options' => $options,
            )//Meta box
        );
    }
}

new Portfolio();