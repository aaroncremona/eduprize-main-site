<?php

require_once('post-type.php');

class SpecialIntro extends PostType
{

    function __construct()
    {
        parent::__construct('special-intro');
    }

    function CreatePostType()
    {
	    $p = new JesGS_PostType(array(
	        'name'       => $this->postType,
	        'singlename' => __( 'Special Intro', TEXTDOMAIN ),
	        'pluralname' => __( 'Special Intro', TEXTDOMAIN ),
	        'arguments'  => array(
				'public' => true,
				'capability_type' => 'post',
				'has_archive' => true,
				'hierarchical' => false,
				'menu_icon' => THEME_IMAGES_URI . '/specialintro.png',
	            'supports' => array('title', 'editor', 'thumbnail'),           
	    		'rewrite' => array('slug' => $this->postType),
	        ),
	    ));
	    
    }

    function EnqueueScripts()
    {
        wp_enqueue_script('hoverIntent');
        wp_enqueue_script('jquery-easing');

        wp_enqueue_style('theme-admin');
        wp_enqueue_script('theme-admin');
    }
}

new SpecialIntro();