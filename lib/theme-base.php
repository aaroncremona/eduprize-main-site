<?php 
if(function_exists('wp_get_theme'))
{
	$theme = wp_get_theme();
	
	define('THEME_NAME',	$theme->Name);
	define('THEME_NAME_SEO', strtolower(str_replace(" ", "_", THEME_NAME)));
	define('THEME_AUTHOR',	$theme->Author);
	define('THEME_VERSION',	$theme->Version);
	define('OPTIONS_KEY', "theme_". THEME_NAME_SEO ."_options");
}

// load comment scripts only on single pages
function single_scripts() {
	if(is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) 
		wp_enqueue_script( 'comment-reply' ); // loads the javascript required for threaded comments 
}
add_action('wp_print_scripts', 'single_scripts');


function register_menu() {
	register_nav_menu( 'primary-nav', __( 'Primary Navigation', TEXTDOMAIN ) );
}
add_action( 'init', 'register_menu' );


/*-----------------------------------------------------------------------------------*/
/*	Configure WP2.9+ Thumbnails
/*-----------------------------------------------------------------------------------*/

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 770, 513, true );
	
	add_image_size( 'post-full-thumbnails', 1170, 780, true );
	
	add_image_size( 'thumb-portfolio', 262, 180, true );
	add_image_size( 'portfolio-related', 262, 180, true );
	add_image_size( 'latest-posts', 270, 186, true );
	add_image_size( 'thumb-portfolio-widget', 59, 59, true);
	add_image_size( 'top-blog-post', 170, 200, true);

	add_image_size( 'team-member', 270, 285, true );
	add_image_size( 'home-gallery', 270, 285, true);
	add_image_size( 'thumb-portfolio-2', 362, 250, true );	
}

/*-----------------------------------------------------------------------------------*/
/*	RSS Feeds
/*-----------------------------------------------------------------------------------*/

add_theme_support( 'automatic-feed-links' );
