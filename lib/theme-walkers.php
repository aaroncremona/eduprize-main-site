<?php

//<li><a class="current" data-filter="*" href="#"> View All </a></li>

class Portfolio_Walker extends Walker_Category {

   function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 )  
   {
		extract($args);
		$cat_name = esc_attr( $category->name);
		$cat_name = apply_filters( 'list_cats', $cat_name, $category );
		
		$link = '<a href="#" data-filter=".term-'.$category->term_id.'" ';
		
		if ( $use_desc_for_title == 0 || empty($category->description) )
			$link .= 'title="' . sprintf(__( 'View all items filed under %s', TEXTDOMAIN ), $cat_name) . '" >';
		else
			$link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '" >';
		
		$link .= $cat_name . '</a>';
		
		if ( isset($current_category) && $current_category )
			$_current_category = get_category( $current_category );
		
		if ( $args['style'] == 'list' ) {
			$class = 'cat-item cat-item-'.$category->term_id;
			
			if ( isset($current_category) && $current_category && ($category->term_id == $current_category) )
			{
				$class .=  ' current';
			}
			elseif ( isset($_current_category) && $_current_category && ($category->term_id == $_current_category->parent) )
			{
				$class .=  ' current-parent';
			}
			
			$output .= "<li class=\"$class\"";
			$output .= ">$link\n";
		} 
		else 
		{
			$output .= "\t$link<br />\n";
		}
   }
   
}

class Custom_Nav_Walker extends Walker_Nav_Menu
{
	private $navIdPrefix = '';

	public function __construct($idPrefix='menu-item-')
	{
		$this->navIdPrefix = $idPrefix;
	}
	
    function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output )
    {
        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }	

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) 
	{
	   	global $wp_query;
	   	$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

	   	$class_names = $value = '';
	   
	   	$classes = empty( $item->classes ) ? array() : (array) $item->classes;
	   	
		if ($depth == 0)
	   	{
	   		if (in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes) || in_array('current_page_item', $classes))
	   			$classes[] = 'active ';
	   	}	   		   
	   	
	   	$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );	   		   	
	   	$class_names = ' class="'. esc_attr( $class_names ) . '"';

	   	$output .= $indent . '<li id="'. $this->navIdPrefix . $item->ID . '"' . $value . $class_names .'>';

	   	$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
	   	$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	   	$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
	   	$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	   	$description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

	   	$prepend = '<div class="clear"></div>';
	   
	   	if($depth != 0)
	   	{
			$description = $prepend = '';
	   	}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID );
		$item_output .= $description.$args->link_after;
		$item_output .= '</a>';
		$item_output .= $prepend;
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

?>