<?php
function Get_Default_Options()
{
	return array(
	//General
		'logo' => '',
		'favicon' => '',
		'responsive_layout' => 1,
		'sidebar_position' => 2,
		'google_font' => 'Open Sans',
		'google_font_family' => 'Open+Sans:700,400,300',
		'theme_main_color' => '#009cff',

	//Social
		'social_facebook_address' => '',
    	'social_twitter_address' => '',
        'social_dribbble_address' => '',
        'social_vimeo_address' => '',
        'social_youtube_address' => '',
        'social_googleplus_address' => '',
        'social_digg_address' => '',
        'social_tumblr_address' => '',
        'social_linkedin_address' => '',
        'social_flickr_address' => '',
        'social_forrst_address' => '',
        'social_github_address' => '',
        'social_myspace_address' => '',
        'social_lastfm_address' => '',
        'social_paypal_address' => '',
        'rss_feed_address' => get_bloginfo('rss2_url'),
        'social_skype_address' => '',
        'social_wordpress_address' => '',
        'social_yahoo_address' => '',	
	
	//Header
		'header_email'	=> '',
		'header_phone'	=> '',
		'social_networks'	=> 1,
	
	//Footer
		'footer-logo'	=> '',
		'footer_widgets' => 4,
		'footer_copyright' => '2013 PixFlow Production Network. All Rights Reserved',
	
	//Contact Page
		'maps_api_key' => 'AIzaSyC2jFfPfQLc8p0T3I_i-z7_35kYOOQZPu0',
		'contact_map_address' => 'Mew York,little italy,7th street',
	
	//Home Navigation
		'link_icon_1'	=> '',
		'link_title_1'	=> '',
		'link_url_1'	=> '',
		'link_text_1'	=> '',	
		'link_icon_2'	=> '',
		'link_title_2'	=> '',
		'link_url_2'	=> '',
		'link_text_2'	=> '',	
		'link_icon_3'	=> '',
		'link_title_3'	=> '',
		'link_url_3'	=> '',
		'link_text_3'	=> '',	
		'link_icon_4'	=> '',
		'link_title_4'	=> '',
		'link_url_4'	=> '',
		'link_text_4'	=> '',	
	
	//
		'third_tab_content_kind'	=> 1,
		'third_tab_page'			=> 0,
	
	//spotlight
		'spotlight_1'	=> '',
		'spotlight_2'	=> '',
		'spotlight_3'	=> '',
		'spotlight_4'	=> '',
	
	//tagline
		'home_tagline'	=>	1,
		'tagline_title'	=> '',
		'tagline_subtitle'	=> '',
		'tagline_button'	=> '',
		'tagline_button_url'	=> '',
	
	//Home Page
		'first_tab_title' => '',
		'first_tab_heading' => '',
		'second_tab_title' => '',
		'second_tab_heading' => '',
		'third_tab_title' => '',
		'third_tab_heading' => '',
		'fourth_tab_title' => '',
		'fourth_tab_heading' => '',
		'default_selected_tab' => 0,
		'tab_position' => '',
		'home_slogan_text' => 'We Design Unique And Beautiful Websites That Fits Every Category',
		'home_navigation' => 1,
		'home_special_intro' => 1,
		'home_latest_portfolio' => 1,	
		'home_latest_portfolio_title' => '',
		'home_latest_portfolio_desc' => '',
	
	//Blog Page
		'blog_sidebar_position' => 2,

	//Portfolio Page
		'portfolio_related_posts' => 6,
		
	//Advanced Options
		'custom_javascript' => '',
		'custom_css' => '',

	//Import Demo Content
		'import_dummy_data'	=> 0,
	);
}