<?php

function admin_get_defaults()
{
    static $values = array();

    if(count($values))
        return $values;

    //Extract key-value pairs from settings
    $settings = admin_get_form_settings();
    $panels   = $settings['panels'];

    foreach($panels as $panel)
    {
        foreach($panel['sections'] as $section)
        {
            foreach($section['fields'] as $fieldKey => $field)
            {
                $values[$fieldKey] = array_value('value', $field);
            }
        }
    }

    return $values;
}

function admin_get_form_settings()
{
    static $settings = array();//Cache the settings

    if(count($settings))
        return $settings;

    $generalSettingsPanel = array(
        'title' => __('General', TEXTDOMAIN),
        'sections' => array(

            'favicon' => array(
                'title'   => __('Custom Favicon', TEXTDOMAIN),
                'tooltip' => __('Specify custom favicon url or upload new icon here', TEXTDOMAIN),
                'fields'  => array(
                    'favicon' => array(
                        'type' => 'upload',
                        'title' => __('Upload Favicon', TEXTDOMAIN),
                        'referer' => 'px-settings-favicon'
                    ),
                )
            ),//Favicon sec
            'logo' => array(
                'title'   => __('Custom Logo', TEXTDOMAIN),
                'tooltip' => __('Specify custom logo url or upload new logo here', TEXTDOMAIN),
                'fields'  => array(
                    'logo' => array(
                        'type' => 'upload',
                        'title' => __('Upload Logo', TEXTDOMAIN),
                        'referer' => 'px-settings-logo'
                    ),
                )
            ),//Logo sec
            'responsive-layout' => array(
                'title'   => __('Responsive Layout', TEXTDOMAIN),
                'tooltip' => __('Enable or disable responsive layout here', TEXTDOMAIN),
                'fields'  => array(
                    'responsive_layout' => array(
                        'type'   => 'switch',
                        'state0' => __('Disabled', TEXTDOMAIN),
                        'state1' => __('Enabled', TEXTDOMAIN),
                        'value'  => '1'
                    ),
                )
            ),//responsive-layout sec
            
            'google_font' => array(
                'title'   => __('Google Font', TEXTDOMAIN),
                'tooltip' => __('Open Sans is Haxon default google font, you can replace your desired google font with that by adding font name in following fields. NOTICE that font name must be exactly the way it is on google font website.', TEXTDOMAIN),
                'fields'  => array(
                    'google_font' => array(
                        'type' => 'text',
                        'class' => 'field-spacer',
            			'placeholder' => __('Google Font Name', TEXTDOMAIN),
						'value' => 'Open Sans',
            		),
            		'google_font_family' => array(
                        'type' => 'text',
                        'class' => 'field-spacer',
                    	'placeholder' => __('Google Font Family link', TEXTDOMAIN),
            			'value' => 'Open+Sans:700,400,300',            	
                    ),
                )
            ),//google_font_family sec

            'theme_main_color' => array(
                'title'   => __('Theme General Color', TEXTDOMAIN),
                'tooltip' => __('Theme general color is the main color of most Of theme graphical elements.', TEXTDOMAIN),
                'fields'  => array(
                    'theme_main_color' => array(
                        'type'   => 'color',
            			'value'=> '#009cff',
                    ),
                )
            ),//main-color sec
            'sidebar-position' => array(
                'title'   => __('Page Sidebar Position', TEXTDOMAIN),
                'tooltip' => __('Choose default sidebar position for pages that has sidebar, you can override this option in page settings', TEXTDOMAIN),
                'fields'  => array(
                    'sidebar_position' => array(
                        'type' => 'visual-select',
                        'options' => array('none'=>0, /*'left-side'=>1,*/ 'right-side'=>2),
                        'class' => 'page-sidebar',
                        'value' => '2',
                    ),
                )
            ),//sidebar-position sec
		)
    );//$generalSettingsPanel

    $socialSettingsPanel = array(
        'title' => __('Social', TEXTDOMAIN),
        'sections' => array(
            'socials' => array(
                'title'   => __('Social Network URLs', TEXTDOMAIN),
                'tooltip' => __('Enter your social network addresses in respective fields. You can clear fields to hide icons from the website user interface', TEXTDOMAIN),
                'fields'  => array(
                    'social_facebook_address' => array(
                        'type' => 'text',
                        'label' => __('Facebook', TEXTDOMAIN),
    					'value'=> '',
                    ),//Facebook
                    'social_twitter_address' => array(
                        'type' => 'text',
                        'label' => __('Twitter', TEXTDOMAIN),
                    	'value'=> '',
                    ),//twitter
                    'social_dribbble_address' => array(
                        'type' => 'text',
                        'label' => __('Dribbble', TEXTDOMAIN),
                    	'value'=> '',
                    ),//dribbble
                    'social_vimeo_address' => array(
                        'type' => 'text',
                        'label' => __('Vimeo', TEXTDOMAIN),
                    	'value'=> '',
                    ),//vimeo
                    'social_youtube_address' => array(
                        'type' => 'text',
                        'label' => __('YouTube', TEXTDOMAIN),
                    	'value'=> '',
                    ),//youtube
                    'social_googleplus_address' => array(
                        'type' => 'text',
                        'label' => __('Google+', TEXTDOMAIN),
                    	'value'=> '',
                    ),//Google+
                    'social_digg_address' => array(
                        'type' => 'text',
                        'label' => __('Digg', TEXTDOMAIN),
                    	'value'=> '',
                    ),//Digg
                    'social_tumblr_address' => array(
                        'type' => 'text',
                        'label' => __('Tumblr', TEXTDOMAIN),
                    	'value'=> '',
                    ),//Tumblr
                    'social_linkedin_address' => array(
                        'type' => 'text',
                        'label' => __('LinkedIn', TEXTDOMAIN),
                    	'value'=> '',
                    ),//LinkedIn
                    'social_flickr_address' => array(
                        'type' => 'text',
                        'label' => __('Flickr', TEXTDOMAIN),
                    	'value'=> '',
                    ),//flickr
                    'social_forrst_address' => array(
                        'type' => 'text',
                        'label' => __('Forrst', TEXTDOMAIN),
                    	'value'=> '',
                    ),//forrst
                    'social_github_address' => array(
                        'type' => 'text',
                        'label' => __('GitHub', TEXTDOMAIN),
                    	'value'=> '',
                    ),//GitHub
                    'social_myspace_address' => array(
                        'type' => 'text',
                        'label' => __('MySpace', TEXTDOMAIN),
                    	'value'=> '',
                    ),//myspace
                    'social_lastfm_address' => array(
                        'type' => 'text',
                        'label' => __('Last.fm', TEXTDOMAIN),
                    	'value'=> '',
                    ),//Last.fm
                    'social_paypal_address' => array(
                        'type' => 'text',
                        'label' => __('PayPal', TEXTDOMAIN),
                    	'value'=> '',
                    ),//Paypal
                    'rss_feed_address' => array(
                        'type' => 'text',
                        'label' => __('RSS Feed', TEXTDOMAIN),
                        'value' => get_bloginfo('rss2_url'),
                    ),//rss
                    'social_skype_address' => array(
                        'type' => 'text',
                        'label' => __('Skype', TEXTDOMAIN),
                    	'value'=> '',
                    ),//skype
                    'social_wordpress_address' => array(
                        'type' => 'text',
                        'label' => __('WordPress', TEXTDOMAIN),
                    	'value'=> '',
                    ),//wordpress
                    'social_yahoo_address' => array(
                        'type' => 'text',
                        'label' => __('Yahoo', TEXTDOMAIN),
                    	'value'=> '',
                    ),//wordpress
                )
            ),//Favicon sec
        ),
    );

    $headerSettingsPanel = array(
        'title' => __('Header Settings', TEXTDOMAIN),
        'sections' => array(

            'header_email' => array(
                'title'   => __('Email', TEXTDOMAIN),
                'tooltip' => __('Enter header email here.', TEXTDOMAIN),
                'fields'  => array(
                    'header_email' => array(
                        'type' => 'text',
                    	'value'=> '',
    				),//header_email sec
                )
            ),//header_email sec
    
            'header_phone' => array(
                'title'   => __('Phone', TEXTDOMAIN),
                'tooltip' => __('Enter header phone number here.', TEXTDOMAIN),
                'fields'  => array(
                    'header_phone' => array(
                        'type' => 'text',
                    	'value'=> '',
            		),//header_phone sec
                )
            ),//header_phone sec
            
            'social_networks' => array(
                'title'   => __('Display social networks icons', TEXTDOMAIN),
                'tooltip' => __('Enable or disable header social networks icons here', TEXTDOMAIN), 
                'fields'  => array(
                    'social_networks' => array(
                        'type'   => 'switch',
                        'state0' => __('Disabled', TEXTDOMAIN),
                        'state1' => __('Enabled', TEXTDOMAIN),
                        'value'  => '1'
                    ),
                )
            ),//social_networks sec
        ),
    );

    $footerSettingsPanel = array(
        'title' => __('Footer Settings', TEXTDOMAIN),
        'sections' => array(
            'widget-areas' => array(
                'title'   => __('Widget Areas', TEXTDOMAIN),
                'tooltip' => __('How many widget areas you like to have in the footer', TEXTDOMAIN),
                'fields'  => array(
                    'footer_widgets' => array(
                        'type' => 'visual-select',
                        'options' => array('zero' => 0, 'one'=>1, 'two'=>2, 'three'=>3, 'four'=>4),
                        'class' => 'footer-widgets',
                        'value' => 4,
                    ),
                )
            ),//widget-areas sec
            'footer-logo' => array(
                'title'   => __('Custom Footer Logo', TEXTDOMAIN),
                'tooltip' => __('Specify custom logo url or upload new logo here', TEXTDOMAIN),
                'fields'  => array(
                    'footer-logo' => array(
                        'type' => 'upload',
                        'title' => __('Upload Footer Logo', TEXTDOMAIN),
                        'referer' => 'px-settings-footer-logo'
                    ),
                )
            ),//Logo sec
            'footer_copyright' => array(
                'title'   => __('Copyright Message', TEXTDOMAIN),
                'tooltip' => __('Enter footer copyright text. ', TEXTDOMAIN),
                'fields'  => array(
                    'footer_copyright' => array(
                        'type' => 'text',
                        'label' => __('Copyright Text', TEXTDOMAIN),
                        'value' => '2013 PixFlow Production Network. All Rights Reserved'
                    ),//footer_copyright sec
                )
            ),//widget-areas sec

        ),
    );

    $contactSettingsPanel = array(
        'title' => __('Contact Page Settings', TEXTDOMAIN),
        'sections' => array(
            'maps_api_key' => array(
                'title'   => __('Google API Key', TEXTDOMAIN),
                'tooltip' => __('Enter Google API Key for google map here. ', TEXTDOMAIN),
                'fields'  => array(
                    'maps_api_key' => array(
                        'type' => 'text',
    					'class'=> 'field-spacer',
    					'value'=> '',
                    ),//maps_api_key sec
                )
            ),//maps_api_key sec
            'contact_map_address' => array(
                'title'   => __('Address', TEXTDOMAIN),
                'tooltip' => __('Enter Google Map address here. ', TEXTDOMAIN),
                'fields'  => array(
                    'contact_map_address' => array(
                        'type' => 'text',
    					'class'=> 'field-spacer',
            			'value'=> ''
                        //'label' => __('Google API Key', TEXTDOMAIN),
                    ),//contact_map_address sec
                )
            ),//contact_map_address sec
        ),
    );

    $homenavigationSettingsPanel = array(
        'title' => __('Home Navigation', TEXTDOMAIN),
        'sections' => array(

            'home_navigation' => array(
                'title'   => __('Display Home Navigation', TEXTDOMAIN),
                'tooltip' => __('Enable or disable home navigation here', TEXTDOMAIN), 
                'fields'  => array(
                    'home_navigation' => array(
                        'type'   => 'switch',
                        'state0' => __('Disabled', TEXTDOMAIN),
                        'state1' => __('Enabled', TEXTDOMAIN),
                        'value'  => '1'
                    ),
                )
            ),//home_navigation sec
    
            'first_tab' => array(
                'title'   => __('First Tab', TEXTDOMAIN),
                'tooltip' => __('Enter the first tab title and heading here.', TEXTDOMAIN),
                'fields'  => array(
                    'first_tab_title' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> __('Start Here!', TEXTDOMAIN),
            			'placeholder' => __('Title', TEXTDOMAIN),
            		),
                    'first_tab_heading' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            			'placeholder' => __('Heading', TEXTDOMAIN),
            		),
            	)
            ),

            'second_tab' => array(
                'title'   => __('Second Tab', TEXTDOMAIN),
                'tooltip' => __('Enter the second tab title and heading here.', TEXTDOMAIN),
                'fields'  => array(
                    'second_tab_title' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> __('Slogan', TEXTDOMAIN),
            			'placeholder' => __('Title', TEXTDOMAIN),
            		),
                    'second_tab_heading' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            			'placeholder' => __('Heading', TEXTDOMAIN),
            		),
            	)
            ),
            
            'third_tab' => array(
                'title'   => __('Third Tab', TEXTDOMAIN),
                'tooltip' => __('Enter the third tab title and heading here.', TEXTDOMAIN),
                'fields'  => array(
                    'third_tab_title' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> __('Top Blog Posts', TEXTDOMAIN),
            			'placeholder' => __('Title', TEXTDOMAIN),
            		),
                    'third_tab_heading' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            			'placeholder' => __('Heading', TEXTDOMAIN),
            		),
            	)
            ),
                        
            'fourth_tab' => array(
                'title'   => __('Fourth Tab', TEXTDOMAIN),
                'tooltip' => __('Enter the fourth tab title and heading here.', TEXTDOMAIN),
                'fields'  => array(
                    'fourth_tab_title' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> __('Spotlight', TEXTDOMAIN),
            			'placeholder' => __('Title', TEXTDOMAIN),
            		),
                    'fourth_tab_heading' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            			'placeholder' => __('Heading', TEXTDOMAIN),
            		),
            	)
            ),
            
            'home-tab-position' => array(
                'title'   => __('Tab Positions', TEXTDOMAIN),
                'tooltip' => __('Define the tab\'s positions of home navigation here.', TEXTDOMAIN),
                'fields'  => array(
                    'tab_position' => array(
                        'type' => 'sortable',
                        'options' => array(0	=>	__('First Tab', TEXTDOMAIN), 
    										1	=>	__('Second Tab', TEXTDOMAIN),
    										2	=>	__('Third Tab', TEXTDOMAIN),
    										3	=>	__('Fourth Tab', TEXTDOMAIN),
    									),
                    ),
                )
            ),//tab-position sec
            
            'home-tab-active' => array(
                'title'   => __('Active Tab', TEXTDOMAIN),
                'tooltip' => __('Select the active tab here.', TEXTDOMAIN),
                'fields'  => array(
                    'default_selected_tab' => array(
                        'type' => 'select',
                        'options' => array(0	=>	__('First Tab', TEXTDOMAIN), 
    										1	=>	__('Second Tab', TEXTDOMAIN),
    										2	=>	__('Third Tab', TEXTDOMAIN),
    										3	=>	__('Fourth Tab', TEXTDOMAIN),
    									),
                		'value' => 0,
                    ),
                )
            ),//tab-position sec
            
		)
    );            
    
    $firstSettingsPanel = array(
        'title' => __('First Tab Settings', TEXTDOMAIN),
        'sections' => array(

            'info' => array(
                'title'   => '',//__('Info', TEXTDOMAIN),
                'tooltip' => __('You can download your icons from here! choose any size you want and download the icons.', TEXTDOMAIN),
                'fields'  => array(
                    'info_icons' => array(
                        'type' => 'info',
                        'content' => __('You can download your icons from <a href="http://icomoon.io/app">here</a>! choose any size you want and download the icons.', TEXTDOMAIN),
                    ),
              	)
            ),//Icon_1 sec
    
            'icon_1' => array(
                'title'   => __('Icon 1', TEXTDOMAIN),
                'tooltip' => __('Choose icon and title of the first button here. ', TEXTDOMAIN),
                'fields'  => array(
                    'link_icon_1' => array(
                        'type' => 'upload',
                        'placeholder' => __('Icon', TEXTDOMAIN),
                        'referer' => 'px-settings-icon-1'
                    ),
                    'link_title_1' => array(
                        'type' => 'text',
                        'placeholder' => __('Title', TEXTDOMAIN),
                        'referer' => 'px-settings-title-1'
                    ),
                    'link_url_1' => array(
                        'type' => 'text',
                        'placeholder' => __('Url', TEXTDOMAIN),
                        'referer' => 'px-settings-url-1'
                    ),
                    'link_text_1' => array(
                        'type' => 'textarea',
                        'placeholder' => __('Description', TEXTDOMAIN),
                    	'row' => 4,
                    	'referer' => 'px-settings-text-1'
                    ),                    
              	)
            ),//Icon_1 sec
            'icon_2' => array(
                'title'   => __('Icon 2', TEXTDOMAIN),
                'tooltip' => __('Choose icon and title of the second button here.', TEXTDOMAIN),
                'fields'  => array(
                    'link_icon_2' => array(
                        'type' => 'upload',
                        'placeholder' => __('Icon', TEXTDOMAIN),
                        'referer' => 'px-settings-icon-2'
                    ),
                    'link_title_2' => array(
                        'type' => 'text',
                        'placeholder' => __('Title', TEXTDOMAIN),
                        'referer' => 'px-settings-title-2'
                    ),
                    'link_url_2' => array(
                        'type' => 'text',
                        'placeholder' => __('Url', TEXTDOMAIN),
                        'referer' => 'px-settings-url-2'
                    ),
                    'link_text_2' => array(
                        'type' => 'textarea',
                        'placeholder' => __('Description', TEXTDOMAIN),
                    	'row' => 4,
                        'referer' => 'px-settings-text-2'
                    ),                    
              	)
            ),//Icon_2 sec
            'icon_3' => array(
                'title'   => __('Icon 3', TEXTDOMAIN),
                'tooltip' => __('Choose icon and title of the third button here.', TEXTDOMAIN),
                'fields'  => array(
                    'link_icon_3' => array(
                        'type' => 'upload',
                        'placeholder' => __('Icon', TEXTDOMAIN),
                        'referer' => 'px-settings-icon-3'
                    ),
                    'link_title_3' => array(
                        'type' => 'text',
                        'placeholder' => __('Title', TEXTDOMAIN),
                        'referer' => 'px-settings-title-3'
                    ),
                    'link_url_3' => array(
                        'type' => 'text',
                        'placeholder' => __('Url', TEXTDOMAIN),
                        'referer' => 'px-settings-url-3'
                    ),
                    'link_text_3' => array(
                        'type' => 'textarea',
                        'placeholder' => __('Description', TEXTDOMAIN),
                    	'row' => 4,
                        'referer' => 'px-settings-text-3'
                    ),                    
              	)
            ),//Icon_3 sec
            'icon_4' => array(
                'title'   => __('Icon 4', TEXTDOMAIN),
                'tooltip' => __('Choose icon and title of the fourth button here.', TEXTDOMAIN),
                'fields'  => array(
                    'link_icon_4' => array(
                        'type' => 'upload',
                        'placeholder' => __('Icon', TEXTDOMAIN),
                        'referer' => 'px-settings-icon-4'
                    ),
                    'link_title_4' => array(
                        'type' => 'text',
                        'placeholder' => __('Title', TEXTDOMAIN),
                        'referer' => 'px-settings-title-4'
                    ),
                    'link_url_4' => array(
                        'type' => 'text',
                        'placeholder' => __('Url', TEXTDOMAIN),
                        'referer' => 'px-settings-url-4'
                    ),
                    'link_text_4' => array(
                        'type' => 'textarea',
                        'placeholder' => __('Description', TEXTDOMAIN),
                    	'row' => 4,
                        'referer' => 'px-settings-text-4'
                    ),                    
              	)
            ),//Icon_4 sec
        )
    );    
    
    $secondSettingsPanel = array(
        'title' => __('Second Tab Settings', TEXTDOMAIN),
        'sections' => array(
                    
            'home_slogan_text' => array(
                'title'   => __('Second Tab Text', TEXTDOMAIN),
                'tooltip' => __('Enter your slogan here. You can clear the field to hide slogan section.', TEXTDOMAIN),
                'fields'  => array(
                    'home_slogan_text' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            			'placeholder' => __('Slogan text', TEXTDOMAIN),
            		),
                )
            ),//home_slogan_text sec
        ),
    );
    
    $pageArray = GetPostsForAdmin("page");
    $thirdSettingsPanel = array(
        'title' => __('Third Tab Settings', TEXTDOMAIN),
        'sections' => array(
                    
            'page_content' => array(
                'title'   => __('Tab Content', TEXTDOMAIN),
                'tooltip' => __('Define the content of third tab here.', TEXTDOMAIN),
                'fields'  => array(
                    'third_tab_content_kind' => array(
                        'type' => 'select',
            			'options' => array( '1' => __('Top Blog Posts', TEXTDOMAIN), 
								 			'2' => __('Select a page', TEXTDOMAIN)), 
    					'class' => 'field-spacer field-selector',
    					'attributes' => 'data-fields=".third_tab_page"',
						'option-attributes' => array('2' => 'data-show=".third_tab_page"'),
            		),
                    'third_tab_page' => array(
                        'type' => 'select',
            			'options' => $pageArray, 
    					'class' => 'third_tab_page',
            		),
            	)
            ),            
		),
    );
    
    $portfolioArray = GetPostsForAdmin("portfolio");
    $fourthSettingsPanel = array(
        'title' => __('Fourth Tab Settings', TEXTDOMAIN),
        'sections' => array(

            'spotlight_1' => array(
                'title'   => __('First Place', TEXTDOMAIN),
                'tooltip' => __('Choose a portfolio item to display in first place here. ', TEXTDOMAIN),
                'fields'  => array(
                    'spotlight_1' => array(
                        'type' => 'select',
                        'placeholder' => __('First Place', TEXTDOMAIN),
                        'options' => $portfolioArray 
                    ),
              	)
            ),//spotlight_1 sec
            'spotlight_2' => array(
                'title'   => __('Second Place', TEXTDOMAIN),
                'tooltip' => __('Choose a portfolio item to display in second place here. ', TEXTDOMAIN),
                'fields'  => array(
                    'spotlight_2' => array(
                        'type' => 'select',
                        'placeholder' => __('Second Place', TEXTDOMAIN),
                        'options' => $portfolioArray 
                    ),
              	)
            ),//spotlight_2 sec
            'spotlight_3' => array(
                'title'   => __('Third Place', TEXTDOMAIN),
                'tooltip' => __('Choose a portfolio item to display in third place here. ', TEXTDOMAIN),
                'fields'  => array(
                    'spotlight_3' => array(
                        'type' => 'select',
                        'placeholder' => __('Third Place', TEXTDOMAIN),
                        'options' => $portfolioArray 
                    ),
              	)
            ),//spotlight_3 sec
            'spotlight_4' => array(
                'title'   => __('Fourth Place', TEXTDOMAIN),
                'tooltip' => __('Choose a portfolio item to display in fourth place here. ', TEXTDOMAIN),
                'fields'  => array(
                    'spotlight_4' => array(
                        'type' => 'select',
                        'placeholder' => __('Fourth Place', TEXTDOMAIN),
                        'options' => $portfolioArray 
                    ),
              	)
            ),//spotlight_4 sec
        )
    );//$spotlightSettingsPanel    
    
    $taglineSettingsPanel = array(
        'title' => __('Tagline Settings', TEXTDOMAIN),
        'sections' => array(
    
    		'home_tagline' => array(
                'title'   => __('Tagline Display', TEXTDOMAIN),
                'tooltip' => __('Enable or disable Tagline section', TEXTDOMAIN), 
                'fields'  => array(
                    'home_tagline' => array(
                        'type'   => 'switch',
                        'state0' => __('Disabled', TEXTDOMAIN),
                        'state1' => __('Enabled', TEXTDOMAIN),
                        'value'  => '1'
                    ),
                )
            ),//home_tagline sec

    		'tagline_title' => array(
                'title'   => __('Tagline Title', TEXTDOMAIN),
                'tooltip' => __('Enter Tagline Title here.', TEXTDOMAIN), 
                'fields'  => array(
                    'tagline_title' => array(
                        'type'   => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
                    ),
                )
            ),//tagline_title sec
            
    		'tagline_subtitle' => array(
                'title'   => __('Tagline Subtitle', TEXTDOMAIN),
                'tooltip' => __('Enter Tagline Subtitle here.', TEXTDOMAIN), 
                'fields'  => array(
                    'tagline_subtitle' => array(
                        'type'   => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            		),
                )
            ),//tagline_subtitle sec
            
    		'tagline_button' => array(
                'title'   => __('Tagline Button Title', TEXTDOMAIN),
                'tooltip' => __('Enter Tagline Button Title here.', TEXTDOMAIN), 
                'fields'  => array(
                    'tagline_button' => array(
                        'type'   => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            		),
                )
            ),//tagline_button sec
    		
            'tagline_button_url' => array(
                'title'   => __('Tagline Button Link', TEXTDOMAIN),
                'tooltip' => __('Enter tagline button URL.', TEXTDOMAIN), 
                'fields'  => array(
                    'tagline_button_url' => array(
                        'type'   => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            		),
                )
            ),//tagline_button_url sec
    	),
    );           

    $specialSettingsPanel = array(
        'title' => __('Special Intro Settings', TEXTDOMAIN),
        'sections' => array(
            'home_special_intro' => array(
                'title'   => __('Display special intro', TEXTDOMAIN),
                'tooltip' => __('Enable or disable special intro here.', TEXTDOMAIN), 
                'fields'  => array(
                    'home_special_intro' => array(
                        'type'   => 'switch',
                        'state0' => __('Disabled', TEXTDOMAIN),
                        'state1' => __('Enabled', TEXTDOMAIN),
                        'value'  => '1'
                    ),
                )
            ),//home_special_intro sec
            
        ),
    );
        
    $blogSettingsPanel = array(
        'title' => __('Blog Settings', TEXTDOMAIN),
        'sections' => array(
    
        	'blog_sidebar_position' => array(
                'title'   => __('Blog Sidebar Position', TEXTDOMAIN),
                'tooltip' => __('Here you can disable the sidebar or choose the sidebar position in blog page, archive page, single post page.', TEXTDOMAIN),
                'fields'  => array(
                    'blog_sidebar_position' => array(
                        'type' => 'visual-select',
                        'options' => array('none'=>0, /*'left-side'=>1,*/ 'right-side'=>2),
                        'class' => 'page-sidebar',
                        'value' => 2,
                    ),
                )
            ),//blog_sidebar_position sec
        ),
    );
    
    $portfolioSettingsPanel = array(
        'title' => __('Portfolio Settings', TEXTDOMAIN),
        'sections' => array(
            'portfolio_related_posts' => array(
                'title'   => __('Related Portfolios Count', TEXTDOMAIN),
                'tooltip' => __('Enter Related Portfolios Count Here. ', TEXTDOMAIN),
                'fields'  => array(
                    'portfolio_related_posts' => array(
                        'type' => 'range',
    					'value'	=> '6'
                    ),//portfolio_related_posts sec
				)
            ),//widget-areas sec
    
        ),
    );    
    
    $importSettingsPanel = array(
        'title' => __('Import Dummy Content', TEXTDOMAIN),
        'sections' => array(
            'import_dummy_data' => array(
                'title'   => __('Import Dummy Data', TEXTDOMAIN),
                'tooltip' => __('If you are new to wordpress or have problems creating posts or pages that look like the theme preview you can import dummy posts and pages here that will definitley help to understand how those tasks are done. Please note that this will override theme options as well, so be careful.', TEXTDOMAIN),
                'fields'  => array(
                    'import_dummy_data' => array(
                        'type'   => 'switch',
                        'state0' => __('Disabled', TEXTDOMAIN),
                        'state1' => __('Enabled', TEXTDOMAIN),
                        'value'  => '0'
                    ),//import_dummy_data sec
				)
            ),//import_dummy_data sec
    
        ),
    );    
        
    $advanceSettingsPanel = array(
        'title' => __('Advanced Settings', TEXTDOMAIN),
        'sections' => array(

    		'custom_javascript' => array(
                'title'   => __('Custom Javascript', TEXTDOMAIN),
                'tooltip' => __('Enter your custom javascript codes here.(eg Google Analytics)', TEXTDOMAIN),
                'fields'  => array(
                    'custom_javascript' => array(
                        'type' => 'textarea',
            			'value'=> ''            
                    ),//custom_javascript sec
				)
            ),//custom_javascript sec
    
    		'custom_css' => array(
                'title'   => __('Custom CSS', TEXTDOMAIN),
                'tooltip' => __('Enter your custom css codes here to override or add to the default stylesheet.', TEXTDOMAIN),
                'fields'  => array(
                    'custom_css' => array(
                        'type' => 'textarea',
            			'value'=> ''            
                    ),//custom_css sec
				)
            ),//custom_css sec
		),
    );    
    
    $latestportfolioSettingsPanel = array(
        'title' => __('Latest Portfolio Settings', TEXTDOMAIN),
        'sections' => array(
    		'home_latest_portfolio' => array(
                'title'   => __('Display Latest Portfolio', TEXTDOMAIN),
                'tooltip' => __('Enable or disable latest portfolio here. ', TEXTDOMAIN),
                'fields'  => array(
                    'home_latest_portfolio' => array(
                        'type'   => 'switch',
                        'state0' => __('Disabled', TEXTDOMAIN),
                        'state1' => __('Enabled', TEXTDOMAIN),
                        'value'  => '1'
                    ),
                )
            ),//home_latest_portfolio sec

    		'home_latest_portfolio_title' => array(
                'title'   => __('Latest Portfolio Title', TEXTDOMAIN),
                'tooltip' => __('Enter Latest Portfolio Title Here.', TEXTDOMAIN), 
                'fields'  => array(
                    'home_latest_portfolio_title' => array(
                        'type' => 'text',
            			'class' => 'field-spacer',
            			'value'	=> '',
            		),
                )
            ),//home_latest_portfolio_title sec
    		
            'home_latest_portfolio_desc' => array(
                'title'   => __('Latest Portfolio Description', TEXTDOMAIN),
                'tooltip' => __('Enter Description of Latest Portfolio here.', TEXTDOMAIN), 
                'fields'  => array(
                    'home_latest_portfolio_desc' => array(
                        'type' => 'textarea',
            			'class' => 'field-spacer',
            			'value'	=> '',
            		),
                )
            ),//home_latest_portfolio_desc sec
    	),
    );    
    
    $panels = array(
        'general'			=> $generalSettingsPanel,
        'social'			=> $socialSettingsPanel,
        'header'			=> $headerSettingsPanel,
        'footer'			=> $footerSettingsPanel,
    	'portfolio'			=> $portfolioSettingsPanel,
    	'special-intro'		=> $specialSettingsPanel,
    	'blog'				=> $blogSettingsPanel,
    	'contact'			=> $contactSettingsPanel,
    	'tagline'			=> $taglineSettingsPanel,
    	'advance'			=> $advanceSettingsPanel,
    	'import'			=> $importSettingsPanel,
    	'latest-portfolio'	=> $latestportfolioSettingsPanel,
    	'home-navigation'	=> $homenavigationSettingsPanel,
    	'first'				=> $firstSettingsPanel,
    	'second'			=> $secondSettingsPanel,
    	'third'				=> $thirdSettingsPanel,
        'fourth'			=> $fourthSettingsPanel,
    );

    $tabs = array(
        'general'	=> array( 'text' => __('General', TEXTDOMAIN), 'panel' => 'general'),
        'header'	=> array( 'text' => __('Header', TEXTDOMAIN), 'panel' => 'header'),
        'footer'	=> array( 'text' => __('Footer', TEXTDOMAIN), 'panel' => 'footer'),
        'social'	=> array( 'text' => __('Social', TEXTDOMAIN), 'panel' => 'social'),
        'portfolio'	=> array( 'text' => __('Portfolio', TEXTDOMAIN), 'panel' => 'portfolio'),
        'blog'		=> array( 'text' => __('Blog', TEXTDOMAIN), 'panel' => 'blog'),
        'contact'	=> array( 'text' => __('Contact Page', TEXTDOMAIN), 'panel' => 'contact'),

        'special-intro'   	=> array( 'text' => __('Special Intro', TEXTDOMAIN), 'panel' => 'special-intro'),
    	'home-navigation'	=> array( 'text' => __('Home Navigation', TEXTDOMAIN), 'panel' => 'home-navigation'),
    	'first'		=> array( 'text' => __('First Tab', TEXTDOMAIN), 'panel' => 'first'),
    	'second'	=> array( 'text' => __('Second Tab', TEXTDOMAIN), 'panel' => 'second'),
    	'third'		=> array( 'text' => __('Third Tab', TEXTDOMAIN), 'panel' => 'third'),
    	'fourth' 	=> array( 'text' => __('Fourth Tab', TEXTDOMAIN), 'panel' => 'fourth'),
    	'latest-portfolio'	=> array( 'text' => __('Latest Portfolio', TEXTDOMAIN), 'panel' => 'latest-portfolio'),
    	'tagline'   => array( 'text' => __('Tagline', TEXTDOMAIN), 'panel' => 'tagline'),
    
    	'advance'	=> array( 'text' => __('Settings', TEXTDOMAIN), 'panel' => 'advance'),

    	'import'	=> array( 'text' => __('Import Settings', TEXTDOMAIN), 'panel' => 'import'),
    );
    
       
    $tabGroups = array(
        'theme-settings' => array( 
        	'text' => __('Theme Settings', TEXTDOMAIN), 
        	'tabs' => array(
        		'general', 
        		'social', 
        		'header', 
        		'footer',
    			'blog', 
    			'portfolio',
    			'contact',
    			) 
    		),
        'home-page' => array( 
        	'text' => __('Home Page', TEXTDOMAIN), 
        	'tabs' => array(
    			'home-navigation',
    			'first', 
    			'second',
    			'third',
    			'fourth',
    			'latest-portfolio',
    			'special-intro',
    			'tagline',
    			) 
    		),    		
        'advance-settings' => array( 
        	'text' => __('Advanced Options', TEXTDOMAIN), 
        	'tabs' => array(
    			'advance',
    			) 
    		),    		
    		
    	'demo-content' => array( 
        	'text' => __('Demo Content', TEXTDOMAIN), 
        	'tabs' => array(
    			'import',
    			) 
    		),    		
    	);

    $settings = array(
        'document-url' => '#',
        'support-url' => '#',
        'tabs-title' => __('Theme Options', TEXTDOMAIN),
        'tab-groups' => $tabGroups,
        'tabs' => $tabs,
        'panels' => $panels,
    );

    return $settings;
}