<?php 

require_once('admin-base.php');
require_once('importer/importer.php');
require_once('importer/dummy-settings.php');
require_once( ABSPATH . 'wp-admin/includes/plugin.php' ); // Require plugin.php to use is_plugin_active() below

//Extended admin class
class Admin extends ThemeAdmin
{
	function Save_Options()
	{
		//Check for import dummy data option
		if( array_key_exists('import_dummy_data', $_POST) && 
		    $_POST['import_dummy_data'] == '1')
		{
			//Don't save anything just Import data
			$this->ImportDummyData();
			
			echo 'OK';
			die();
		}
		
		parent::Save_Options();
	}
	
	function ImportDummyData()
	{
		$wp_import = new WP_Import();
		//$wp_import->fetch_attachments = true;
		ob_start();
		$wp_import->import(THEME_ADMIN.'/importer/haxon.wordpress.2013-09-14.xml');
		ob_end_clean();//Prevents sending output to client
		
		//Import dummy data
		update_option(OPTIONS_KEY, Get_Dummy_Options());

		//Import Our LayerSlider Samples
		if (is_plugin_active('LayerSlider/layerslider.php')) { 
			$this->ImportOurSampleSlider('data.txt');
		}		

		//Import ShortCode Templates
		if (class_exists('WPBakeryVisualComposerAbstract')) {
			$this->ImportShortCodeTemplate('vc_templates.txt');
		}		
	}
	
	function ImportShortCodeTemplate($filename)
	{		
		$file = THEME_ADMIN."/importer/{$filename}";
		if (!file_exists($file)) {
			return;
		}

		$content = json_decode(base64_decode(file_get_contents($file)), true);
		update_option('wpb_js_templates', $content);
	}
	
	function ImportOurSampleSlider($filename)
	{
		$file = THEME_ADMIN."/importer/layarslider_sample/{$filename}";
		if (!file_exists($file)) {
			return;
		}
		
		// Base64 encoded, serialized slider export code				
		$sample_slider = json_decode(base64_decode(file_get_contents($file)), true);
		
		// Iterate over the sliders
		foreach($sample_slider as $sliderkey => $slider) {
	
			// Iterate over the layers
			foreach($sample_slider[$sliderkey]['layers'] as $layerkey => $layer) {
	
				// Change background images if any
				if(!empty($sample_slider[$sliderkey]['layers'][$layerkey]['properties']['background'])) {
					$sample_slider[$sliderkey]['layers'][$layerkey]['properties']['background'] = THEME_ADMIN_URI."/importer/layarslider_sample/".basename($layer['properties']['background']);
				}
	
				// Change thumbnail images if any
				if(!empty($sample_slider[$sliderkey]['layers'][$layerkey]['properties']['thumbnail'])) {
					$sample_slider[$sliderkey]['layers'][$layerkey]['properties']['thumbnail'] = THEME_ADMIN_URI."/importer/layarslider_sample/".basename($layer['properties']['thumbnail']);
				}
	
				// Iterate over the sublayers
				if(isset($layer['sublayers']) && !empty($layer['sublayers'])) {
					foreach($layer['sublayers'] as $sublayerkey => $sublayer) {
	
						// Only IMG sublayers
						if($sublayer['type'] == 'img') {
							$sample_slider[$sliderkey]['layers'][$layerkey]['sublayers'][$sublayerkey]['image'] = THEME_ADMIN_URI."/importer/layarslider_sample/".basename($sublayer['image']);
						}
					}
				}
			}
		}
	
		// Get WPDB Object
		global $wpdb;
	
		// Table name
		$table_name = $wpdb->prefix . "layerslider";
	
		// Append duplicate
		foreach($sample_slider as $key => $val) {
	
			// Insert the duplicate
			$wpdb->query(
				$wpdb->prepare("INSERT INTO $table_name
									(name, data, date_c, date_m)
								VALUES (%s, %s, %d, %d)",
								$val['properties']['title'],
								json_encode($val),
								time(),
								time()
				)
			);
		}
	}
	
	function Enqueue_Scripts()
	{
		wp_enqueue_script('jquery');  
		wp_enqueue_script('thickbox');  
		wp_enqueue_style('thickbox');  
		wp_enqueue_script('media-upload');
		wp_enqueue_script('jquery-sortable');
		wp_enqueue_script('jquery-easing');
		wp_enqueue_style('nouislider');
		wp_enqueue_script('nouislider');
		wp_enqueue_style('colorpicker0');
		wp_enqueue_script('colorpicker0');
		wp_enqueue_style('chosen');
		wp_enqueue_script('chosen');
		wp_enqueue_style('theme-admin-css');
		wp_enqueue_script('theme-admin-script');
	}
}

new Admin();