<?php

function MemberDetail($id = null)
{
	if ($id == null) {
		$id = get_the_ID();
	}
	$terms = get_the_terms( get_the_ID(), 'job-title' );		
?>
	<div class="member" id="member-<?php the_ID(); ?>">
		<div class="span3">
			<div class="member-image">
			<?php 
			if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) {
				the_post_thumbnail('team-member');				
			} 
			?>
			</div>
		</div>
		<div class="span7">
			<div class="title">
				<h1><?php the_title(); ?></h1>
				<div class="separator2"></div>
				<div class="subtitle"><?php PrintTerms($terms, ', '); ?></div>
			</div>
			<div class="content">
				<div class="text"><?php the_content(); ?></div>
				<div class="separator"></div>
				<div class="links">
					<?php 
						$optKey = array(
							'social_facebook_address',  'social_twitter_address', 'social_dribbble_address',
							'social_vimeo_address', 'social_youtube_address', 'social_googleplus_address',
							'social_digg_address', 'social_tumblr_address', 'social_linkedin_address',
							'social_forrst_address', 
							'social_lastfm_address', 'social_flickr_address', 'social_myspace_address', 
							'rss_feed_address');
					
						$textKey = array(
							__('Facebook', TEXTDOMAIN), __('Twitter', TEXTDOMAIN), __('Dribbble', TEXTDOMAIN), 
							__('Vimeo', TEXTDOMAIN), __('YouTube', TEXTDOMAIN), __('Google+', TEXTDOMAIN), 
							__('Digg', TEXTDOMAIN), __('Tumblr', TEXTDOMAIN), __('Linkedin', TEXTDOMAIN), 
							__('Forrst', TEXTDOMAIN),  
							__('last.fm', TEXTDOMAIN), __('Flickr', TEXTDOMAIN), __('Myspace', TEXTDOMAIN), 
							__('RSS Feed', TEXTDOMAIN));
							
						GetAllMetaSocialLink($optKey, $textKey);
					?>
				</div>
			</div>
		</div>
	</div>
<?php 
}

function DisplayMemberItem($id = null, $thumbImageSize = 'team-member', $mainClass = "span3") 
{
	if ($id == null) {
		$id = get_the_ID();
	}
	$terms = get_the_terms( get_the_ID(), 'job-title' );		
?>
	<div class="<?php echo $mainClass; ?>" data-id="item-<?php the_ID(); ?>">
		<div class="member">
			<a href="<?php the_permalink(); ?>">
				<div class="member-image">
					<?php 
					if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) {
						the_post_thumbnail($thumbImageSize);				
					} 
					?>
					<div class="overlay"></div>
				</div>
				<div class="title">
					<h4><?php the_title(); ?></h4>
					<div class="subtitle"><?php PrintTerms($terms, ', '); ?></div>
				</div>
			</a>
			<div class="content">
				<p class="text"><?php the_content(); ?></p>
				<div class="separator"></div>
				<div class="links">
					<?php 
						$optKey = array(
							'social_facebook_address',  'social_twitter_address', 'social_dribbble_address',
							'social_vimeo_address', 'social_youtube_address', 'social_googleplus_address',
							'social_digg_address', 'social_tumblr_address', 'social_linkedin_address',
							'social_forrst_address', 
							'social_lastfm_address', 'social_flickr_address', 'social_myspace_address', 
							'rss_feed_address');
					
						$textKey = array(
							__('Facebook', TEXTDOMAIN), __('Twitter', TEXTDOMAIN), __('Dribbble', TEXTDOMAIN), 
							__('Vimeo', TEXTDOMAIN), __('YouTube', TEXTDOMAIN), __('Google+', TEXTDOMAIN), 
							__('Digg', TEXTDOMAIN), __('Tumblr', TEXTDOMAIN), __('Linkedin', TEXTDOMAIN), 
							__('Forrst', TEXTDOMAIN),  
							__('last.fm', TEXTDOMAIN), __('Flickr', TEXTDOMAIN), __('Myspace', TEXTDOMAIN), 
							__('RSS Feed', TEXTDOMAIN));
							
						GetAllMetaSocialLink($optKey, $textKey);
					?>
				</div>
			</div>
		</div>
	</div>
	<?php 
}