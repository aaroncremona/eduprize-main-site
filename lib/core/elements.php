<?php

function TagLine($title, $text, $link, $buttontitle)
{
?>
	<div class="tagline">
		<div class="content">
			<div class="title"><?php echo $title; ?></div>
			<div class="subtitle"><?php echo $text; ?></div>
		</div>
		<a class="button medium" href="<?php echo $link; ?>"><?php echo $buttontitle; ?></a>
		<div class="clear"></div>
	</div>
<?php 
}
?>