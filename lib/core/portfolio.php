<?php

function PortfolioDetail($id = null)
{
	if ($id == null) {
		$id = get_the_ID();
	}
		
	setPostViews($id);		
	?>
	<div id="post-<?php the_ID(); ?>" class="row portfolio portfolio_single">
        <div class="row">
            <div  class="span12 portfolio_image">
                <div class="navigator">
                    <a href="<?php echo get_permalink(get_adjacent_post(false,'',true)); ?>" class="icon next"></a>
                    <a href="<?php echo get_permalink(get_adjacent_post(false,'',false)); ?>" class="icon previous"></a>
                </div>
                <div class="nav-text"><?php _e('Next/Prev Award', TEXTDOMAIN); ?></div>	
                <div class="clear"></div>
            </div>
            <div class="span12 portfolio-content">
                <h2><?php the_title(); ?></h2>
                <div class="post_content">
                    <?php echo RemoveLastMarginOfPortfolio(_get_the_content()); ?>
                </div>
            </div>
        </div>
	</div>
<?php 
	PortfolioDetailRelateds($id);
}

function PortfolioDetailRelateds($id = null)
{
	if ($id == null) {
		$id = get_the_ID();
	}

	$related = get_related_posts_by_taxonomy($id, 'skill-type', intval(opt('portfolio_related_posts')) );
		
	if($related->have_posts())
	{
	?>
	<div class="related_project"> 
		<div class="heading">
			<?php _e('Related Projects', TEXTDOMAIN); ?>
		</div>	
		<ul id="mycarousel" class="jcarousel">
		<?php
			while ($related->have_posts()) 
			{ 
				$related->the_post();
				?>
				<li>
					<?php DisplayPortfolioItem(get_the_ID(), 'portfolio-related', 'item'); ?>				
				</li>
				<?php
			}
		?>
		</ul>
	</div>
	<?php
	}//End if	
	wp_reset_query();
}

function Portfolio($includeCats, $postCount, $galleryKind, $addRowDiv = true, $orderby = NULL, $order = 'DESC', $catBy = 'id', $displaySubnavigation = true, $useIsotope = true) 
{
	$spanClass = "span12";
	if ($galleryKind == 'gallery2')
		$spanClass = "span8";

	$useCats  = false;
	$list_cats_args = array('title_li' => '', 'taxonomy' => 'skill-type', 'walker' => new Portfolio_Walker());	
	
	if(gettype($includeCats) == 'array' && count($includeCats))
    {
    	$useCats = true;
        $list_cats_args['include'] = implode(',', GetTermIdsBySlug('skill-type', $includeCats));
	}
	
	if ($galleryKind == 'gallery1') 
	{		
		if ($displaySubnavigation)
	    {
		?>
		<div class="row">
	    	<div class="<?php echo $spanClass; ?>">
	        <!-- Subnavigation Categories -->
	        <?php
				if( !$useCats || count($includeCats) > 1 )
		        {?>
					<ul id="filters" class="subnavigation">
		            	<li class="current"><a href="#" data-filter="*"><?php _e('All', TEXTDOMAIN); ?></a></li>
						<?php wp_list_categories($list_cats_args); ?>
					</ul>
		<?php   }
	         ?>            		
			</div>
		</div>
		<!-- Subnavigation Categories End -->
		<div class="row">
			<div class="separator1 span12"></div>
		</div>
<?php
	    }
	} 
	if ($addRowDiv) {
	?>            		
	<div class="row">
	<?php } ?>
		<div class="<?php echo $spanClass; ?>">
			<!-- Gallery -->
			<div class="gallery <?php echo $galleryKind; if ($galleryKind == 'gallery1' && $useIsotope) echo ' isotope'; ?>">
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$queryArgs = array(
                	'post_type'      => 'portfolio',
                    'posts_per_page' => $postCount,
                    'paged' => $paged
                );

				if($useCats)
                {
                	$queryArgs['tax_query'] =  array(
                    	//	Note: tax_query expects an array of arrays!
                    	array(
                        	'taxonomy' => 'skill-type',
                        	'field'    => $catBy,
                        	'terms'    => $includeCats
                         ));
                }

				if($orderby != NULL)
                {
            		$queryArgs['orderby'] = $orderby;
        			$queryArgs['order'] = $order;
                }
                
                $count = 1;
                $query = new WP_Query($queryArgs);

                $size = 'thumb-portfolio';
                if ($galleryKind == 'gallery2')
					$size = 'thumb-portfolio-2';                
				while ($query->have_posts())
				{ 
					$query->the_post();
					
					if ($galleryKind == 'gallery2') {
						if (($count % 2) == 1) { ?> 
						<div class="row">
	<?php 				}	?>
						<div class="span4">
					<?php
					} 					
							PortfolioItem(get_the_ID(), $size);
					
					if ($galleryKind == 'gallery2') { ?>
						</div>
<?php
						if (($count % 2) == 0) { ?> 
						</div>
	<?php 				}
					}
															
					$count++;
				}
				if ($galleryKind == 'gallery2') {
					if (($count % 2) == 0) { ?> 
					</div>
	<?php 			} ?>
					<div class="clear"></div>
<?php 			} ?>
			</div>
            <?php
                get_pagination($query);
                wp_reset_query();
            ?>
		</div>
<?php
	if ($addRowDiv) {
	?>            		
	</div>
<?php }
}

function PortfolioItem($id = null, $thumbImageSize = 'thumb-portfolio') 
{
	if ($id == null) {
		$id = get_the_ID();
	}
	
	$terms = get_the_terms($id, 'skill-type');
?>
	<div class="item music isotope-item <?php if($terms) { foreach ($terms as $term) { echo 'term-'.$term->term_id.' '; } } ?>" data-id="item-<?php the_ID(); ?>">
		<div class="item_box">
			<div class="item_image">
				<?php 
				if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) {
					the_post_thumbnail($thumbImageSize);				
				} 
				?>
				<div class="frame_overlay">
				<?php 
					$images = GetPostImages($id);	
					foreach ($images as $v) {									
				?>
					<a class="hover_icon" data-rel="prettyPhoto[gallery_<?php the_ID(); ?>]" href="<?php echo $v; ?>"></a>
				<?php 
					}
				?>
				</div>
			</div>
			<a class="content" href="<?php the_permalink(); ?>">
				<h4 class="title"><?php the_title(); //echo GetFirstNChar(get_the_title(), 28); ?></h4>
			</a>
		</div>
	</div>
	<?php 
}

function DisplayPortfolioItem($id = null, $thumbImageSize = 'thumb-portfolio', $mainClass = "span3") 
{
	if ($id == null) {
		$id = get_the_ID();
	}
?>
	<div class="<?php echo $mainClass; ?>" data-id="item-<?php the_ID(); ?>">
		<div class="item_box">
			<div class="item_image">

				<?php 
				if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) {
					the_post_thumbnail($thumbImageSize);				
				} 
				?>
				<!--<div class="frame_overlay">
					<a class="hover_icon" href="<?php the_permalink(); ?>">Go to <?php the_title(); //echo GetFirstNChar(get_the_title(), 28); ?></a>
				</div>		-->

				<a class="frame_overlay" href="<?php the_permalink(); ?>">Go to <?php the_title(); //echo GetFirstNChar(get_the_title(), 28); ?></a>
			</div>
			<!-- commented by cremona - this is the original content - removed to remediate double link accessibility error
			<a class="content" href="<?php the_permalink(); ?>"> -->
				<h4 class="title"><?php the_title(); //echo GetFirstNChar(get_the_title(), 28); ?></h4>
			<!-- </a> -->
		</div>
	</div>
	<?php 
}
