<?php

function DisplayPostDetail($id = null, $blogClass = 'span8', $isFullWidth = false)
{
	if ($id == null) 
	{
		$id = get_the_ID();
	}
?>
	<div class="<?php echo $blogClass; ?> blog1 blog_detail">
	<?php 
	if ($id != null)
	{
		setPostViews($id);
	?>										
	<div class="blog_post" id="post-<?php the_ID(); ?>">
		<div class="post_date">
			<span class="day"><?php $dt = explode('-', get_the_date('j-F-Y')); echo $dt[0]; ?></span>
			<span class="month"><?php echo $dt[1]." ".$dt[2]; ?></span>
		</div>
		<div class="clear hidden-desktop"></div>
		<a class="post_head" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		<div class="clear"></div>
		<div class="post_meta">
			<span class="author left"><?php the_author_posts_link(); ?></span>
			<?php if(has_tag()){ ?>
			<div class="clear visible-phone"></div>
			<span class="tag2 left"><?php the_tags(''); ?></span>
			<?php } ?>
			<div class="clear visible-phone"></div>
			<span class="comment_icon"><?php echo get_comments_number($id); ?></span>
			<div class="clear"></div>
		</div>
		<?php the_content(); ?>
		<div class="pagelink">
			<?php wp_link_pages(); ?>
		</div>
		<?php comments_template('', true); ?>
	</div>
	<?php
	}
	else
	{
		?>
		<div id="post-0" >
			<p><?php _e("Sorry, but you are looking for something that isn't here.", TEXTDOMAIN) ?></p>
		</div>
	<?php 
	} ?>
</div>	
<?php 
}

function DisplayCurrentBlogPosts($blogClass = 'span8', $isFullWidth = false)
{
	?>
	<div id="posts" class="<?php echo $blogClass; ?> blog blog1">
	<?php 
	if (have_posts())
	{
		while (have_posts())
		{
			the_post();
	?>
		<div class="blog_post" id="post-<?php the_ID(); ?>">
			<div class="post_date">
				<span class="day"><?php $dt = explode('-', get_the_date('j-F-Y')); echo $dt[0]; ?></span>
				<span class="month"><?php echo $dt[1]." ".$dt[2]; ?></span>
			</div>
			<div class="clear hidden-desktop"></div>
			<a class="post_head" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<div class="clear"></div>
			<?php
			$max_width = '';
			$size = 'post-thumbnail';
			if ($isFullWidth) {
				$size = 'post-full-thumbnails';
				$max_width = " style='max-width:100%;' ";
			}			
			if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) 
			{
			?>
			<div class="post_image" <?php echo $max_width; ?>>	
				<?php the_post_thumbnail($size); ?>						
			</div>
			<?php 
			} 
			?>
			<div class="post_meta">
				<span class="author left"><?php the_author_posts_link(); ?></span>
				<?php if(has_tag()){ ?>
				<div class="clear visible-phone"></div>
				<span class="tag2 left"><?php the_tags(''); ?></span>
				<?php } ?>
				<div class="clear visible-phone"></div>
				<span class="comment_icon"><?php echo get_comments_number(get_the_ID()); ?></span>
				<div class="clear"></div>
			</div>
			<div class="content">
				<?php the_content(__('Continue Reading', TEXTDOMAIN)); ?>
			</div>
		</div>
		<div class="separator"></div>
<?php 
		}  
	}
	else
	{ ?>
		<div id="post-0" style="margin-bottom: 700px;">
			<p><?php _e("Sorry, but you are looking for something that isn't here.", TEXTDOMAIN) ?></p>
		</div>
	<?php 
	}
	get_pagination();
	?>
	</div>
<?php 
}




function DisplayCurrentBlogPostsEvents($blogClass = 'span8', $isFullWidth = false)
{
	?>
	<div id="posts" class="<?php echo $blogClass; ?> blog blog1">
	<?php 
	if (have_posts())
	{
		while (have_posts())
		{
			the_post();
	?>
		<div class="blog_post" id="post-<?php the_ID(); ?>">
			<div class="post_date">
			
			<?php
                $event_date = get_post_meta( get_the_ID() , 'event_date', true );
				$dt = date('j-M-Y',$event_date); 
				$dt = explode('-', $dt);
			
				?>
			
			
				<span class="day"><?php echo $dt[0]; ?></span>
				<span class="month"><?php echo $dt[1]." ".$dt[2]; ?></span>
			</div>
			<div class="clear hidden-desktop"></div>
			<a class="post_head" href="javascript:void(0);"><?php the_title(); ?></a>
			<div class="clear"></div>
			<?php
			$max_width = '';
			$size = 'post-thumbnail';
			if ($isFullWidth) {
				$size = 'post-full-thumbnails';
				$max_width = " style='max-width:100%;' ";
			}			
			if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) 
			{
			?>
			<div class="post_image" <?php echo $max_width; ?>>	
				<?php the_post_thumbnail($size); ?>						
			</div>
			<?php 
			} 
			?>
			
			<div class="content">
				<?php the_content(__('Continue Reading', TEXTDOMAIN)); ?>
			</div>
		</div>
		<div class="separator"></div>
<?php 
		}  
	}
	else
	{ ?>
		<div id="post-0" style="margin-bottom: 700px;">
			<p><?php _e("Sorry, but you are looking for something that isn't here.", TEXTDOMAIN) ?></p>
		</div>
	<?php 
	}
	get_pagination();
	?>
	</div>
<?php 
}



function DisplayBlogPosts($blogClass = 'span8', $includeCats, $postCount, $orderby = NULL, $order = 'DESC')
{
	$useCats  = false;
	$list_cats_args = array('title_li' => '', 'taxonomy' => 'category');	
	
	if(gettype($includeCats) == 'array' && count($includeCats))
    {
    	$useCats = true;
        $list_cats_args['include'] = implode(',', $includeCats);
	}

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$queryArgs = array(
    	'post_type' => 'post',
		'posts_per_page' => $postCount,
        'paged' => $paged
	);

	if($useCats)
    {
    	$queryArgs['tax_query'] =  array(
        //	Note: tax_query expects an array of arrays!
        	array(
            	'taxonomy' => 'category',
				'field'    => 'slug',
                'terms'    => $includeCats
            ));
    }

	if($orderby != NULL)
    {
    	$queryArgs['orderby'] = $orderby;
        $queryArgs['order'] = $order;
    }
                
    $count = 1;
    $query = new WP_Query($queryArgs);
	?>
	<div id="posts" class="<?php echo $blogClass; ?> blog blog1">
	<?php 
	if ($query->have_posts())
	{
		while ($query->have_posts())
		{
			$query->the_post();
			global $more;
			$more = 0;
	?>
		<div class="blog_post" id="post-<?php the_ID(); ?>">
			<div class="post_date">
				<span class="day"><?php $dt = explode('-', get_the_date('j-F-Y')); echo $dt[0]; ?></span>
				<span class="month"><?php echo $dt[1]." ".$dt[2]; ?></span>
			</div>
			<div class="clear hidden-desktop"></div>
			<a class="post_head" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<div class="clear"></div>
			<?php 
			if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) 
			{
			?>
			<div class="post_image">
				<?php the_post_thumbnail(); ?>						
			</div>
			<?php 
			} 
			?>
			<div class="post_meta">
				<span class="author left"><?php the_author_posts_link(); ?></span>
				<?php if(has_tag()){ ?>
				<div class="clear visible-phone"></div>
				<span class="tag2 left"><?php the_tags(''); ?></span>
				<?php } ?>
				<div class="clear visible-phone"></div>
				<span class="comment_icon"><?php echo get_comments_number(get_the_ID()); ?></span>
				<div class="clear"></div>
			</div>
			<div class="content">
				<?php the_content(__('Continue Reading', TEXTDOMAIN)); ?>
			</div>
		</div>
		<div class="separator"></div>
<?php 
		}  
	}
	else
	{ ?>
		<div id="post-0" >
			<p><?php _e("Sorry, but you are looking for something that isn't here.", TEXTDOMAIN) ?></p>
		</div>
	<?php 
	}
		get_pagination($query);
		wp_reset_postdata();
		//wp_reset_query();
	?>
	</div>
<?php 
}

function DisplayPostItem($id = null, $thumbImageSize = 'latest-posts') 
{
	if ($id == null) {
		$id = get_the_ID();
	}
?>
	<div class="span3" data-id="item-<?php the_ID(); ?>">
		<div class="item_box">
			<div class="item_image">
				<?php 
				if ( (function_exists('has_post_thumbnail')) && has_post_thumbnail()) {
					the_post_thumbnail($thumbImageSize);				
				} 
				?>
				<div class="frame_overlay">
					<a class="hover_icon" href="<?php the_permalink(); ?>"></a>
				</div>
			</div>
			<div class="content">
				<h4 class="title"><?php the_title(); ?></h4>
			</div>
		</div>
	</div>
	<?php 
}

function DisplayPostItemDetail($id = null, $thumbImageSize = 'top-blog-post') 
{
	if ($id == null) {
		$id = get_the_ID();
	}
?>
    <!-- featured image not implemented
	<div class="span1">
		<div class="review_image_right">
		<?php 
			if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
				the_post_thumbnail($thumbImageSize);
		?> 
		</div>
	</div>
    -->
	<div class="span5">
		<div class="content">
			<div class="meta left">
				<h4><?php the_title(); ?></h4>
			</div>	
			<div class="post_date">
				<span class="day"><?php $dt = explode('-', get_the_date('j-F-Y')); echo $dt[0]; ?></span>
				<span class="month"><?php echo $dt[1]." ".$dt[2]; ?></span>
			</div>
			<div class="clear"></div>
			<div class="content">
				<?php the_content(__('Continue Reading', TEXTDOMAIN)); ?>
			</div>
		</div>
	</div>
<?php 
}


function DisplayPostItemDetailEvent($id = null, $thumbImageSize = 'top-blog-post') 
{
	if ($id == null) {
		$id = get_the_ID();
	}
?>
    <!-- featured image not implemented
	<div class="span1">
		<div class="review_image_right">
		<?php 
			if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
				the_post_thumbnail($thumbImageSize);
		?> 
		</div>
	</div>
    -->
	<div class="span5">
		<div class="content">
			<div class="meta left">
				<h4><?php the_title(); ?></h4>
			</div>	
			<div class="post_date">
				
				<?php if($id!=''){ ?>
				<span class="day">
				
				<?php
                $event_date = get_post_meta( $id , 'event_date', true );
				$dt = date('j-M-Y',$event_date); 
				$dt = explode('-', $dt);
				echo $dt[0];
				?>
				</span>
				<span class="month"><?php echo $dt[1]." ".$dt[2]; ?></span>
				<?php } ?>
			</div>
			<div class="clear"></div>
			<div class="content">
				<?php the_content(__('Continue Reading', TEXTDOMAIN)); ?>
			</div>
		</div>
	</div>
<?php 
}
