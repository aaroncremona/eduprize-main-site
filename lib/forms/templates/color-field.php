<?php
$name     = $vars['key'];
$settings = $vars['settings'];
$class    = array_value('class', $settings);//Optional value
$placeholder = array_value('placeholder', $settings);//Optional value
?>

<div class="field clear-after  <?php echo $class; ?>">
    <input name="<?php echo $name; ?>" type="text" value="<?php echo esc_attr( $this->GetValueDef($name, $settings) ); ?>" class="colorinput" placeholder="<?php echo $placeholder; ?>" />
</div>