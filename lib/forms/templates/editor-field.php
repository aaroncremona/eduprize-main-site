<?php
$name     = $vars['key'];
$settings = $vars['settings'];
$id = array_value('id', $settings);//Optional value
$autop = array_value('autop', $settings);//Optional value
$class    = array_value('class', $settings);//Optional value
$title = array_value('title', $settings);//Optional value

$config = array(
	'textarea_name' => $name,
    'editor_class' => $class,
    'wpautop' => (!empty($autop)) ? $autop : true
);
?>
<div class="field text-input">
	<label for="field-<?php echo $name; ?>"><?php echo $title; ?></label><br/>
	<?php wp_editor($this->GetValueDef($name, $settings), $id, $config); ?>
</div>