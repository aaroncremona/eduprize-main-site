<?php
$name     = $vars['key'];
$settings = $vars['settings'];
$class    = array_value('class', $settings);//Optional value
$label    = array_value('label', $settings);//Optional value
$content    = array_value('content', $settings);//Optional value
?>

<div class="field text-input">
    <?php if($label != ''){ ?>
        <label for="field-<?php echo $name; ?>"><?php echo $label; ?></label>
    <?php } ?>
    <div class="<?php echo $class; ?>">
    	<?php echo /*esc_attr*/( $content ); ?>
    </div>
</div>