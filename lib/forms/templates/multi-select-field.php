<?php
$name = $vars['key'];
$settings = $vars['settings'];
$selected = $this->GetValue($name);
$options  = $settings['options'];
$class        = array_value('class', $settings);
$placeholder = array_value('placeholder', $settings);
$selectAttrs  = array_value('attributes', $settings);
$optionsAttrs = array_value('option-attributes', $settings, array());

$size    = array_value('size', $settings);//Optional value
$sizeAttrib = "";
if($size != '')
	$sizeAttrib = " size='{$size}' "; 
?>

<div>
	<select name="<?php echo $name. '[]'; ?>" <?php echo $sizeAttrib; ?> multiple="" data-placeholder="<?php echo $placeholder; ?>" class="<?php echo $class; ?>" <?php echo $selectAttrs; ?>>
	<?php
    	foreach($options as $value => $text)
		{
        	$selectedAttr = in_array($value, $selected) ? 'selected="selected"' : '';
            $attrs = array_key_exists($value, $optionsAttrs) ? $optionsAttrs[$value] : '';
    		?>
    		<option value="<?php echo esc_attr($value); ?>" <?php echo "$selectedAttr $attrs"; ?>><?php  echo $text; ?></option>
			<?php
        }
    ?>
	</select>
</div>