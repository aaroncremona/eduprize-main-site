<?php
$name     = $vars['key'];
$settings = $vars['settings'];
$options  = $settings['options'];
$class    = array_value('class', $settings);
$values = $this->GetValue($name);
if ($values == '' || count($values) == 0)
{
	$values = array_keys($options); 
}
?>
<div class="field <?php echo $class; ?>">
	<ul class="sortable grid">	    
	    <?php
	    foreach($values as $item)
	    {
	    	$key = $item;
	    	$value = $options[$item];
		?>
	        <li id="sort_<?php echo $key; ?>" class="<?php echo $value; ?>" style="margin: 5px;">
	        	<span><?php echo $value; ?></span>
 				<input type="hidden" name="<?php echo $name; ?>[]" value="<?php echo $key; ?>" />	        	
	        </li>
	    <?php
	    }
	    ?>
	</ul>
</div>