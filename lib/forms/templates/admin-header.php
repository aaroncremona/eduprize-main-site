<!--Header-->
<div id="px-head" class="clear-after">
    <div class="logo">
        <?php echo $this->GetImage('logo.png', THEME_NAME); ?>
    </div>

    <ul class="support">
        <li><a href="<?php echo $this->template['support-url']; ?>"><?php _e('Support', TEXTDOMAIN); ?></a></li>
        <li><div class="separator"></div></li>
        <li><a href="<?php echo $this->template['document-url']; ?>"><?php _e('Documentation', TEXTDOMAIN); ?></a></li>
    </ul>
</div>
<!--End Header-->