<?php
$name     = $vars['key'];
$settings = $vars['settings'];
$class    = array_value('class', $settings);//Optional value
$placeholder = array_value('placeholder', $settings);//Optional value
$row = array_value('row', $settings);//Optional value
if($row == '')
	$row = 10
?>

<div class="field textarea-input <?php echo $class; ?>">
    <textarea name="<?php echo $name; ?>" cols="30" rows="<?php echo $row; ?>" placeholder="<?php echo $placeholder; ?>" ><?php echo esc_textarea($this->GetValueDef($name, $settings)); ?></textarea>
</div>