<div id="px-main">

<?php
$panels = $this->template['panels'];

foreach($panels as $panelKey => $panel)
{
?>
    <div id="<?php echo $panelKey; ?>" class="panel">
        <div class="content-head">
            <h3><?php echo $panel['title']; ?></h3>
            <a href="#" class="save-button" >
				Save
                <div class="tooltip">
                    <div><?php _e('CLICK', TEXTDOMAIN); ?></div>
                </div>
            </a>
        </div>

        <?php echo $this->GetTemplate('section.php', $panel['sections']); ?>

    </div>
<?php
}
?>
</div>