<?php

require_once('ivalueprovider.php');

class PostOptionsProvider implements IValueProvider {

    public function GetValue($key)
    {
        global $post;
        return get_post_meta( $post->ID, $key, true );
    }
    
    public function GetValueDef($key, $settings)
    {
        global $post;        
        $value = get_post_meta( $post->ID, $key, true );
		
        if ($value == NULL || $value == '')
			$value = array_value('value', $settings);

		return $value;
    }
}