<?php

interface IValueProvider {
    public function GetValue($key);
    public function GetValueDef($key, $settings);
}