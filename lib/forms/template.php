<?php


//Base class for generating html code from
//given template file
class Template
{
    protected $templatesDir  = 'templates';
    /* @var IValueProvider $valueProvider */
    private   $valueProvider = null;

    function __construct(IValueProvider $valueProvider, $templatesDir = '')
    {
        if($templatesDir != '')
            $this->templatesDir = $templatesDir;

        $this->valueProvider = $valueProvider;
    }

    function GetValue($key)
    {
        return $this->valueProvider->GetValue($key);
    }

    function GetValueDef($key, $settings)
    {
        return $this->valueProvider->GetValueDef($key, $settings);
    }
    
    function SetWorkingDirectory($dir)
    {
        $this->templatesDir = $dir;
    }

    function GetTemplate($file, $vars = array())
    {
        ob_start();
        require(path_combine($this->templatesDir, $file));
        return ob_get_clean();
    }

    public function GetField($key, array $settings, array $vars=null)
    {
        $params = array('key' => $key, 'settings' => $settings);

        if($vars != null)
            $params = array_merge($vars, $params);

        return $this->GetTemplate($settings['type'] . '-field.php', $params);
    }

}