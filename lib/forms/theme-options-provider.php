<?php

require_once('ivalueprovider.php');

class ThemeOptionsProvider implements IValueProvider {
    public function GetValue($key)
    {
        return opt($key);
    }
    public function GetValueDef($key, $settings)
    {
        global $post;        
        $value = opt($key);
		
        if ($value == NULL || $value == '')
        {
			$value = $settings;
        	if (is_array($settings) )
				$value = array_value('value', $settings);
        }
		
		return $value;
    }
    
}