<?php
//$resId  = portfolio, slider, ...
function enqueueResources($resId) 
{
	registerResources();
	
	switch ($resId)
	{
		case 'portfolio':
	        wp_enqueue_script('jquery-easing');
			
			wp_enqueue_style('ie-style-portfolio');
	        wp_enqueue_style('isotope');
	        wp_enqueue_script('isotope');

		    wp_enqueue_style('prettyPhoto');
	    	wp_enqueue_script('prettyPhoto');
			break;

		case 'portfolio2':
	        wp_enqueue_script('jquery-easing');
			
	        wp_enqueue_style('prettyPhoto');
	    	wp_enqueue_script('prettyPhoto');
	    	
	    	wp_enqueue_style('ie-style-portfolio');
	        //wp_enqueue_script('quicksand');
			break;
			
		case 'single-portfolio':
	        wp_enqueue_script('jquery-easing');
			
	        wp_enqueue_style('prettyPhoto');
	    	wp_enqueue_script('prettyPhoto');

	    	wp_enqueue_style('flexslider');
			wp_enqueue_script('flexslider');
			
			wp_enqueue_script('jcarousel');	        
	        break;
	}
}

function registerResources()
{
	wp_enqueue_style('style', get_bloginfo('stylesheet_url'), false, THEME_VERSION);	
	
	if(opt('responsive_layout'))
		wp_enqueue_style('responsive-style', THEME_CSS_URI . '/responsive.css', false, THEME_VERSION);
		
	//Enqueue default Scripts
	//wp_enqueue_script('jquery_172', THEME_JS_URI . '/jquery-1.7.2.min.js', false, '1.7.2', true );
    wp_enqueue_script('superfish', THEME_JS_URI . '/superfish.js', false, '1.0', true );		
		
	//Register Styles		
	wp_register_style('isotope', THEME_CSS_URI. '/isotope.css', false, '1.5.19', 'screen');
	wp_register_style('prettyPhoto', THEME_CSS_URI. '/prettyPhoto.css', false, '3.1.4', 'screen');
	wp_register_style('flexslider', THEME_CSS_URI. '/flexslider.css', false, '1.0', 'screen');
	wp_register_style('ie-style-portfolio', THEME_CSS_URI. '/ie.css.css', false, '1.0', 'screen');
	global $wp_styles; 
	$wp_styles->add_data('ie-style-portfolio', 'conditional', 'IE');	

	//Register scripts
	wp_register_script( 'idTabs', THEME_JS_URI . '/jquery.idTabs.min.js', false, '2.2', true );
	wp_register_script( 'isotope', THEME_JS_URI . '/jquery.isotope.min.js', false, '1.5.19', true );
	wp_register_script( 'prettyPhoto', THEME_JS_URI . '/jquery.prettyPhoto.js', false, '3.1.4', true );	
	wp_register_script( 'jquery-easing', THEME_JS_URI . '/jquery.easing.1.3.js', false, '1.3', true );	
	wp_enqueue_script('jquery-easing');
	
	wp_register_script( 'gmap', THEME_JS_URI . '/jquery.gmap-1.1.0-min.js', false, '1.1.0', true );
	wp_register_script( 'jcarousel', THEME_JS_URI . '/jquery.jcarousel.min.js', false,'1.0',true );
	wp_register_script( 'flexslider', THEME_JS_URI . '/jquery.flexslider-min.js',false, '1.0', true);	
}

function theme_scripts()
{
	registerResources();
	    
    $pageSettings = array();
    
    //Include page specific scripts
    if(is_singular('portfolio'))
	{
	    wp_enqueue_style('prettyPhoto');
    	wp_enqueue_script('prettyPhoto');
		
		wp_enqueue_style('flexslider');
		wp_enqueue_script('flexslider');
		
		wp_enqueue_script('jcarousel');
	}
    //Google Map script for contact page
    elseif(is_page_template('template-contact.php'))
    {
		wp_enqueue_script('gmap');
    }
    //Load isotope/prettyPhoto on portfolio page
    elseif(is_page_template('template-portfolio.php'))
    {
    	//Isotope Plugin
        wp_enqueue_style('ie-style-portfolio');
        wp_enqueue_style('isotope');
        wp_enqueue_script('isotope');
	  	wp_enqueue_style('prettyPhoto');
    	wp_enqueue_script('prettyPhoto');
    }
    elseif(is_page_template('template-portfolio-2.php'))
    {
	    wp_enqueue_style('prettyPhoto');
    	wp_enqueue_script('prettyPhoto');
    	wp_enqueue_style('ie-style-portfolio');
        wp_enqueue_script('quicksand');
    }    
	//About template
	elseif(is_page_template('template-about.php'))
	{
		wp_enqueue_script('jcarousel');
	}
	elseif(is_page_template('template-home.php'))
	{
		wp_enqueue_script('idTabs');
	}
    elseif(is_page_template('template-full-width.php'))
    {
    }
    elseif(is_page() && !is_page_template())//page.php
    {
    }
    elseif (is_single()) 
    {
    }
        
    wp_enqueue_script('custom', THEME_JS_URI . '/custom.js', false, THEME_VERSION, true);
    wp_localize_script('custom', 'theme_uri', array('img' => THEME_IMAGES_URI));
    wp_localize_script('custom', 'theme_strings', array('contact_submit'=>__('Send', TEXTDOMAIN) ) );

    //Custom settings
    if(count($pageSettings))
        wp_localize_script('custom', 'page_settings', $pageSettings);

	Ajax_Load_Posts_Init();
}

add_action('wp_enqueue_scripts', 'theme_scripts');


function my_inline_script() {
  if ( wp_script_is( 'some-script-handle', 'done' ) && (opt('custom_javascript') != '')) {
 ?>
<script type="text/javascript">
	<?php echo stripslashes(opt('custom_javascript')); ?>
</script>
<?php
  }
}
add_action( 'wp_footer', 'my_inline_script' );


//Register Google Fonts
function wpse_google_webfonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    $query_args = array(
        'family' =>  opt('google_font_family') ,
    );

    wp_enqueue_style('google-webfonts',
        add_query_arg($query_args, "{$protocol}://fonts.googleapis.com/css" ),
        array(), null);
}

add_action( 'wp_enqueue_scripts', 'wpse_google_webfonts' );

function my_styles_method() {

	wp_enqueue_style(
		'custom-style',
		THEME_CSS_URI . '/options.css'
	);
	
	$google_font = opt('google_font');
	$theme_main_color = opt('theme_main_color');
	$theme_main_color_rgb = implode(hex2rgb($theme_main_color), ',');
	$custom_css = opt('custom_css');
	
	$font_css = "body , .btn , .text_btn , blockquote , #comment_form , #comment_form .textarea_input textarea  , #respond .textarea_input textarea ,#comment_form .button_submit input , #respond  .button_submit input , 
.post-pagination span ,
.post-pagination a ,
.special_intro .titles li .circle ,
.special_intro .titles li a
{
  font-family:'{$google_font}', sans-serif;
}";
	
	$main_css = "a , .colored , .sf-menu li:hover>a , .subnavigation li.current a , .subnavigation li a:hover
	, #home-navigation .tab_head span:hover , .error .colored ,  .right_sidebar .arrow2_list li:hover a , 
	.right_sidebar .widget-title , .special_intro .heading , .member .title h4 , .member .links a:hover ,
	.testimonial .meta .name
{
  color:{$theme_main_color};
}
.sf-menu ul > li:hover , #top_button , #footer .foot , .button:hover , .messageBox1 , 
.post-pagination a:hover , .post-pagination .this-page , #home-navigation .steps .hover , 
#home-navigation .steps .circle:hover .hover , .portfolio .navigator .previous:hover , 
.portfolio .navigator .next:hover , .tagline , .latest_blogs li.current .date , .member .member-image .overlay ,
.tab2 .tab_head .selected .tab_end,.tab2 .tab_head .selected .tab_text 
{
	background-color: {$theme_main_color};
}
.sf-menu li.active > a {
	border-bottom:4px solid {$theme_main_color};
}
.sf-menu li.active ul li a:hover{
	background-color: {$theme_main_color};
	color: #fff !important;
}
.subnavigation li.current
{
	border-bottom: 2px solid {$theme_main_color};
}
#home-navigation .tab .selected span
{
  background-color: {$theme_main_color};
  color: #fff;	
}
#home-navigation .steps .circle .outer-border
{
	border: 3px solid {$theme_main_color};
}
.item_box:hover , .latest_blogs li:hover .date
{
	background-color: {$theme_main_color};
	border-bottom: 2px solid {$theme_main_color};
}
.tab_head a
{
	border-bottom:1px solid {$theme_main_color};
}
.tab2 .tab_last .selected .tab_end,.tab2 .tab_last .selected .tab_text
{
	background-color: {$theme_main_color} !important
}
.item_box .item_image .frame_overlay
{
	background: rgba({$theme_main_color_rgb},0.7);
}
a:hover
{
  color: rgba({$theme_main_color_rgb},0.6);
}";
		
	if($theme_main_color != '') { 		
		wp_add_inline_style( 'custom-style', $main_css);
	}

	if($google_font != '') { 		
		wp_add_inline_style( 'custom-style', $font_css);
	}
	
	if($custom_css != '') { 		
		wp_add_inline_style( 'custom-style', $custom_css );
	}
}

add_action( 'wp_enqueue_scripts', 'my_styles_method' );


function clean_header(){
	wp_deregister_script( 'comment-reply' );
}

add_action('init','clean_header');

function Ajax_Load_Posts_Init()
{
	if( is_singular() || (is_home() /*&& !opt('blog_load_more')*/) ) 
		return;
	
	global $wp_query;
	
	// What page are we on? And what is the pages limit?
	$max = $wp_query->max_num_pages;
	$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
	
	// Add some parameters for the JS.
	wp_localize_script(
		'custom',
		'paged_data',
		array(
			'startPage' => $paged,
			'maxPages' => $max,
			'nextLink' => next_posts($max, false),
			'loadingText' => __('Loading...', TEXTDOMAIN),
			'loadMoreText' => __('Load More', TEXTDOMAIN),
			'noMorePostsText' => __('No More Posts', TEXTDOMAIN)
		)
	);
}

if ( !function_exists('register_sidebar') )
    return;
					
register_sidebar(array(
    'name' => 'Main Sidebar',
    'before_widget' => '<div id="%1$s" class="widget arrow2_list %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<span class="widget-title">',
    'after_title' => '</span>',
));

register_sidebar(array(
    'name' => 'Page Sidebar',
    'before_widget' => '<div id="%1$s" class="widget arrow2_list %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<span class="widget-title">',
    'after_title' => '</span>',
));

register_sidebar(array(
    'id'   => 'contact-sidebar',
    'name' => 'Contact Page Sidebar',
    'before_widget' => '<div id="%1$s" class="widget arrow2_list %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<span class="widget-title">',
    'after_title' => '</span>',
));

register_sidebar(array(
    'id'   => 'footer-widget-1',
    'name' => 'First Footer Widget Area',
    'before_widget' => '<div id="%1$s" class="widget arrow2_list %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4><div class="footer_separator"></div>',
));
register_sidebar(array(
    'id'   => 'footer-widget-2',
    'name' => 'Second Footer Widget Area',
    'before_widget' => '<div id="%1$s" class="widget arrow2_list %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4><div class="footer_separator"></div>',
));
register_sidebar(array(
    'id'   => 'footer-widget-3',
    'name' => 'Third Footer Widget Area',
    'before_widget' => '<div id="%1$s" class="widget arrow2_list %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4><div class="footer_separator"></div>',
));
register_sidebar(array(
    'id'   => 'footer-widget-4',
    'name' => 'Fourth Footer Widget Area',
    'before_widget' => '<div id="%1$s" class="widget arrow2_list %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4><div class="footer_separator"></div>',
));
