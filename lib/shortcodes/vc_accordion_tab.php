<?php
$output = $title = '';

extract(shortcode_atts(array(
	'title' => __("Section", TEXTDOMAIN)
), $atts));

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_accordion_section group', $this->settings['base']);
$_content = ($content=='' || $content==' ') ? __("Empty section. Edit page to add content here.", TEXTDOMAIN) : wpb_js_remove_wpautop($content);

$output = <<<OUTPUT
	<div class="accordion accordion1 {$css_class}">
		<div class="accordion_header">
			<h3 class="accordion_title">
				<span>-</span>
				<a href="#">{$title}</a>
			</h3>
			<div class="clear"></div>
		</div>
		<div class="accordion_content content_pad">
			<p>{$_content}</p>
		</div>		
	</div>
OUTPUT;

echo $output. $this->endBlockComment('.wpb_accordion_section') . "\n";