<?php

class WPBakeryShortCode_portfolio extends WPBakeryShortCode {

    protected function content( $atts, $content = null ) {
        $title = $useisotope = $displaysubnavigation = $gallerykind = $postcount = $includecats = $orderby = $order = '';

        extract(shortcode_atts(array(
            'title' => '',
            'gallerykind' => 'gallery1',
        	'useisotope' => '',
        	'displaysubnavigation' => '', 
            'postcount' => 8, // count per page
            'includecats' => '',
            'orderby' => NULL,
            'order' => 'DESC'
        ), $atts));

        $useisotope = ($useisotope!='' && $useisotope!='0') ? true : false;
        $displaysubnavigation = ($displaysubnavigation!='' && $displaysubnavigation!='0') ? true : false;
        
        $output = '';

        //$el_class = $this->getExtraClass($el_class);
        $width = '';//wpb_translateColumnWidthToSpan($width);
        
        if ($gallerykind == 'gallery1')
        	enqueueResources('portfolio');
        else if ($gallerykind == 'gallery2')
        		enqueueResources('portfolio2');

        if ( $postcount != '' && !is_numeric($postcount) ) $postcount = 8;

        $cats = explode(',', $includecats);
        
        ob_start();
        Portfolio($cats, $postcount, $gallerykind, true, $orderby, $order, 'slug', $displaysubnavigation, $useisotope);
        $output = ob_get_contents();
    	ob_end_clean();

        return $output;
    }
}

wpb_map( array(
    "base"		=> "portfolio",
    "name"		=> __(THEME_NAME." Portfolio", TEXTDOMAIN),
    "class"		=> "",
    "icon"      => "icon-pixflow",
	'category'	=> THEME_NAME,
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Widget title", TEXTDOMAIN),
            "param_name" => "title",
            "value" => "",
            "description" => __("Heading text. Leave it empty if not needed.", TEXTDOMAIN)
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Portfolio kind", TEXTDOMAIN),
            "param_name" => "gallerykind",
            "admin_label" => true,
            "value" => array(__("Gallery 1", TEXTDOMAIN) => "gallery1", __("Gallery 2", TEXTDOMAIN) => "gallery2"),
            "description" => __("Select portfolio kind.", TEXTDOMAIN)
        ),
        array(
			"type" => "checkbox",
		    "heading" => __("Use Isotope", TEXTDOMAIN),
	      	"param_name" => "useisotope",
	      	"value" => array(__('Yes', TEXTDOMAIN) => true),
            "dependency" => Array('element' => "gallerykind", 'value' => array('gallery1'))
        ),        
        array(
			"type" => "checkbox",
		    "heading" => __("Display Subnavigation", TEXTDOMAIN),
	      	"param_name" => "displaysubnavigation",
	      	"value" => array(__('Yes', TEXTDOMAIN) => true),
            "dependency" => Array('element' => "gallerykind", 'value' => array('gallery1'))
        ),        
        array(
            "type" => "textfield",
            "heading" => __("Portfolio count", TEXTDOMAIN),
            "param_name" => "postcount",
            "value" => "",
            "description" => __('How many protfolios to show per page?', TEXTDOMAIN)
        ),
        array(
			"type" => "checkbox",
		    "heading" => __("Include Skill Types", TEXTDOMAIN),
	      	"param_name" => "includecats",
	      	"value" => GetTermsForVC('skill-type'),
        	"admin_label" => true
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Order by", TEXTDOMAIN),
            "param_name" => "orderby",
            "value" => array( "", __("Date", TEXTDOMAIN) => "date", __("ID", TEXTDOMAIN) => "ID", __("Author", TEXTDOMAIN) => "author", __("Title", TEXTDOMAIN) => "title", __("Modified", TEXTDOMAIN) => "modified", __("Random", TEXTDOMAIN) => "rand", __("Menu order", TEXTDOMAIN) => "menu_order" ),
            "description" => __('Select how to sort retrieved portfolios. More at <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>.', 'js_composer')
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Order by", TEXTDOMAIN),
            "param_name" => "order",
            "value" => array( __("Descending", TEXTDOMAIN) => "DESC", __("Ascending", TEXTDOMAIN) => "ASC" ),
            "description" => __('Designates the ascending or descending order. More at <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>.', 'js_composer')
        ),        
     )
) );

?>