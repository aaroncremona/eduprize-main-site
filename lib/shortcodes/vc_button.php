<?php
$output = $size = $target = $href = /*$icon = $style = */$el_class = $title = $position = '';
extract(shortcode_atts(array(
    'size' => 'medium',
	//'icon'	=> '',
	//'style' => 'regular',
    'target' => '_self',
    'href' => '',
    'el_class' => '',
    'title' => __('Button Text', TEXTDOMAIN),
    'position' => ''
), $atts));
$a_class = '';

if ( $el_class != '' ) {
    $tmp_class = explode(" ", strtolower($el_class));
    $tmp_class = str_replace(".", "", $tmp_class);
    if ( in_array("prettyphoto", $tmp_class) ) {
        wp_enqueue_script( 'prettyphoto' );
        wp_enqueue_style( 'prettyphoto' );
        $a_class .= ' prettyphoto';
        $el_class = str_ireplace("prettyphoto", "", $el_class);
    }
    if ( in_array("pull-right", $tmp_class) && $href != '' ) { $a_class .= ' pull-right'; $el_class = str_ireplace("pull-right", "", $el_class); }
    if ( in_array("pull-left", $tmp_class) && $href != '' ) { $a_class .= ' pull-left'; $el_class = str_ireplace("pull-left", "", $el_class); }
}

if ( $target == 'same' || $target == '_self' ) { $target = ''; }
$target = ( $target != '' ) ? ' target="'.$target.'"' : '';

$position = ( $position != '' ) ? ' '.$position.'-button-position' : '';
$el_class = $this->getExtraClass($el_class);

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_button '.$el_class.$position, $this->settings['base']);

/*if ($style == 'regular')
{*/	
	if ( $href != '' ) {
	    $output = '<a class="button '.$size.' '.$a_class.'" title="'.$title.'" href="'.$href.'"'.$target.'>' . $title. '</a>';
	} else {
	    $output = '<a class="button '.$size.' '.$a_class.'" title="'.$title.'">' . $title. '</a>';
	}
/*}
else if ($style == 'circle') 
{
	$output = "
		<div class='steps' id='home-navigation'>
			<a href='{$href}' {$target} class='circle {$a_class}'>
				<div class='outer-border'></div>
				<span class='hover'></span>
				<span class='icon' style='background: url(\"{$icon}\") no-repeat scroll center 30px transparent;'></span>
			</a>
		</div>";
}*/

echo $output . $this->endBlockComment('button') . "\n";