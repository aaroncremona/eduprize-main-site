<?php

class WPBakeryShortCode_blogpost_item extends WPBakeryShortCode {

    protected function content( $atts, $content = null ) {
        $title = $postid = '';

        extract(shortcode_atts(array(
            'title' => '',
            'postid' => 0 
        ), $atts));

        $output = '';

        //$el_class = $this->getExtraClass($el_class);
        $width = '';//wpb_translateColumnWidthToSpan($width);
        
        if ( $postid != '' && !is_numeric($postid) ) $postid = 0;
        
        ob_start();
        
        $args = array(
			'p' => $postid,
			'post_type' => 'post'
        );
        
		$my_query = new WP_Query($args);
   		if ( $my_query->have_posts() ) { 
       		while ( $my_query->have_posts() ) { 
           		$my_query->the_post();           		
           		DisplayPostItem($postid);
       		}
   		}
   		wp_reset_postdata();
        $output = ob_get_contents();
    	ob_end_clean();

        return $output;
    }
}

wpb_map( array(
    "base"		=> "blogpost_item",
    "name"		=> __(THEME_NAME." BlogPost Item", TEXTDOMAIN),
    "class"		=> "",
    "icon"      => "icon-pixflow",
	'category'	=> THEME_NAME,
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Widget title", TEXTDOMAIN),
            "param_name" => "title",
            "value" => "",
            "description" => __("Heading text. Leave it empty if not needed.", TEXTDOMAIN)
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Select a Post", TEXTDOMAIN),
            "param_name" => "postid",
            "value" => GetPostsForVC('post'),
        	"admin_label" => true
        ),
     )
) );


?>