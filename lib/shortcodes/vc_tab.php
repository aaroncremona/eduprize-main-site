<?php
$output = $title = $tab_id = '';
extract(shortcode_atts($this->predefined_atts, $atts));

//wp_enqueue_script('idTabs');
//w1p_enqueue_script('jquery_ui_tabs_rotate');

$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tab_content ', $this->settings['base']);
$id = (empty($tab_id) ? sanitize_title( $title ) : $tab_id);
$_content = ($content=='' || $content==' ') ? __("Empty section. Edit page to add content here.", TEXTDOMAIN) : wpb_js_remove_wpautop($content);

$output = '<div class="'.$css_class.'" id="tab-'.$id.'">';
$output .= $_content;
$output .= '</div>';

echo $output. $this->endBlockComment('.wpb_tab');