<?php

class WPBakeryShortCode_blogposts extends WPBakeryShortCode {

    protected function content( $atts, $content = null ) {
        $title = $postcount = $includecats = $orderby = $order = '';

        extract(shortcode_atts(array(
            'title' => '',
            'postcount' => 8, // count per page
            'includecats' => '',
            'orderby' => NULL,
            'order' => 'DESC'
        ), $atts));

        $output = '';

        //$el_class = $this->getExtraClass($el_class);
        $width = '';//wpb_translateColumnWidthToSpan($width);
        
        if ( $postcount != '' && !is_numeric($postcount) ) $postcount = 8;

        $cats = explode(',', $includecats);
        
        ob_start();
        DisplayBlogPosts('span8', $cats, $postcount, $orderby, $order);
        $output = ob_get_contents();
    	ob_end_clean();

        return $output;
    }
}

wpb_map( array(
    "base"		=> "blogposts",
    "name"		=> __(THEME_NAME." BlogPosts", TEXTDOMAIN),
    "class"		=> "",
    "icon"      => "icon-pixflow",
	'category'	=> THEME_NAME,
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Widget title", TEXTDOMAIN),
            "param_name" => "title",
            "value" => "",
            "description" => __("Heading text. Leave it empty if not needed.", TEXTDOMAIN)
        ),
        array(
            "type" => "textfield",
            "heading" => __("post count", TEXTDOMAIN),
            "param_name" => "postcount",
            "value" => "",
            "description" => __('How many post to show per page?', TEXTDOMAIN),
        	"admin_label" => true
        ),
        array(
			"type" => "checkbox",
		    "heading" => __("Include Categories", TEXTDOMAIN),
	      	"param_name" => "includecats",
	      	"value" => GetTermsForVC('category'),
        	"admin_label" => true
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Order by", TEXTDOMAIN),
            "param_name" => "orderby",
            "value" => array( "", __("Date", TEXTDOMAIN) => "date", __("ID", TEXTDOMAIN) => "ID", __("Author", TEXTDOMAIN) => "author", __("Title", TEXTDOMAIN) => "title", __("Modified", TEXTDOMAIN) => "modified", __("Random", TEXTDOMAIN) => "rand", __("Menu order", TEXTDOMAIN) => "menu_order" ),
            "description" => __('Select how to sort retrieved blogposts. More at <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>.', 'js_composer')
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Order by", TEXTDOMAIN),
            "param_name" => "order",
            "value" => array( __("Descending", TEXTDOMAIN) => "DESC", __("Ascending", TEXTDOMAIN) => "ASC" ),
            "description" => __('Designates the ascending or descending order. More at <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>.', 'js_composer')
        ),        
     )
) );

?>