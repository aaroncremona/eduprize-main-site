<?php

class WPBakeryShortCode_testimonial extends WPBakeryShortCode {

    protected function content( $atts, $content = null ) {
        $info = $text = $name = '';

        extract(shortcode_atts(array(
        	'name'  => '',
        	'info'  => '',
        	'text' => ''
        ), $atts));

		$output = 
  			"<div class='testimonial'>
				<div class='content'>
                	{$text}
                	<div class='bottom'></div>
                </div>				
				<div class='meta'>
					<div class='name'>{$name}</div>
					<div class='info'>{$info}</div>
				</div>
			</div>";

		return $output;
    }
}

wpb_map( array(
    "base"		=> "testimonial",
    "name"		=> __("Testimonial", TEXTDOMAIN),
    "class"		=> "",
    "icon"      => "icon-pixflow",
	//'category'	=> 'Content',
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Name", TEXTDOMAIN),
            "param_name" => "name",
            "value" => "",
        	"admin_label" => true
        ),
        array(
            "type" => "textfield",
            "heading" => __("Info", TEXTDOMAIN),
            "param_name" => "info",
            "value" => "",
        	"admin_label" => true
        ),
		array(
	      "type" => "textarea",
	      //"holder" => "div",				  
	      "heading" => __("Text", TEXTDOMAIN),
	      "param_name" => "text",
	      "value" => __("I am text block. Click edit button to change this text.", TEXTDOMAIN)
	    ),
     )
) );
?>