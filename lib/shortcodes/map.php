<?php
$vc_is_wp_version_3_6_more = version_compare(preg_replace('/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo('version')), '3.6') >= 0;

$colors_arr = array(__("Grey", TEXTDOMAIN) => "wpb_button", __("Blue", TEXTDOMAIN) => "btn-primary", __("Turquoise", TEXTDOMAIN) => "btn-info", __("Green", TEXTDOMAIN) => "btn-success", __("Orange", TEXTDOMAIN) => "btn-warning", __("Red", TEXTDOMAIN) => "btn-danger", __("Black", TEXTDOMAIN) => "btn-inverse");

// Used in "Button" and "Call to Action" blocks
$size_arr = array(__("Regular size", TEXTDOMAIN) => "wpb_regularsize", __("Large", TEXTDOMAIN) => "btn-large", __("Small", TEXTDOMAIN) => "btn-small", __("Mini", TEXTDOMAIN) => "btn-mini");

$target_arr = array(__("Same window", TEXTDOMAIN) => "_self", __("New window", TEXTDOMAIN) => "_blank");

// Progress Bar

wpb_map( array(
  "name" => __("Progress Bar", TEXTDOMAIN),
  "base" => "vc_progress_bar",
  "icon" => "icon-wpb-graph",
  "category" => __('Content', TEXTDOMAIN),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", TEXTDOMAIN),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", TEXTDOMAIN)
    ),
    array(
      "type" => "exploded_textarea",
      "heading" => __("Graphic values", TEXTDOMAIN),
      "param_name" => "values",
      "description" => __('Input graph values here. Divide values with linebreaks (Enter). Example: 90|Development', TEXTDOMAIN),
      "value" => "90|Development,80|Design,70|Marketing"
    ),
    array(
      "type" => "textfield",
      "heading" => __("Units", TEXTDOMAIN),
      "param_name" => "units",
      "description" => __("Enter measurement units (if needed) Eg. %, px, points, etc. Graph value and unit will be appended to the graph title.", TEXTDOMAIN)
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Bar color", TEXTDOMAIN),
      "param_name" => "bgcolor",
      "value" => array("Theme Color" => "theme_main_color", __("Grey", TEXTDOMAIN) => "bar_grey", __("Blue", TEXTDOMAIN) => "bar_blue", __("Turquoise", TEXTDOMAIN) => "bar_turquoise", __("Green", TEXTDOMAIN) => "bar_green", __("Orange", TEXTDOMAIN) => "bar_orange", __("Red", TEXTDOMAIN) => "bar_red", __("Black", TEXTDOMAIN) => "bar_black", __("Custom Color", TEXTDOMAIN) => "custom"),
      "description" => __("Select bar background color.", TEXTDOMAIN),
      "admin_label" => true
    ),
    array(
      "type" => "colorpicker",
      "heading" => __("Bar custom color", TEXTDOMAIN),
      "param_name" => "custombgcolor",
      "description" => __("Select custom background color for bars.", TEXTDOMAIN),
      "dependency" => Array('element' => "bgcolor", 'value' => array('custom'))
    ),
    array(
      "type" => "checkbox",
      "heading" => __("Options", TEXTDOMAIN),
      "param_name" => "options",
      "value" => array(__("Add Stripes?", TEXTDOMAIN) => "striped", __("Add animation? Will be visible with striped bars.", TEXTDOMAIN) => "animated")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", TEXTDOMAIN),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", TEXTDOMAIN)
    )
  )
) );


/* Separator (Divider)
---------------------------------------------------------- */
wpb_map( array(
  "name"		=> __("Separator", TEXTDOMAIN),
  "base"		=> "vc_separator",
  'icon'		=> 'icon-wpb-ui-separator',
  "category"  => __('Content', TEXTDOMAIN),
  "controls"	=> 'popup_delete',
  "params" => array(
	array(
      "type" => "dropdown",
      "heading" => __("Style", TEXTDOMAIN),
      "param_name" => "style",
      "value" => array('Style 1'=> "separator", 'Style 2'=> "separator1"),
      "description" => __("Separator styles.", TEXTDOMAIN)
    ))
) );

/* 
 * Button
 */
wpb_map( array(
  "name" => __("Button", TEXTDOMAIN),
  "base" => "vc_button",
  "icon" => "icon-wpb-ui-button",
  "category" => __('Content', TEXTDOMAIN),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("URL (Link)", TEXTDOMAIN),
      "param_name" => "href",
      "description" => __("Button link.", TEXTDOMAIN)
    ),
	/*array(
      "type" => "dropdown",
      "heading" => __("Style", TEXTDOMAIN),
      "param_name" => "style",
      "value" => array('Regular'=> "regular", 'Circle'=> "circle"),
	),
    array(
      "type" => "attach_image",
      "heading" => __("Icon", TEXTDOMAIN),
      "param_name" => "icon",
      "value" => "",
      "description" => __("Select icon from media library.", TEXTDOMAIN),
      "dependency" => Array('element' => "style", 'value' => array('circle'))
    ),*/	
    array(
      "type" => "textfield",
      "heading" => __("Text on the button", TEXTDOMAIN),
      "holder" => "button",
      "class" => "wpb_button",
      "param_name" => "title",
      "value" => __("Text on the button", TEXTDOMAIN),
      "description" => __("Text on the button.", TEXTDOMAIN),
      //"dependency" => Array('element' => "style", 'value' => array('regular'))
    ),
	array(
      "type" => "dropdown",
      "heading" => __("Size", TEXTDOMAIN),
      "param_name" => "size",
      "value" => array('Small'=> "small", 'Medium'=> "medium", 'Large'=> "large"),
      //"dependency" => Array('element' => "style", 'value' => array('regular'))
	),
    array(
      "type" => "dropdown",
      "heading" => __("Target", TEXTDOMAIN),
      "param_name" => "target",
      "value" => $target_arr,
      "dependency" => Array('element' => "href", 'not_empty' => true)
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", TEXTDOMAIN),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", TEXTDOMAIN)
    )
  ),
  "js_view" => 'VcButtonView'
) );


/* Tabs
---------------------------------------------------------- */
$tab_id_1 = time().'-1-'.rand(0, 100);
$tab_id_2 = time().'-2-'.rand(0, 100);
wpb_map( array(
  "name"  => __("Tabs", TEXTDOMAIN),
  "base" => "vc_tabs",
  "show_settings_on_create" => false,
  "is_container" => true,
  "icon" => "icon-wpb-ui-tab-content",
  "category" => __('Content', TEXTDOMAIN),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", TEXTDOMAIN),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", TEXTDOMAIN)
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", TEXTDOMAIN),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", TEXTDOMAIN)
    )
  ),
  "custom_markup" => '
  <div class="wpb_tabs_holder wpb_holder vc_container_for_children">
  <ul class="tabs_controls">
  </ul>
  %content%
  </div>'
  ,
  'default_content' => '
  [vc_tab title="'.__('Tab 1', TEXTDOMAIN).'" tab_id="'.$tab_id_1.'"][/vc_tab]
  [vc_tab title="'.__('Tab 2', TEXTDOMAIN).'" tab_id="'.$tab_id_2.'"][/vc_tab]
  ',
  "js_view" => ($vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35')
) );




/* Call to Action Button
---------------------------------------------------------- */
wpb_map( array(
  "name" => __("Call to Action Button", TEXTDOMAIN),
  "base" => "vc_cta_button",
  "icon" => "icon-wpb-call-to-action",
  "category" => __('Content', TEXTDOMAIN),
  "params" => array(
    array(
      "type" => "textarea",
      'admin_label' => true,
      "heading" => __("Text", TEXTDOMAIN),
      "param_name" => "call_text",
      "value" => __("Click edit button to change this text.", TEXTDOMAIN),
      "description" => __("Enter your content.", TEXTDOMAIN)
    ),
    array(
      "type" => "textfield",
      "heading" => __("Text on the button", TEXTDOMAIN),
      "param_name" => "title",
      "value" => __("Text on the button", TEXTDOMAIN),
      "description" => __("Text on the button.", TEXTDOMAIN)
    ),
    array(
      "type" => "textfield",
      "heading" => __("URL (Link)", TEXTDOMAIN),
      "param_name" => "href",
      "description" => __("Button link.", TEXTDOMAIN)
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Target", TEXTDOMAIN),
      "param_name" => "target",
      "value" => $target_arr,
      "dependency" => Array('element' => "href", 'not_empty' => true)
    ),
    array(
      "type" => "colorpicker",
      "heading" => __("Color", TEXTDOMAIN),
      "param_name" => "color",
      "description" => __("Select custom color for button.", TEXTDOMAIN),
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Size", TEXTDOMAIN),
      "param_name" => "size",
      "value" => array('Small'=> "small", 'Medium'=> "medium", 'Large'=> "large"),
      "description" => __("Button size.", TEXTDOMAIN)
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", TEXTDOMAIN),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", TEXTDOMAIN)
    )
  ),
  "js_view" => 'VcCallToActionView'
) );
