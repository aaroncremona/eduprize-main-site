<?php

class WPBakeryShortCode_portfolio_detail extends WPBakeryShortCode {

    protected function content( $atts, $content = null ) {
        $title = $postid = '';

        extract(shortcode_atts(array(
            'title' => '',
            'postid' => 0 
        ), $atts));

        $output = '';

        //$el_class = $this->getExtraClass($el_class);
        $width = '';//wpb_translateColumnWidthToSpan($width);
        
        enqueueResources('single-portfolio');
        
        if ( $postid != '' && !is_numeric($postid) ) $postid = 0;
        
        $args = array(
			'p' => $postid,
			'post_type' => 'portfolio'
        );
        
		$my_query = new WP_Query($args);
        ob_start();
		if ( $my_query->have_posts() ) { 
       		while ( $my_query->have_posts() ) { 
           		$my_query->the_post();           		
           		PortfolioDetail($postid);
       		}
   		}
   		wp_reset_postdata();
        $output = ob_get_contents();
    	ob_end_clean();
    	
        return '<div class="container"><div id="main" class="container">'.$output.'</div></div>';
    }
}

wpb_map( array(
    "base"		=> "portfolio_detail",
    "name"		=> __(THEME_NAME." Portfolio Detail", TEXTDOMAIN),
    "class"		=> "",
    "icon"      => "icon-pixflow",
	'category'	=> THEME_NAME,
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Widget title", TEXTDOMAIN),
            "param_name" => "title",
            "value" => "",
            "description" => __("Heading text. Leave it empty if not needed.", TEXTDOMAIN)
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Select a Portfolio", TEXTDOMAIN),
            "param_name" => "postid",
            "value" => GetPostsForVC('portfolio'),
        	"admin_label" => true
        ),
     )
) );
?>