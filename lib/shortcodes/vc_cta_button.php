<?php
$output = $color = $size = $target = $href = $title = $call_text = $el_class = '';
extract(shortcode_atts(array(
    'color' => '',
    'size' => '',
    'target' => '',
    'href' => '',
    'title' => __('Button', TEXTDOMAIN),
    'call_text' => '',
    'el_class' => ''
), $atts));

$el_class = $this->getExtraClass($el_class);

if ( $target == 'same' || $target == '_self' ) { $target = ''; }
if ( $target != '' ) { $target = ' target="'.$target.'"'; }

$color = ( $color != '' ) ?  "background-color:{$color};": '';
$size = ( $size != '' ) ? $size : ' small';

$a_class = '';
if ( $el_class != '' ) {
    $tmp_class = explode(" ", $el_class);
    if ( in_array("prettyphoto", $tmp_class) ) {
        wp_enqueue_script( 'prettyphoto' );
        wp_enqueue_style( 'prettyphoto' );
        $a_class .= ' prettyphoto'; $el_class = str_ireplace("prettyphoto", "", $el_class);
    }
}

if ( $href != '' ) {
	$button = '<a class="button button2 '.$size.' '.$a_class.'" style="color:#000; margin-bottom: 4px; '.$color.'" title="'.$title.'" href="'.$href.'"'.$target.'>' . $title. '</a>';	
} else {
    $button = '';
    $el_class .= ' cta_no_button';
}
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_call_to_action wpb_content_element clearfix '.$el_class, $this->settings['base']);

$output .= '<div class="'.$css_class.'" style="background-color: '.opt('theme_main_color').'">';
$output .= apply_filters('wpb_cta_text', '<h2 class="wpb_call_text">'. $call_text . '</h2><br/>', array('content'=>$call_text));
$output .= $button;
$output .= '</div> ' . $this->endBlockComment('.wpb_call_to_action') . "\n";

echo $output;