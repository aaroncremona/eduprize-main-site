<?php

class WPBakeryShortCode_tagline extends WPBakeryShortCode {

    protected function content( $atts, $content = null ) {
        $title = $link = $text = $buttontitle = '';

        extract(shortcode_atts(array(
            'title' => '',
        	'link'  => '',
        	'buttontitle'  => '',
        	'text' => ''
        ), $atts));

        ob_start();        
		TagLine($title, $text, $link, $buttontitle);
        $output = ob_get_contents();
    	ob_end_clean();

		return $output;
    }
}

wpb_map( array(
    "base"		=> "tagline",
    "name"		=> __("TagLine", TEXTDOMAIN),
    "class"		=> "",
    "icon"      => "icon-pixflow",
	//'category'	=> 'Content',
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Title", TEXTDOMAIN),
            "param_name" => "title",
            "value" => "",
        ),
	    array(
	      "type" => "textfield",
	      "holder" => "div",
	      "heading" => __("Text", TEXTDOMAIN),
	      "param_name" => "text",
	      "value" => __("You can add some title text here and use it as info box", TEXTDOMAIN)
	    ),
        array(
            "type" => "textfield",
            "heading" => __("Button Title", TEXTDOMAIN),
            "param_name" => "buttontitle",
            "value" => "",
        ),
        array(
	        "type" => "textfield",
            "heading" => __("Button URL (Link)", TEXTDOMAIN),
            "param_name" => "link",
            "value" => "",
        ),
     )
) );
?>