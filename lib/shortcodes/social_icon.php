<?php

class WPBakeryShortCode_socialicon extends WPBakeryShortCode {

    protected function content( $atts, $content = null ) {
        $link = $type = $text = '';

        extract(shortcode_atts(array(
            'link' => '',
        	'type'  => 'twitter',
        	'text' => ''
        ), $atts));
        
		$output = "<div class='social_icons'><a href='{$link}' class='{$type}'>{$text}</a></div>";

		return $output;
    }
}

wpb_map( array(
    "base"		=> "socialicon",
    "name"		=> __("Social Icon", TEXTDOMAIN),
    "class"		=> "",
    "icon"      => "icon-pixflow",
	'category'	=> 'Social',
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Link (Url)", TEXTDOMAIN),
            "param_name" => "link",
            "value" => "",
        	"admin_label" => true,
        ),
		array(
	      	"type" => "dropdown",
	      	"heading" => __("Type", TEXTDOMAIN),
	      	"param_name" => "type",
			"admin_label" => true,
	      	"value" => array(
		      	'twitter'=> "twitter", 
		      	'dribbble'=> "dribbble", 
		      	'vimeo'=> "vimeo", 
		      	'youtube'=> "youtube", 
		      	'facebook'=> "facebook", 
		      	'google'=> "google", 
		      	'digg'=> "digg", 
		      	'tumblr'=> "tumblr", 
		      	'linkedin'=> "linkedin", 
		      	'forrst'=> "forrst", 
		      	'rss'=> "rss", 
		      	'lastfm'=> "lastfm", 
		      	'flickr'=> "flickr", 
		      	'myspace'=> "myspace", 
		      	'behance'=> "behance", 
		      	'deviantart'=> "deviantart", 
		      	'dropbox'=> "dropbox", 
		      	'evernote'=> "evernote", 
		      	'github'=> "github", 
		      	'instagram'=> "instagram", 
		      	'picassa'=> "picassa", 
		      	'pintrest'=> "pintrest", 
		      	'reddit'=> "reddit", 
		      	'skype'=> "skype", 
		      	'soundcloud'=> "soundcloud", 
		      	'xing'=> "xing", 
		      	'yahoo'=> "yahoo", 
			),
	    ),
        array(
            "type" => "textfield",
            "heading" => __("Text", TEXTDOMAIN),
            "param_name" => "text",
            "value" => "",
        ),
      )
) );

?>