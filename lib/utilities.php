<?php

function _get_the_content()
{
	ob_start();
	the_content();
	$output = trim(ob_get_contents());
	ob_end_clean();

	return $output;
}

function SpecialIntroModifyHeader($content)
{
	// change the header color
	if (substr($content, 0, 2) == "<h")
	{
		$h = $content[2];
		$posEndTag = strpos($content, ">");
		$pos = strpos($content, "</h".$h);
		if (!($pos === false))
		{			
			$endTagIdx = $posEndTag;
			for($i=$posEndTag+1; $i<= $pos;$i++) 
			{
				if($content[$i] == '>')
					$endTagIdx = $i;
				if($content[$i] == '<' && $content[$i+1] == '/')
					break;  
			}			
			
			$content = ""
			.substr($content, 0, $endTagIdx)
			." style='color: ".opt('theme_main_color')."' "
			.substr($content, $endTagIdx);
		}
	}

	return $content;
}

function RemoveLastMargin($content)
{
	$pos = strrpos($content, '<div class="row ');
	if (!($pos === false))
	{
		$content = ""
		.substr($content, 0, $pos)
		.'<div style="margin-bottom: 0px;" class="row '
		.substr($content, $pos+16);
	}

	return $content;
}

function RemoveLastMarginOfFullWidthPages($content)
{
	$startIdx = strrpos($content, 'row ');
	if (!($startIdx === false))
	{
		$pos = strpos($content, 'wpb_content_element', $startIdx);
		while (!($pos === false)) 
		{
			$content = ""
			.substr($content, 0, $pos)
			.'wpb_content_element1'
			.substr($content, $pos+19);
			
			$pos = strpos($content, 'wpb_content_element', $pos+19);			
		}
	}

	return $content;
}

function RemoveLastMarginOfPortfolio($content)
{
	$startIdx = strrpos($content, 'row ');
	if (!($startIdx === false))
	{
		$pos = strpos($content, 'vc_row-fluid', $startIdx);
		while (!($pos === false)) 
		{
			$content = ""
			.substr($content, 0, $pos)
			.'vc_row-fluid1'
			.substr($content, $pos+12);
			
			$pos = strpos($content, 'vc_row-fluid', $pos+12);			
		}
	}

	return $content;
}

function comment_fields($fields)
{
	$commenter = wp_get_current_commenter();

	$fields = array();

	$fields['author'] = "<div class=\"text_input\"><input name=\"author\" value=\"" . esc_attr($commenter['comment_author']) . "\" placeholder=\"" . __('Your Name', TEXTDOMAIN) . "\" type=\"text\"></div>";
	$fields['email'] = "<div class=\"text_input\"><input name=\"email\" value=\"" . esc_attr($commenter['comment_author_email']). "\" placeholder=\"" . __('Email Address (never published)', TEXTDOMAIN) . "\" type=\"text\"></div>";

	return $fields;
}
add_filter('comment_form_default_fields','comment_fields');

//Comment styling
function theme_comment($comment, $args, $depth) {

	$GLOBALS['comment'] = $comment; ?>

<li
	id="li-comment-<?php comment_ID(); ?>">
	<div class="comment" id="comment-<?php comment_ID(); ?>">
		<div class="hidden comment_id">
		<?php comment_ID(); ?>
		</div>
		<div class="comment_head">
			<div class="comment_image">
			<?php echo get_avatar(get_comment_ID(), $size='80', THEME_IMAGES_URI . '/commnet_avatar1.jpg'); ?>
			</div>
			<div class="meta">
				<cite><?php comment_author(); ?> </cite> <span class="says"><?php _e('says:', TEXTDOMAIN); ?>
				</span>
				<?php
				if ( $comment->user_id == get_the_author_meta('ID'))
				{
					?>
				<span class="author-tag"><?php _e('(Author)', TEXTDOMAIN); ?> </span>
				<?php
				}
				?>
				<br /> <a class="comment-date"
					href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s', TEXTDOMAIN), get_comment_date(),  get_comment_time()) ?>
				</a> <br /> <br />
				<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			</div>
		</div>
		<div class="comment_body">
		<?php comment_text() ?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div> <?php
}

function theme_list_pings($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	?> <li id="comment-<?php comment_ID(); ?>"><?php
	comment_author_link();
}

function add_comment_form_top() {
?>
	<p><?php _e('Your email address will not be published. Website Field Is Optional', TEXTDOMAIN); ?></p>
<?php 
}
add_action('comment_form_top', 'add_comment_form_top');

function _comment_form( $args = array(), $post_id = null ) {
		
	ob_start();
	comment_form($args, $post_id);
	$output = trim(ob_get_contents());
	ob_end_clean();
	
	$anchor = '<p class="form-submit">';
	$pos = strrpos($output, $anchor);
	
	if (!($pos === false)) {
		
		$offset = $pos+strlen($anchor);
		$pos = strpos($output, '</p>', $offset);		
		$output = ""
		.substr($output, 0, $offset)
		.'<a href="#" class="button small">'
		.substr($output, $offset, $pos-$offset)
		.'</a>'
		.substr($output, $pos);
	}
	
	echo $output;
}

function GetPostsForVC($post_type = 'post')
{
	$args['numberposts'] = -1;
	$args['orderby'] = 'post_date';
	$args['post_type'] = $post_type;
	
	$postslist = get_posts( $args );

	$result = array();
	foreach($postslist as $post)
		$result[$post->post_title] = $post->ID;
		
	return $result;
}

function GetPostsForAdmin($post_type = 'post')
{
	$args['numberposts'] = -1;
	$args['orderby'] = 'post_date';
	$args['post_type'] = $post_type;
	
	$postslist = get_posts( $args );

	$result = array();
	foreach($postslist as $post)
		$result[$post->ID] = $post->post_title;
		
	return $result;
}

function GetTermsForVC($taxonomies, $args = '') 
{
	$terms = get_terms($taxonomies, $args);
	$cats = array();
	if (!empty($terms) && !is_wp_error($terms)) 
	{
		foreach ( $terms as $tax ) 
			$cats[$tax->name] = $tax->slug;
}
	
	return $cats;
}

function GetTermsForAdmin($taxonomies, $args = '') 
{
	$terms = get_terms($taxonomies, $args);
	$cats = array();
	if (!empty($terms) && !is_wp_error($terms)) 
	{
		foreach ( $terms as $tax ) 
			$cats[$tax->slug] = $tax->name;
	}
	
	return $cats;
}

function SocialLink($optKey, $text, $class)
{
	if(opt($optKey) != '')
	{
	?>
		<li class="<?php echo $class; ?>"><a href="<?php eopt($optKey); ?>"><?php echo $text; ?></a></li>
<?php 
	}
}

function MetaSocialLink($optKey, $text, $class = "item")
{
    $meta = get_post_meta( get_the_ID(), $optKey, true );
    if($meta != '')
    {?>
    <a href="<?php echo $meta; ?>" class="<?php echo $class; ?>"><?php echo $text; ?></a>
    <?php
    }
}

function GetAllMetaSocialLink($optKey = array(), $text = array(), $class = "item", $sep = " / ")
{
	$result = array();
	for ($i=0; $i<count($optKey); $i++) 
	{
    	$meta = get_post_meta( get_the_ID(), $optKey[$i], true );
	    if($meta != '')
	    {
			$result[] = "<a class='{$class}' href='{$meta}'>{$text[$i]}</a>";
	    }
	}

	$res = implode($result, $sep);
	echo $res;
}

add_filter( 'body_class', 'add_body_class' );
function add_body_class( $class )
{
	if (is_page_template('template-portfolio.php') || is_page_template('template-portfolio-2.php'))
	{
	  if( is_array($class) )
	  {
	    $class[] = 'portfolio';
	    $class[] = 'pngfix';
	  }
	  else 
	  {
	    $class .= " portfolio pngfix";
	  }
	}
		
	if (is_singular('portfolio'))
	{
		if( is_array($class) )
		{
			$class[] = 'portfolio';
		}
		else
		{
			$class .= " portfolio ";
		}
	}	
	
	if (is_singular('team')) {
		if( is_array($class) ) {
		    $class[] = 'member-view';
		}
		else {
			$class .= " member-view";
		}
	} 

  	if( is_array($class) )
  	{  	  		
  		$class = DeleteFromArray('tag', $class);
  		$class = DeleteFromArray('search', $class);
  	}
  	else 
  	{
  		$cl = explode($class, ' ');
  		$cl = DeleteFromArray('tag', $cl);
  		$cl = DeleteFromArray('search', $cl);
  		$class = implode($cl, ' ');
  	}
	
    return $class;
}

//DISPLAY MAIN MENU
function main_menu($idPrefix = ''){

	//If this is WordPress 3.0 and above AND if the menu location registered in functions/register-wp3.php has a menu assigned to it
	if(function_exists('wp_nav_menu') && has_nav_menu('primary-nav')) 
	{
		$css_class = '';
		if ($idPrefix == '')
			$css_class = 'sf-menu';

		wp_nav_menu( array( 
				'container' =>'',
				'menu_class' => $css_class,
				'theme_location' => 'primary-nav', 
				'walker' => new Custom_Nav_Walker($idPrefix)) 
		);
	}
	//If either this is WP version<3.0 or if a menu isn't assigned, use wp_list_pages()
	else {
		echo '<ul class="sf-menu">';
		$mainMenu = wp_list_pages('echo=0&title_li=');
		$mainMenu = str_replace('</a>', '</a><div class="clear"></div>', $mainMenu);
		echo $mainMenu;
		echo '</ul>';	
	}		
}

// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return __("0 View", TEXTDOMAIN);
    }
    return $count.__(" Views", TEXTDOMAIN);
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
function posts_column_views($defaults){
    $defaults['post_views'] = __('Views', TEXTDOMAIN);
    return $defaults;
}
function posts_custom_column_views($column_name, $id){
	if($column_name === 'post_views'){
        echo getPostViews(get_the_ID());
    }
}

remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

/*add_filter('the_content_more_link', '_more_link', 10, 2);

function _more_link( $more_link, $more_link_text ) {
	global $post;
	$result = '<div class="button ">
		<a class="button1 button_arrow_right" href="'.get_permalink().'#more-'.$post->ID.'">'.$more_link_text.'</a>
	</div>';	
	return $result;
}*/

function get_pagination($query = null, $range = 4) {
    global $paged, $wp_query;

    $q = $query == null ? $wp_query : $query;

    // How much pages do we have?
    if ( !isset($max_page) ) {
        $max_page = $q->max_num_pages;
    }

    // We need the pagination only if there is more than 1 page
    if ( $max_page > 1 ) {
        if ( !$paged ) $paged = 1;

        echo '<div class="post-pagination">';

        // To the previous page
        if($paged > 1)
            echo '<a class="prev-page-link" href="' . get_pagenum_link($paged-1) . '">'. __('Prev', TEXTDOMAIN) .'</a>';

        if ( $max_page > $range + 1 ) :
            if ( $paged >= $range ) echo '<a href="' . get_pagenum_link(1) . '">1</a>';
            if ( $paged >= ($range + 1) ) echo '<span class="page-numbers">&hellip;</span>';
        endif;

        // We need the sliding effect only if there are more pages than is the sliding range
        if ( $max_page > $range ) {
            // When closer to the beginning
            if ( $paged < $range ) {
                for ( $i = 1; $i <= ($range + 1); $i++ ) {
                    echo ( $i != $paged ) ? '<a href="' . get_pagenum_link($i) .'">'.$i.'</a>' : '<span class="this-page">'.$i.'</span>';
                }
                // When closer to the end
            } elseif ( $paged >= ($max_page - ceil(($range/2))) ) {
                for ( $i = $max_page - $range; $i <= $max_page; $i++ ) {
                    echo ( $i != $paged ) ? '<a href="' . get_pagenum_link($i) .'">'.$i.'</a>' : '<span class="this-page">'.$i.'</span>';
                }
                // Somewhere in the middle
            } elseif ( $paged >= $range && $paged < ($max_page - ceil(($range/2))) ) {
                for ( $i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++ ) {
                    echo ($i != $paged) ? '<a href="' . get_pagenum_link($i) .'">'.$i.'</a>' : '<span class="this-page">'.$i.'</span>';
                }
            }
            // Less pages than the range, no sliding effect needed
        } else {
            for ( $i = 1; $i <= $max_page; $i++ ) {
                echo ($i != $paged) ? '<a href="' . get_pagenum_link($i) .'">'.$i.'</a>' : '<span class="this-page">'.$i.'</span>';
            }
        }
        if ( $max_page > $range + 1 ) :
            // On the last page, don't put the Last page link
            if ( $paged <= $max_page - ($range - 1) ) echo '<span class="page-numbers">&hellip;</span><a href="' . get_pagenum_link($max_page) . '">' . $max_page . '</a>';
        endif;

        // Next page
        if($paged < $max_page)
            echo '<a class="next-page-link" href="' . get_pagenum_link($paged+1) . '">'. __('Next', TEXTDOMAIN) .'</a>';

        echo '</div><!-- post-pagination -->';
    }
}


// retrieves the attachment ID from the file URL
function get_image_id($image_url) {
	global $wpdb;

	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM " .$wpdb->prefix. "posts WHERE guid='%s';", $image_url));
	
	if(count($attachment))
		return $attachment[0];
	else
		return -1;
}

function GetFirstNChar($content, $n = 200, $more = ' ...') 
{
	$n = intval($n);
	$len = strlen($content); 
	if($len <= $n)
	{
		//$content .= str_repeat('&nbsp; ', $n - $len);
		return $content;		
	}
	else
	{
		$n = $n -3;
		$matches = array();
		preg_match('/^.{'.$n.'}(?:.*?)\b/iu', strip_tags($content), $matches);
		$matches[] = '';//str_repeat('&nbsp; ', $n);
		$continued = $more;
		if (trim($matches[0]) == '')
			$continued = '';
		return $matches[0] . $continued;
	}
}

function Intro($title, $pageId = null, $def = false)
{
	if ($pageId == null)
		$pageId = get_the_ID();
		
	$show_intro = get_post_meta($pageId, 'show_intro', true);
	if (!$def && $show_intro != '' && !$show_intro )
		return false;	
?>
	<div id="intro">
		<div id="top"></div>
		<div class="container">
			<h2>
			<?php echo $title; ?>
			</h2>
			<div class="clear visible-phone"></div>
			<div class="search">
				<form action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<fieldset>
						<input type="text" value="<?php echo get_search_query(); ?>" name="s" placeholder="<?php echo esc_attr__('Search', TEXTDOMAIN); ?>" />
						<input type="submit" name="submit" value="" />
					</fieldset>
				</form>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div> 
	<?php
	return true;
}

function GetPostImages($id = null)
{
	if ($id == null)
	{
		$id = get_the_ID();
	}

	$images = get_post_meta($id, 'image', true);
	if ($images == '')
	{
		return array();
	}

	return $images;
}


function theme_before_header()
{
	// initialize hook
	do_action('theme_before_header');
}

function get_related_posts_by_taxonomy($postId, $taxonomy, $maxPosts = 9)
{
	$terms   = wp_get_object_terms($postId, $taxonomy);

	if (!count($terms))
	return new WP_Query();

	$termsSlug = array();

	foreach($terms as $term)
	$termsSlug[] = $term->slug;

	$args=array(
	  'post__not_in' => array($postId),
	  'post_type' => get_post_type($postId),
	  'showposts'=>$maxPosts,
	  'tax_query' => array(
	array(
				'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms' => $termsSlug
	)
	)
	);

	return new WP_Query($args);
}


//Return theme option
function opt($option){
	$opt = get_option(OPTIONS_KEY);

	$result = $opt[$option];
	if (!is_array($result))
	{
		$result = stripslashes($result);
	}
	
	return $result;
}

//Echo theme option
function eopt($option){
	echo opt($option);
}

function PrintTerms($terms, $separatorString)
{
	$termIndex = 1;
	if($terms)
	foreach ($terms as $term)
	{
		echo $term->name;

		if(count($terms) > $termIndex)
		{
			echo $separatorString;
		}	

		$termIndex++;
	}
}

function format_attr($name, $val)
{
	return $name . '="'. $val . '" ';
}

function format_std_attrs(array $params)
{
	$attrs =  '';
	$keys = array_keys($params);

	foreach ($keys as $key)
	{
		if($key != 'id' && $key != 'class' &&
		$key != 'style' && $key != 'src' &&
		$key != 'href' && $key != 'alt' &&
		$key != 'type')
		continue;

		$attrs .= format_attr($key, $params[$key]);
	}

	return $attrs;
}

//Returns a html image tag string
function img(array $params){

	if(!isset($params['file']))
	{
		throw new Exception('file parameter is missing.');
	}

	$params['src'] = THEME_IMAGES_URI . '/' . $params['file'];

	$tag = '<img ' . format_std_attrs($params) . '/>';

	echo $tag;
}

//Returns a html script tag string
function js(array $params){
	echo get_js($params);
}

function get_js(array $params){

	if(!isset($params['file']))
	{
		throw new Exception('file parameter is missing.');
	}

	$params['type'] = 'text/javascript';
	$params['src'] = THEME_JS_URI . '/' . $params['file'];

	return '<script ' . format_std_attrs($params) . '></script>';
}

/*
 * Gets array value with specified key, if the key doesn't exist
 * default value is returned
 */
function array_value($key, $arr, $default='')
{
	return array_key_exists($key, $arr) ? $arr[$key] : $default;
}


/*
 * Deletes attachment by given url
 */
function delete_attachment( $url ) {
	global $wpdb;

	// We need to get the image's meta ID.
	$query = "SELECT ID FROM wp_posts where guid = '" . esc_url($url) . "' AND post_type = 'attachment'";
	$results = $wpdb->get_results($query);

	// And delete it
	foreach ( $results as $row ) {
		wp_delete_attachment( $row->ID );
	}
}

/*
 * Converts array of slugs to corresponding array of IDs
 */
function slugs_to_ids($slugs, $taxonomy)
{
	$tempArr = array();
	foreach($slugs as $slug)
	{
		if(!strlen(trim($slug))) continue;
		$term = get_term_by('slug', $slug, $taxonomy);
		if(!$term) continue;
		$tempArr[] = $term->term_id;
	}

	return $tempArr;
}

function DeleteFromArray($needle, $array)
{
	$key = array_search($needle, $array);
	if( $key !== false)
	unset($array[$key]);

	return $array;
}

function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}
	$rgb = array($r, $g, $b);
	//return implode(",", $rgb); // returns the rgb values separated by commas
	return $rgb; // returns an array with the rgb values
}

function GetTermIdsBySlug($taxonomy, $slugs = array())
{
	$result = array();

	foreach ($slugs as $slug) {
		$term = get_term_by('slug', $slug, $taxonomy);
		if (!empty($term))
		$result[] = $term->term_id;
	}

	return $result;
}
