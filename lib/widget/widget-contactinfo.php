<?php

// Widget class
class PixFlow_ContactInfo_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
	 		THEME_SLUG . '_ContactInfo_Widget', // Base ID
			THEME_SLUG . ' - Contact Info Widget', // Name
			array( 'description' => __( 'Displays your contact information', TEXTDOMAIN ) ) // Args
		);
	}
		
	function widget( $args, $instance ) {
		extract( $args );

		// Our variables from the widget settings
		$title      = apply_filters('widget_title', $instance['title'] );
		$phone      = $instance['phone'];
		$cell       = $instance['cell'];
		$email      = $instance['email'];
		$address   = $instance['address'];

		// Before widget (defined by theme functions file)
		echo $before_widget;

		// Display the widget title if one was input
		if ( $title )
			echo $before_title . $title . $after_title;

		?>
		<div class="contact_info">
			<?php if (trim($address) != '') { ?>
			<span class="head"><?php _e('Address:', TEXTDOMAIN); ?></span><br />
			<span class="info"><?php echo $address; ?></span><br />
			<?php } ?>
			<br />
			<?php if (trim($phone) != '') { ?>
			<span class="head"><?php _e('Phone:', TEXTDOMAIN); ?></span><br />
			<span class="info"><?php echo $phone; ?><br />
			<?php } ?>
			<?php if (trim($cell) != '') { echo $cell; } ?></span><br />
			<br />
			<?php if (trim($email) != '') { ?>
			<span class="head"><?php _e('Email:', TEXTDOMAIN); ?></span><br />
			<a class="info" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
			<?php } ?>
		</div>		
		<?php

		// After widget (defined by theme functions file)
		echo $after_widget;
	}

		
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Strip tags to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['phone'] = strip_tags($new_instance['phone']);
		$instance['cell']    = strip_tags($new_instance['cell']);
		$instance['email']   = strip_tags($new_instance['email']);
		$instance['address'] = strip_tags($new_instance['address']);
		
		return $instance;
	}
		 
	function form( $instance ) {

		// Set up some default widget settings
		$defaults = array(
			'title' => 'Contact Info',
			'phone' => '',
			'cell'  => '',
			'email' => '',
			'address' => ''
		);
		
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<!-- Phone: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" />
		</p>
		
		<!-- Cell Phone: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'cell' ); ?>"><?php _e('Cell Phone', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'cell' ); ?>" name="<?php echo $this->get_field_name( 'cell' ); ?>" value="<?php echo $instance['cell']; ?>" />
		</p>
		
		<!-- Email: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e('eMail', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo $instance['email']; ?>" />
		</p>
			
		<!-- Address: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e('Address', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" value="<?php echo $instance['address']; ?>" />
		</p>		
		
		<?php
		}
}

// register widget
add_action( 'widgets_init', create_function( '', 'register_widget( "PixFlow_ContactInfo_Widget" );' ) );

?>