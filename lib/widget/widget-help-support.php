<?php

// Widget class
class PixFlow_HelpSupport_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
	 		THEME_SLUG . '_HelpSupport_Widget', // Base ID
			THEME_SLUG . ' - Help and Support Widget', // Name
			array( 'description' => __( 'Displays Help and Support information', TEXTDOMAIN ) ) // Args
		);
	}
		
	function widget( $args, $instance ) {
		extract( $args );

		// Our variables from the widget settings
		$title      = apply_filters('widget_title', $instance['title'] );
		$about      = $instance['about'];
		$terms      = $instance['terms'];
		$contact    = $instance['contact'];
		$faq   		= $instance['faq'];
		$portfolio  = $instance['portfolio'];
		
		// Before widget (defined by theme functions file)
		echo $before_widget."<div class='link-widget'>";

		// Display the widget title if one was input
		if ( $title )
			echo $before_title . $title . $after_title;

		?>
		<a class="link" href="<?php echo $faq; ?>"><?php _e('Read FAQ', TEXTDOMAIN); ?></a>
		<div class="separator"></div>
		<a class="link" href="<?php echo $contact; ?>"><?php _e('Contact Support', TEXTDOMAIN); ?></a>
		<div class="separator"></div>
		<a class="link" href="<?php echo $about; ?>"><?php _e('About us', TEXTDOMAIN); ?></a>
		<div class="separator"></div>
		<a class="link" href="<?php echo $terms; ?>"><?php _e('Terms of Use', TEXTDOMAIN); ?></a>
		<div class="separator"></div>
		<a class="link" href="<?php echo $portfolio; ?>"><?php _e('Portfolio', TEXTDOMAIN); ?></a>
		<div class="separator"></div>
		<?php

		// After widget (defined by theme functions file)
		echo '</div>'.$after_widget;
	}

		
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Strip tags to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['about'] = strip_tags($new_instance['about']);
		$instance['faq'] = strip_tags($new_instance['faq']);
		$instance['contact']    = strip_tags($new_instance['contact']);
		$instance['terms']   = strip_tags($new_instance['terms']);
		$instance['portfolio'] = strip_tags($new_instance['portfolio']);
		
		return $instance;
	}
		 
	function form( $instance ) {

		// Set up some default widget settings
		$defaults = array(
			'title' => 'Help and Support',
			'faq' => '#',
			'about' => '#',
			'contact' => '#',
			'terms' => '#',
			'portfolio' => '#',
		);
		
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
			<label for="<?php echo $this->get_field_id( 'faq' ); ?>"><?php _e('Read FAQ Link:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'faq' ); ?>" name="<?php echo $this->get_field_name( 'faq' ); ?>" value="<?php echo $instance['faq']; ?>" />
			<label for="<?php echo $this->get_field_id( 'contact' ); ?>"><?php _e('Contact Support Link:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'contact' ); ?>" name="<?php echo $this->get_field_name( 'contact' ); ?>" value="<?php echo $instance['contact']; ?>" />
			<label for="<?php echo $this->get_field_id( 'about' ); ?>"><?php _e('About Link:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'about' ); ?>" name="<?php echo $this->get_field_name( 'about' ); ?>" value="<?php echo $instance['about']; ?>" />
			<label for="<?php echo $this->get_field_id( 'terms' ); ?>"><?php _e('Terms of Use Link:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'terms' ); ?>" name="<?php echo $this->get_field_name( 'terms' ); ?>" value="<?php echo $instance['terms']; ?>" />
			<label for="<?php echo $this->get_field_id( 'portfolio' ); ?>"><?php _e('Portfolio Link:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'portfolio' ); ?>" name="<?php echo $this->get_field_name( 'portfolio' ); ?>" value="<?php echo $instance['portfolio']; ?>" />
		</p>
		<?php
		}
}

// register widget
add_action( 'widgets_init', create_function( '', 'register_widget( "PixFlow_HelpSupport_Widget" );' ) );

?>