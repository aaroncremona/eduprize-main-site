<?php

// Widget class
class PixFlow_Latest_Works_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
	 		THEME_SLUG . '_Latest_Works_Widget', // Base ID
			THEME_SLUG . ' - Latest Works Widget', // Name
			array( 'description' => __( 'Displays your latest items in portfolio', TEXTDOMAIN ) ) // Args
		);
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		// Our variables from the widget settings
		$title      = apply_filters('widget_title', $instance['title'] );
		$postcount  = intval($instance['count']);

		// Before widget (defined by theme functions file)
		echo $before_widget.'<div class="widget_latest_works">';

		// Display the widget title if one was input
		if ( $title )
			echo $before_title . $title . $after_title;	

		//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$queryArgs = array(
			'post_type'      => 'portfolio',
			'meta_key'		 => 'post_views_count',
			'orderby'		 => 'meta_value_num',
			'order'			 => 'DESC',
            'posts_per_page' => $postcount
        );	
		
		$query = new WP_Query($queryArgs);
		if( $query->have_posts()) 
		{
			while ($query->have_posts()) 
			{ 
				$query->the_post();  
				?>
			<div class="item">
				<a href="<?php the_permalink(); ?>">
					<?php 
					if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
						the_post_thumbnail('thumb-portfolio-widget'); 
					?>
					<span class="title"><?php the_title(); ?></span><br />
				</a>
				<span class="cat"><?php PrintTerms(get_the_terms( get_the_ID(), 'skill-type' ), ', '); ?></span><br />
				<span class="counter"><?php echo getPostViews(get_the_ID()); ?></span>
			</div>
		<?php 
			}				
		}//If have posts	
		wp_reset_query();
		
		// After widget (defined by theme functions file)
		echo '</div>'.$after_widget;
	}

		
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Strip tags to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['count'] = strip_tags( $new_instance['count'] );

		if(intval($instance['count']) > 10)
			$instance['count'] = 10;

		if(intval($instance['count']) < 1)
			$instance['count'] = 1;
		
		return $instance;
	}
		 
	function form( $instance ) {

		// Set up some default widget settings
		$defaults = array(
			'title' => 'Latest Works',
			'count' => '3'
		);
		
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Post Count: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e('Number Of Recent Works:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" value="<?php echo $instance['count']; ?>" />
		</p>

		<?php
		}
}

// register widget
add_action( 'widgets_init', create_function( '', 'register_widget( "PixFlow_Latest_Works_Widget" );' ) );

?>