<?php

// Widget class
class PixFlow_Contact_Info_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
	 		THEME_SLUG . '_Contact_Info_Widget', // Base ID
			THEME_SLUG . ' - Contact_Info Widget', // Name
			array( 'description' => __( 'Displays your contact information on contact page', TEXTDOMAIN ) ) // Args
		);
	}
		
	function widget( $args, $instance ) {
		extract( $args );

		// Our variables from the widget settings
		$title      = apply_filters('widget_title', $instance['title'] );
		$address1      = $instance['address1'];
		$address2      = $instance['address2'];
		$phone      = $instance['phone'];
		$fax       = $instance['fax'];
		$email      = $instance['email'];
		$sale_phone      = $instance['sale_phone'];
		$sale_fax       = $instance['sale_fax'];
		$sale_email      = $instance['sale_email'];
		
		// Before widget (defined by theme functions file)
		echo $before_widget;

		// Display the widget title if one was input
		if ( $title )
			echo $before_title . $title . $after_title;
		?>
		<!-- Contact info -->
		<p><?php echo $address1; ?></p>
		<p><?php echo $address2; ?></p>
		<p><?php 
			if (trim($phone) != '') 
			{
				_e('Phone:', TEXTDOMAIN); 
				echo " ".$phone; 
			}	
			?>
		</p>
		<p><?php 
			if (trim($fax) != '') 
			{
				_e('FAX:', TEXTDOMAIN); 
				echo " ".$fax; 
			}	
		
		?></p>
		<p><?php 
			if (trim($email) != '') 
			{
				_e('Email:', TEXTDOMAIN); ?><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
			<?php 
			}
			?>		
		</p>	
		<br />
		<p><?php 
			if (trim($sale_phone) != '' || trim($sale_fax) != '' || trim($sale_email) != '') 
			{ 
				_e('Sales Inquiries', TEXTDOMAIN); 
			}?>
			</p>
		<p><?php 
			if (trim($sale_phone) != '') 
			{
				_e('Phone:', TEXTDOMAIN); 
				echo " ".$sale_phone;
			} 
			?>
		</p>
		<p><?php 
			if (trim($sale_fax) != '') 
			{
				_e('FAX:', TEXTDOMAIN); 
				echo " ".$sale_fax; 
			}?>
		</p>
		<p><?php
			if (trim($sale_email) != '') 
			{		
				_e('Email:', TEXTDOMAIN); ?><a href="mailto:<?php echo $sale_email; ?>"><?php echo $sale_email; ?></a>
		<?php }?>
		</p>
		<!-- Contact info End-->
		<?php

		// After widget (defined by theme functions file)
		echo $after_widget;
	}

		
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Strip tags to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['phone'] = strip_tags($new_instance['phone']);
		$instance['fax']    = strip_tags($new_instance['fax']);
		$instance['email']   = strip_tags($new_instance['email']);
		$instance['sale_phone'] = strip_tags($new_instance['sale_phone']);
		$instance['sale_fax']    = strip_tags($new_instance['sale_fax']);
		$instance['sale_email']   = strip_tags($new_instance['sale_email']);
		$instance['address1'] = strip_tags($new_instance['address1']);
		$instance['address2'] = strip_tags($new_instance['address2']);
		
		return $instance;
	}
		 
	function form( $instance ) {

		// Set up some default widget settings
		$defaults = array(
			'title' => 'Contact Info',
			'phone' => '',
			'fax'  => '',
			'email' => '',
			'address1' => '',
			'address2' => '',
			'sale_phone' => '',
			'sale_fax'  => '',
			'sale_email' => ''
		);
		
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<!-- Phone: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" />
		</p>
		
		<!-- FAX: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'fax' ); ?>"><?php _e('FAX', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'fax' ); ?>" name="<?php echo $this->get_field_name( 'fax' ); ?>" value="<?php echo $instance['fax']; ?>" />
		</p>
		
		<!-- Email: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e('eMail', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo $instance['email']; ?>" />
		</p>
			
		<!-- Sale Phone: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'sale_phone' ); ?>"><?php _e('Sale Phone', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'sale_phone' ); ?>" name="<?php echo $this->get_field_name( 'sale_phone' ); ?>" value="<?php echo $instance['sale_phone']; ?>" />
		</p>
		
		<!-- Sale FAX: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'sale_fax' ); ?>"><?php _e('Sale FAX', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'sale_fax' ); ?>" name="<?php echo $this->get_field_name( 'sale_fax' ); ?>" value="<?php echo $instance['sale_fax']; ?>" />
		</p>
		
		<!-- Sale Email: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'sale_email' ); ?>"><?php _e('Sale eMail', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'sale_email' ); ?>" name="<?php echo $this->get_field_name( 'sale_email' ); ?>" value="<?php echo $instance['sale_email']; ?>" />
		</p>

		<!-- Address 1: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'address1' ); ?>"><?php _e('Address 1', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'address1' ); ?>" name="<?php echo $this->get_field_name( 'address1' ); ?>" value="<?php echo $instance['address1']; ?>" />
		</p>		
		
		<!-- Address 2: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'address2' ); ?>"><?php _e('Address 2', TEXTDOMAIN) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'address2' ); ?>" name="<?php echo $this->get_field_name( 'address2' ); ?>" value="<?php echo $instance['address2']; ?>" />
		</p>		
		<?php
		}
}

// register widget
add_action( 'widgets_init', create_function( '', 'register_widget( "PixFlow_Contact_Info_Widget" );' ) );

?>