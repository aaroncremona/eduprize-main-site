<?php 
get_header(); 

$style = "";
if (!Intro(get_the_title()))
{
	$style = "style='margin-top: 0px;'";
}
?>
	<div class="container">
        <div id="main" <?php echo $style; ?>>
			<div class="row">
			<?php 
				if (have_posts())
				{ 
					while (have_posts())
					{
						the_post();
						DisplayPostDetail();						
					}
				}
			?>
			</div>
			<?php if (have_posts())
			{?>
				<!--Single Page Navigation-->
				<div class="page-navigation clearfix">
					<div class="nav-next"><?php next_posts_link(__('&larr; Older Entries', TEXTDOMAIN)) ?></div>
					<div class="nav-previous"><?php previous_posts_link(__('Newer Entries &rarr;', TEXTDOMAIN)) ?></div>
				</div>			
			<?php 
			}
			?>			
		</div>
	</div>
<?php get_footer(); ?>
