<?php
include('includes/custom_field.php');
define('TEXTDOMAIN', 'haxon');
define('THEME_SLUG', 'HX');
/**************************************************
	Added by Aaron Cremona to fix Chrome Browser
	SSL Bug
**************************************************/
// $_SERVER['HTTPS'] = false;




/**************************************************
	FOLDERS
**************************************************/

define('THEME_DIR',         get_template_directory());
define('THEME_LIB',			THEME_DIR . '/lib');
define('THEME_ADMIN',		THEME_LIB . '/admin');
define('THEME_LANGUAGES',	THEME_LIB . '/languages');
define('THEME_CACHE',	    THEME_DIR . '/cache');
define('THEME_ASSETS',   	THEME_DIR . '/assets');
define('THEME_JS',			THEME_ASSETS . '/js');
define('THEME_CSS',			THEME_ASSETS . '/css');
define('THEME_IMAGES',		THEME_ASSETS . '/img');
define('THEME_PLUGINS',	    THEME_LIB	. '/plugins');
define('THEME_WIDGET',	    THEME_LIB	. '/widget');

/**************************************************
	FOLDER URI
**************************************************/

define('THEME_URI',		    	get_template_directory_uri());
define('THEME_LIB_URI',		    THEME_URI . '/lib');
define('THEME_ADMIN_URI',	    THEME_LIB_URI . '/admin');
define('THEME_LANGUAGES_URI',	THEME_LIB_URI . '/languages');
define('THEME_CACHE_URI',	    THEME_URI     . '/cache');
define('THEME_ASSETS_URI',	    THEME_URI     . '/assets');
define('THEME_PLUGINS_URI',	    THEME_LIB_URI . '/plugins');

define('THEME_JS_URI',			THEME_URI . '/scripts');
define('THEME_CSS_URI',			THEME_URI . '/css');
define('THEME_IMAGES_URI',		THEME_URI . '/images');

/**************************************************
	Text Domain
**************************************************/

load_theme_textdomain( TEXTDOMAIN, THEME_DIR . '/languages' );

/**************************************************
	Content Width
**************************************************/

if ( !isset( $content_width ) ) $content_width = 1170;

/**************************************************
	LIBRARIES
**************************************************/

include_once(THEME_LIB . '/theme-base.php');

// post types and meta boxes
include(THEME_LIB. '/post-types/JesGS_Framework_Load.php');
include(THEME_LIB. '/post-types/portfolio.php');
include(THEME_LIB. '/post-types/team.php');
include(THEME_LIB. '/post-types/intro.php');
include(THEME_LIB. '/post-types/page.php');
include(THEME_LIB. '/post-types/post.php');

// theme settings
require_once(THEME_ADMIN . '/admin.php');

include_once(THEME_LIB . '/string-utilities.php');
include_once(THEME_LIB . '/utilities.php');
include_once(THEME_LIB . '/core/blog.php');
include_once(THEME_LIB . '/core/team.php');
include_once(THEME_LIB . '/core/elements.php');
include_once(THEME_LIB . '/core/portfolio.php');

include_once(THEME_LIB . '/theme-walkers.php');
include_once(THEME_LIB . '/theme-support.php');

// widgets
include_once(THEME_WIDGET . '/default-widgets.php');
include_once(THEME_WIDGET . '/widget-contact.php');
include_once(THEME_WIDGET . '/widget-contactinfo.php');
include_once(THEME_WIDGET . '/widget-contact_info.php');
include_once(THEME_WIDGET . '/widget-help-support.php');
include_once(THEME_WIDGET . '/widget-latest-works.php');
include_once(THEME_WIDGET . '/widget-flickr.php');
include_once(THEME_WIDGET . '/widget-twitter.php');
include_once(THEME_WIDGET . '/widget-search.php');
include_once(THEME_WIDGET . '/widget-advertisement.php');

//plugins
include_once(THEME_LIB . '/plugins.php');

add_filter('query_vars', 'add_my_var');
function add_my_var($public_query_vars) {
	$public_query_vars[] = 'pdfLink';
	return $public_query_vars;
}

function redirectStudent(){
	$user = wp_get_current_user();
	$userRole = $user->roles;
	if(in_array('Student', $userRole)){
			wp_redirect(home_url());
			exit();
	}
}

function testing() {
	$user = wp_get_current_user();
  $userRole = $user->roles;
	if(in_array('Student', $userRole)){
		add_filter('show_admin_bar', '__return_true');
		add_action('admin_bar_menu', 'custom_button_example', 50);
		?>
		<style>
		#wpcontent { margin-left: 15px !important; }
		#wp-admin-bar-edit-profile { display: none }
		#footer-thankyou { display: none }
		#footer-upgrade { display: none }
		#wp-admin-bar-dashboard { display: none }
		#wp-admin-bar-user-info { display: none }
		#wp-admin-bar-notes { display: none }
		#adminmenuback { display: none; }
		#adminmenuwrap { display: none; }
		</style>

		<?php
	}
}

function custom_button_example($wp_admin_bar){
		$args = array(
		'id' => 'StudentHome',
		'title' => 'My Classes',
		'href' => 'http://eduprizeschools.net/wp-admin/index.php?page=home_student',
		'meta' => array(
		)
	);
$wp_admin_bar->add_node($args);
}

add_action( 'wp_loaded', 'testing' );
add_action('wp_logout', 'redirectStudent');add_action( 'woocommerce_single_product_summary_db', 'woocommerce_template_single_title', 5 );add_action( 'woocommerce_single_product_summary_db', 'woocommerce_template_single_rating', 10 );add_action( 'woocommerce_single_product_summary_db', 'woocommerce_template_single_price', 10 );add_action( 'woocommerce_single_product_summary_db', 'woocommerce_template_single_excerpt', 20 );add_action( 'woocommerce_single_product_summary_db', 'woocommerce_template_single_meta', 40 );add_action( 'woocommerce_single_product_summary_db', 'woocommerce_template_single_sharing', 50 );add_action( 'woocommerce_single_product_summary_db', 'woocommerce_template_single_add_to_cart', 30 );


	function woocommerce_product_subcategories( $args = array() ) {
		global $wp_query;

		$defaults = array(
			'before'  => '',
			'after'  => '',
			'force_display' => false
		);

		$args = wp_parse_args( $args, $defaults );

		extract( $args );

		// Main query only
		if ( ! is_main_query() && ! $force_display ) return;

		// Don't show when filtering, searching or when on page > 1 and ensure we're on a product archive
		if ( is_search() || is_filtered() || is_paged() || ( ! is_product_category() && ! is_shop() ) ) return;

		// Check categories are enabled
		if ( is_shop() && get_option( 'woocommerce_shop_page_display' ) == '' ) return;

		// Find the category + category parent, if applicable
		$term 			= get_queried_object();
		$parent_id 		= empty( $term->term_id ) ? 0 : $term->term_id;

		if ( is_product_category() ) {
			$display_type = get_woocommerce_term_meta( $term->term_id, 'display_type', true );

			switch ( $display_type ) {
				case 'products' :
					return;
				break;
				case '' :
					if ( get_option( 'woocommerce_category_archive_display' ) == '' )
						return;
				break;
			}
		}

		// NOTE: using child_of instead of parent - this is not ideal but due to a WP bug ( http://core.trac.wordpress.org/ticket/15626 ) pad_counts won't work
		$args = apply_filters( 'woocommerce_product_subcategories_args', array(
			'child_of'		=> $parent_id,
			'menu_order'	=> 'ASC',
			'hide_empty'	=> 1,
			'hierarchical'	=> 1,
			'taxonomy'		=> 'product_cat',
			'pad_counts'	=> 1
		) );

		$product_categories     = get_categories( $args );
		$product_category_found = false;

		if ( $product_categories ) {

			foreach ( $product_categories as $category ) {

				if ( $category->parent != $parent_id ) {
					continue;
				}
				if ( $args['hide_empty'] && $category->count == 0 ) {
					continue;
				}

				if ( ! $product_category_found ) {
					// We found a category
					$product_category_found = true;
					echo $before;
				}

				wc_get_template( 'content-product_cat.php', array(
					'category' => $category
				) );

			}

		}

		// If we are hiding products disable the loop and pagination
		if ( $product_category_found ) {
			if ( is_product_category() ) {
				$display_type = get_woocommerce_term_meta( $term->term_id, 'display_type', true );

				switch ( $display_type ) {
					case 'subcategories' :
						$wp_query->post_count = 0;
						$wp_query->max_num_pages = 0;
					break;
					case '' :
						if ( get_option( 'woocommerce_category_archive_display' ) == 'subcategories' ) {
							$wp_query->post_count = 0;
							$wp_query->max_num_pages = 0;
						}
					break;
				}
			}
			if ( is_shop() && get_option( 'woocommerce_shop_page_display' ) == 'subcategories' ) {
				$wp_query->post_count = 0;
				$wp_query->max_num_pages = 0;
			}

			echo $after;
			return true;
		}

	}

// remove a column
function wc_csv_export_remove_column( $column_headers ) {
	// the list of column keys can be found in class-wc-customer-order-csv-export-generator.php
	unset( $column_headers['order_id'] );
	// unset( $column_headers['status'] );
	unset( $column_headers['shipping_total'] );
	unset( $column_headers['shipping_tax_total'] );
	unset( $column_headers['tax_total'] );
	unset( $column_headers['cart_discount'] );
 	unset( $column_headers['order_discount'] );
 	unset( $column_headers['discount_total'] );
 	unset( $column_headers['refunded_total'] );
 	unset( $column_headers['order_currency'] );
 	unset( $column_headers['payment_method'] );
 	unset( $column_headers['shipping_method'] );
 	unset( $column_headers['customer_id'] );
 	unset( $column_headers['shipping_first_name'] );
 	unset( $column_headers['shipping_last_name'] );
 	unset( $column_headers['shipping_address_1'] );
 	unset( $column_headers['shipping_address_2'] );
 	unset( $column_headers['shipping_postcode'] );
 	unset( $column_headers['shipping_city'] );
 	unset( $column_headers['shipping_state'] );
	unset( $column_headers['shipping_country'] );
	unset( $column_headers['shipping_company'] );
	unset( $column_headers['billing_company'] );

	unset( $column_headers['order_notes'] );
	unset( $column_headers['download_permissions'] );
	unset( $column_headers['item_tax'] );
	unset( $column_headers['item_refunded'] );
	unset( $column_headers['shipping_items'] );
	unset( $column_headers['tax_items'] );
	unset( $column_headers['coupon_items'] );
 	// unset( $column_headers[''] );

	return $column_headers;
}
add_filter( 'wc_customer_order_csv_export_order_headers', 'wc_csv_export_remove_column' );
?>