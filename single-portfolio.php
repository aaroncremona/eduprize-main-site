<?php 
get_header(); 

$style = "";
if (!Intro(get_the_title()))
{
	$style = "style='margin-top: 0px;'";
}
?>
	<div class="container">
        <div id="main" class="container" <?php echo $style; ?>>
			<?php 
			if (have_posts())
			{ 
				while (have_posts())
				{
					the_post();					
					PortfolioDetail();
				}
			}
			else
			{ 
			?>
				<div id="post-0" >
					<p><?php _e("Sorry, but you are looking for something that isn't here.", TEXTDOMAIN) ?></p>
				</div>
			<?php 
			}
			?>			
		</div>
	</div>
<?php get_footer(); ?>
