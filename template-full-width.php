<?php
/*
Template Name: Full Width
*/

get_header();

$style = "";
if (!Intro(get_the_title()))
{
	$style = "style='margin-top: 0px;'";
}
?>
	<div class="container">
        <div id="main" <?php echo $style; ?>>
			<div class="row">
				<div class="span12">
				<?php 
				if (have_posts()) 
				{ 
					while (have_posts()) 
					{ 
						the_post(); 
				?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
						<?php echo RemoveLastMarginOfFullWidthPages(_get_the_content()); ?>
					</div>
				<?php
					}
				}
				comments_template('', true);
				?>				
				</div>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>