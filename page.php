<?php get_header(); ?>
<?php 
	$style = "";
	if (!Intro(get_the_title()))
	{
		$style = "style='margin-top: 0px;'";
	}
?>
	<div class="container">
        <div id="main" <?php echo $style; ?>>
			<div class="row">
				<?php
					$pageClass = 'span8';
					
					if(opt('sidebar_position') == 0)
						$pageClass = 'span12';
					if(opt('sidebar_position') == 1)
						$pageClass .= ' blog_right';	
				?>
				<div class="<?php echo $pageClass; ?>">
				<?php 
				if (have_posts()) 
				{ 
					while (have_posts()) 
					{ 
						the_post(); 
                ?>
                    <div class="post-thumbnail-left">
                    </div>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
						<?php echo RemoveLastMarginOfFullWidthPages(_get_the_content()); ?>
					</div>
				<?php
					}
				}
				// comments_template('', false);
				?>				
				</div>
				<?php 
				if(opt('sidebar_position') != 0)
					get_sidebar(); 
				?>				
			</div>
		</div>
	</div>
<?php get_footer(); ?>
