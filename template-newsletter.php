<?php
/*
Template Name: Newsletter 
*/
?>

<?php get_header(); ?>

<div id="main" class="container newsletter">

<?php
    $the_cat = get_category_by_slug('newsletter');
    echo $the_cat->ID;
    $posts = get_posts( 
        array(
            'category' => $the_cat->ID,
            'posts_per_page' => 12
        )
    );
    if(count($posts))
    {
        foreach($posts as $post) 
        { 
            setup_postdata($post);
            global $more;
            $more = 0;
            DisplayPostItemDetail(); 
        }
    }
?>

</div>

<?php get_footer(); ?>
