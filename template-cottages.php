<?php
/*
Template Name: Cottages
*/
?>

<?php get_header(); ?>

<div id="main" class="container cottages">

<h1>EDUPRIZE Cottages</h1>

<p>"Cottages" are an EDUPRIZE original.  These quarterly, thematic focuses allow students to learn and apply basic skills through a hands-on, science-based program.  Cross curricular instruction ensures that students can transfer knowledge to new and applied content areas and allow students to be actively engaged in their learning.  Our "Cottages" offer multiple modality, 9 week units in a Life Science, Physical Science, Social Science, and Earth Science.</p>

<ul class="grades">
    <li>
        <h2>1st Grade</h2>
        <ul class="themes">
            <li>
                <h3>Bird House</h3>
                <ul class="links">
                    <li>Mutation</li>
                    <li>Flight</li>
                    <li>Adaption</li>
                    <li>Migration</li>
                    <li>Regions</li>
                    <li>Geography</li>
                    <li>Extinction</li>
                    <li>Bird Types</li>
                    <li>Research</li>
                </ul>
            </li>
            <li>
                <h3>Mineral Market</h3>
                <ul class="links">
                    <li>Earth Core</li>
                    <li>Rock Types</li>
                    <li>Geology</li>
                    <li>Erosion</li>
                    <li>Changing Surface</li>
                    <li>Formations</li>
                    <li>Volcanoes</li>
                    <li>Fossils</li>
                    <li>Geography</li>
                </ul>
            </li>
            <li>
                <h3>Sweet Shop</h3>
                <ul class="links">
                    <li>States of matter</li>
                    <li>Food Pyramid</li>
                    <li>Health</li>
                    <li>Recipes</li>
                    <li>Measurements</li>
                    <li>Nutrition</li>
                    <li>Thermometers</li>
                    <li>Chemistry</li>
                    <li>Gardening</li>
                </ul>
            </li>
            <li>
                <h3>Plymouth Plantation</h3>
                <ul class="links">
                    <li>Exploratation</li>
                    <li>Pilgrims</li>
                    <li>Oceans</li>
                    <li>Continents</li>
                    <li>American History</li>
                    <li>Native Americans</li>
                    <li>Culture</li>
                    <li>Governance</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <h2>2nd Grade</h2>
        <ul class="themes">
            <li>
                <h3>Dinosaur Dig</h3>
                <ul class="links">
                    <li>Fossils</li>
                    <li>Archeology</li>
                    <li>Dinosaurs</li>
                    <li>Eras</li>
                    <li>Extinction</li>
                    <li>Change of Earth</li>
                    <li>Ice Age</li>
                    <li>Classification</li>
                    <li>Paleontology</li>
                </ul>
            </li>
            <li>
                <h3>American Boutique</h3>
                <ul class="links">
                    <li>American History</li>
                    <li>Native American</li>
                    <li>Petroglyphs</li>
                    <li>Home Structures</li>
                    <li>Language</li>
                    <li>Culture</li>
                    <li>Western Migration</li>
                    <li>Plants</li>
                    <li>Transportation</li>
                    <li>Trade</li>
                </ul>
            </li>
            <li>
                <h3>Weather Station</h3>
                <ul class="links">
                    <li>Cycles</li>
                    <li>Meteorology</li>
                    <li>Clouds</li>
                    <li>Weather Maps</li>
                    <li>Instruments</li>
                    <li>Storm Types</li>
                    <li>Moon Phases</li>
                    <li>Erosion</li>
                    <li>Climate</li>
                    <li>Phases</li>
                </ul>
            </li>
            <li>
                <h3>Bug Farm</h3>
                <ul class="links">
                    <li>Insects</li>
                    <li>Anatomy</li>
                    <li>Arthropods</li>
                    <li>Life Stages</li>
                    <li>Etymology</li>
                    <li>Colonies</li>
                    <li>Food Chain</li>
                    <li>Globe</li>
                    <li>Systems</li>
                    <li>Adaptations</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <h2>3rd Grade</h2>
        <ul class="themes">
            <li>
                <h3>Aquarium</h3>
                <ul class="links">
                    <li>Oceans</li>
                    <li>Biology</li>
                    <li>Zones</li>
                    <li>Sea Life</li>
                    <li>Tide Pods</li>
                    <li>Waves</li>
                    <li>Plant Life</li>
                    <li>Plate Tectonics</li>
                    <li>Reefs</li>
		    <li>Erosion</li>
                </ul>
            </li>
            <li>
                <h3>Plant Nursery</h3>
                <ul class="links">
                    <li>Botany</li>
                    <li>Cacti</li>
                    <li>Carnivores</li>
                    <li>Plant Types</li>
                    <li>Reproduction</li>
                    <li>Cells</li>
                    <li>Gardening</li>
                    <li>Photosynthesis</li>
                    <li>Plant Structure</li>
                    <li>Systems</li>
                </ul>
            </li>
            <li>
                <h3>Exploration Station</h3>
                <ul class="links">
                    <li>Explorers</li>
                    <li>World History</li>
                    <li>Geography</li>
                    <li>Trade</li>
                    <li>Navigation</li>
                    <li>Poles/Magnets</li>
                    <li>Passages</li>
                    <li>Economics</li>
                    <li>Ship Building Instruments</li>
                    <li>Cultures</li>
                </ul>
            </li>
            <li>
                <h3>Desert Lab</h3>
                <ul class="links">
                    <li>Ancient People</li>
                    <li>Deserts</li>
                    <li>Ethnopotony</li>
                    <li>Cacti</li>
                    <li>Succulents</li>
                    <li>Biomes</li>
                    <li>Geography</li>
                    <li>Canals</li>
                    <li>Animals</li>
                    <li>Adaptations</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <h2>4th Grade</h2>
        <ul class="themes">
            <li>
                <h3>Reptile Ranch</h3>
                <ul class="links">
                    <li>Herpetology</li>
                    <li>Vertebrates</li>
                    <li>Classification</li>
                    <li>Ecology</li>
                    <li>Habitats</li>
                    <li>Life Cycle</li>
                    <li>Reptile</li>
                    <li>Adaptations</li>
                    <li>Amphibians</li>
                    <li>Defenses</li>
                </ul>
            </li>
            <li>
                <h3>Grand Canyon Market Place</h3>
                <ul class="links">
                    <li>Arizona History</li>
                    <li>Land Forms</li>
                    <li>Caves</li>
                    <li>Resources</li>
                    <li>Animal Life</li>
                    <li>Industry</li>
                    <li>Erosion</li>
                    <li>Geology</li>
                    <li>Plant Life</li>
                    <li>Water Ways</li>
                </ul>
            </li>
            <li>
                <h3>Conservation Plantation</h3>
                <ul class="links">
                    <li>Water Conservation</li>
                    <li>Pollution</li>
                    <li>Renewable Resources</li>
                    <li>Cycles</li>
                    <li>Composting</li>
                    <li>Recycling</li>
                    <li>Ecology</li>
                    <li>Rainforest</li>
                    <li>Energy</li>
                    <li>Extinction</li>
                </ul>
            </li>
            <li>
                <h3>Arizona Highways</h3>
                <ul class="links">
                    <li>Statehood</li>
                    <li>Counties</li>
                    <li>Arizona History</li>
                    <li>Mapping</li>
                    <li>Timelines</li>
                    <li>Mining</li>
                    <li>Government</li>
                    <li>Transportations</li>
                    <li>Communities</li>
                    <li>Landmarks</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <h2>5th Grade</h2>
        <ul class="themes">
            <li>
                <h3>Body Shop</h3>
                <ul class="links">
                    <li>Antinomy</li>
                    <li>Physiology</li>
                    <li>Systems</li>
                    <li>Cells</li>
                    <li>Nutrition</li>
                    <li>Health</li>
                    <li>Disease</li>
                    <li>Genetics</li>
                    <li>Fitness</li>
                    <li>Medical Careers</li>
                </ul>
            </li>
            <li>
                <h3>Invention Convention</h3>
                <ul class="links">
                    <li>Industry</li>
                    <li>Inventors</li>
                    <li>Timelines</li>
                    <li>History</li>
                    <li>Entrepreneurship</li>
                    <li>Marketing</li>
                    <li>Motion</li>
                    <li>Physics</li>
                    <li>Machinery</li>
                    <li>Technology</li>
                </ul>
            </li>
            <li>
                <h3>Planetarium</h3>
                <ul class="links">
                    <li>Planets</li>
                    <li>Constellations</li>
                    <li>Space Stations</li>
                    <li>Space Explorations</li>
                    <li>Astrophysics</li>
                    <li>Moon Cycle</li>
                    <li>Laws</li>
                    <li>NASA</li>
                    <li>Flights</li>
                    <li>Observatories</li>
                </ul>
            </li>
            <li>
                <h3>Physics Foundry</h3>
                <ul class="links">
                    <li>Motion</li>
                    <li>Gravity</li>
                    <li>Force</li>
                    <li>Physics</li>
                    <li>Machines</li>
                    <li>Waves</li>
                    <li>Acoustics</li>
                    <li>Light</li>
                    <li>Careers</li>
                    <li>Mass</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <h2>6th Grade</h2>
        <ul class="themes">
            <li>
                <h3>Money Market</h3>
                <ul class="links">
                    <li>Economics</li>
                    <li>Trade</li>
                    <li>History</li>
                    <li>Cultures</li>
                    <li>Stock Markets</li>
                    <li>Math Applications</li>
                    <li>Money</li>
                    <li>Finance</li>
                    <li>Research</li>
                    <li>Supply &amp; Demand</li>
                </ul>
            </li>
            <li>
                <h3>Power Plant</h3>
                <ul class="links">
                    <li>Solar</li>
                    <li>Geothermal</li>
                    <li>Nuclear</li>
                    <li>Wind</li>
                    <li>Electricity</li>
                    <li>Renewable Resources</li>
                    <li>Careers</li>
                    <li>Energy</li>
                    <li>History</li>
                    <li>Cultures</li>
                </ul>
            </li>
            <li>
                <h3>Storm Cellar</h3>
                <ul class="links">
                    <li>Weather</li>
                    <li>Water Cycle</li>
                    <li>Clouds</li>
                    <li>Fronts</li>
                    <li>Natural Disasters</li>
                    <li>Geography</li>
                    <li>Energy</li>
                    <li>Instruments</li>
                    <li>Atmosphere</li>
                    <li>Careers</li>
                </ul>
            </li>
            <li>
                <h3>Greenhouse</h3>
                <ul class="links">
                    <li>Systems</li>
                    <li>Hydroponics</li>
                    <li>Plant Life</li>
                    <li>Climate Change</li>
                    <li>Biology</li>
                    <li>Plant Parts</li>
                    <li>Biospheres</li>
                    <li>Soils</li>
                    <li>Farming</li>
                    <li>Careers</li>
                </ul>
            </li>
        </ul>
    </li>
</ul>

</div>

<?php get_footer(); ?>
