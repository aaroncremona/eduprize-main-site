<div class="searchbox">
	<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<fieldset>
			<input type="text" title="Type your search here and press enter to search the EDUPRIZE website" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php echo esc_attr__('Search', TEXTDOMAIN); ?>"  />
			<input type="submit" title="Click to submit your search" id="searchsubmit" name="submit" value="" />
		</fieldset>
	</form>
</div>