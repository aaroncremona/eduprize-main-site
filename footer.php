	<div id="footer">

		<div class="container">

			<!--Widgets-->

	            <div class="widget_area">

					<div class="row">

						<?php 

						$footerWidgets = opt('footer_widgets');

						

						if($footerWidgets=='') $footerWidgets = 4;

						$widgetSize = 12 / $footerWidgets;

						for($i = 1; $i <= $footerWidgets; $i++)

						{ ?>

							<div class="span<?php echo $widgetSize ?>">

								<?php 

								/* Widgetised Area */ 

								if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar( 'footer-widget-' . $i ) ){}	?>

							</div>

						<?php	

						}

					?>	

				</div>

			</div>

		</div>

		<div class="foot">

			<div class="container">

		        <?php

		        $logo = opt('footer-logo') == "" ? THEME_IMAGES_URI . "/footer-logo.png" : opt('footer-logo');

		        ?>

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="footer-logo"><img src="<?php echo $logo; ?>" alt="Copyright Logo" /></a>

				<span class="copyright"><?php eopt('footer_copyright'); ?>&nbsp; EDUPRIZE Schools is an equal access/equal opportunity school.  If you have 
				trouble accessing this page because of a disability, please contact the Disability Coordinator at <a href="mailto:accessibility@eduprizeschools.net">accessibility@eduprizeschools.net</a> or (480) 813-9537 ext. 1990.</span>

				<a id="top_button" href="#top" title="Click this to return to the top of the webpage"><br />Back to top</a>
				
			</div>		

		</div>

	</div>

	<!-- Theme Hook -->

	<?php wp_footer(); ?>

</body>

</html>