<?php
add_action( 'add_meta_boxes', 'event_meta_box_add' );
function event_meta_box_add()
{
    add_meta_box( 'event-meta-box-id', 'Event', 'event_meta_box', 'post', 'normal', 'high' );
}



function event_meta_box( $post )
{
$values = get_post_custom( $post->ID );
$text = isset( $values['event_date'] ) ? esc_attr( $values['event_date'][0] ) : '';


if($text!=''){
$text = date('Y-m-d',$text);
}

    ?>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/jsDatePick_ltr.min.css" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/css/jsDatePick.min.1.3.js"></script>
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"event_date",
			dateFormat:"%Y-%m-%d"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
</script>
	
	
<p>
    <label for="event_date">Event Date</label>
    <input type="text" size="12" name="event_date" id="event_date" value="<?php echo $text; ?>" /> yyyy-mm-dd
        </p>
    <?php       
}
 
 
 add_action( 'save_post', 'event_meta_box_save' );
function event_meta_box_save( $post_id )
{     
    if( isset( $_POST['event_date'] ) )
        update_post_meta( $post_id, 'event_date',strtotime($_POST['event_date']));      
}
?>