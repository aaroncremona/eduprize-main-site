<?php
/*
Template Name: Contact
*/

get_header();

$style = "";
if (!Intro(__("Contact Us", TEXTDOMAIN)))
{
	$style = "style='margin-top: 0px;'";
}
?>
	<div class="contact_map"></div>
	<div class="container">
        <div id="main">
			<div class="row" <?php echo $style; ?>>
				<div class="span8">
					<?php 
					if (have_posts()) { while (have_posts()) { the_post(); 
					?>					
					<br />
					<?php the_content(); 
						}//While have_posts
					}//If have_posts
					?>					
				</div>
			</div>
        <!--End Content-->
		</div>
	</div>
	<script type="text/javascript">
		var gmapSettings = { 
          	  zoom: 14,
              scrollwheel: false,
              scaleControl: false,
              draggable: false,
              zoomControl: false,
              disableDoubleClickZoom: true,                	
              mapTypeControl: false,
              markers: [{address: '<?php eopt('contact_map_address'); ?>'}], 
              }; 
	</script> 	
	<script type='text/javascript' src="https://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php eopt('maps_api_key'); ?>"></script>
<?php get_footer(); ?>
