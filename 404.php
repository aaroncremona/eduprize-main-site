<?php
/**
The template for displaying 404 pages (Not Found).
**/
header("HTTP/1.1 404 Not Found");
header("Status: 404 Not Found");

get_header(); 

$style = "";
if (!Intro(__('Error Page', TEXTDOMAIN), -1, true))
{
	$style = "style='margin-top: 0px;'";
}
?>			
	<div class="container not-found-page" id="content">
        <div id="main" <?php echo $style; ?>>
			<div class="row">
				<div class="span6">
					<span class="error">4<span class="colored">0</span>4</span>
					<span class="error_text"><?php _e('Sorry, it appears that the page you were looking for doesn\'t exist anymore or might have been moved!', TEXTDOMAIN) ?></span>
					<span class="error_text"><?php _e('TROUBLESHOOTING :', TEXTDOMAIN) ?></span>
					<ul class="arrow_list">
						<li><?php _e('Check Spelling', TEXTDOMAIN) ?></li>
						<li><?php _e('Use search box', TEXTDOMAIN) ?></li>
						<li><?php _e('Contact support', TEXTDOMAIN) ?></li>
						<li><?php _e('Read FAQ', TEXTDOMAIN) ?></li>
					</ul>
				</div>
				<div class="span6">
				</div>
			</div>
		</div>
	</div>	
<?php get_footer(); ?>