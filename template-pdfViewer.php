<?php
/*
Template Name: Pdf-Viewer
*/
//add_action( 'wp_enqueue_scripts', 'mysite_enqueue' );

//function mysite_enqueue() {
	//$ss_url = get_stylesheet_directory_uri();
	//wp_enqueue_script( 'getUrl', "{$ss_url}/scripts/getURL.js" );
//}

get_header();

$style = "";
if (!Intro(get_the_title()))
{
	$style = "style='margin-top: 0px;'";
}

$pdfURL = get_query_var('pdfLink');
$pdfshortcode = "[pdfjs-viewer url=".$pdfURL." viewer_width=70% viewer_height=800px fullscreen=true download=true print=true openfile=false]";
echo do_shortcode($pdfshortcode);
?>
<style>#intro{display:none;}</style>.
	<div class="data">
	</div>

<?php get_footer(); ?>
