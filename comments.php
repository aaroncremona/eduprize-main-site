<?php
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');

if ( post_password_required() ) { ?>
	<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', TEXTDOMAIN) ?></p>
<?php
	return;
}
/*-----------------------------------------------------------------------------------*/
/*	Display the comments + Pings
/*-----------------------------------------------------------------------------------*/
if ( !comments_open() && !have_comments())
	return;
?>
<div class="row">
	<div class="span1 visible-desktop">&nbsp;</div>
	<div class="blog_comments span8">
		<?php 
		if ( have_comments() ) 
		{ // if there are comments ?>
			<div class="comments-wrap">
				<h4 id="comments"><?php comments_number(__('No Comments', TEXTDOMAIN), __('One Comment', TEXTDOMAIN), __('% Comments', TEXTDOMAIN)); ?></h4>
				<div class="separator"></div>
				<?php 	
				if ( ! empty($comments_by_type['comment']) ) 
				{ // if there are normal comments ?>
					<ul id="comments" class="comment_list">
					<?php wp_list_comments('type=comment&avatar_size=30&callback=theme_comment'); ?>
					</ul>
				<?php 
				} // if there are normal comments
				
				if ( ! empty($comments_by_type['pings']) ) 
				{ // if there are pings ?>
					<h4 id="pings"><?php _e('Trackbacks for this post', TEXTDOMAIN) ?></h4>
					<ol class="ping_list">
					<?php wp_list_comments('type=pings&callback=theme_list_pings'); ?>
					</ol>
				<?php 
				} // if there are pings ?>
				<div class="navigation">
					<div class="alignleft"><?php previous_comments_link(); ?></div>
					<div class="alignright"><?php next_comments_link(); ?></div>
				</div>
			</div>
			<?php
		}
		
		//Comment Form
		if ( comments_open() )
		{  
		?>		
		<div id="respond_wrap">
		<?php			
			_comment_form(
				array( 
					'id_form' => 'commentform',
					'comment_notes_before' => '',
					'comment_field'=>'<div class="textarea_input"><textarea tabindex="1" rows="6" cols="58" name="comment" value="" placeholder="' . __('Your Message', TEXTDOMAIN).  '"></textarea></div>',
					'comment_notes_after' => '',
					'title_reply'=> __('Leave a Reply', TEXTDOMAIN),
					'cancel_reply_link' => __('Cancel reply', TEXTDOMAIN),
					'label_submit' => __('Send', TEXTDOMAIN)
				)
			); 
		?>
		</div>
		<?php
		}
		?>
	</div>
</div>