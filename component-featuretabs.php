<?php

/*

Template Name: Feature tabs

*/

?>



<!-- Home Navigation -->

<div id="home-navigation">

    <div class="tab-head-background"></div>

    <div class="container">

        <div class="wrapper">

            <div class="tab">

                <div class="tab_head parentclear">

                    <div class="head_row">

                    <?php

                    $tab_position = opt('tab_position');

                    if ($tab_position == '' || count($tab_position) != 4) {

                        $tab_position = array(0, 1, 2, 3);

                    }

                    $tablist = array(

                            0 => 'first_tab_title', 1 => 'second_tab_title',

                            2 => 'third_tab_title', 3 => 'fourth_tab_title'

                    );

                    for($i=0; $i < count($tab_position); $i++)

                    {

                        $selected = '';

                        if (opt('default_selected_tab') == $tab_position[$i]){

                            $selected = 'class="selected"';

                        }

                    ?>

                        <div class="head_cell"><a <?php echo $selected; ?> href="#tab<?php echo $tab_position[$i];?>"><span><?php eopt($tablist[$tab_position[$i]]); ?></span></a></div>

                    <?php

                    }

                    ?>

                    </div>

                </div>

                <div class="tab_content" id="tab0">

                    <div class="heading">

                        <h3><?php eopt('first_tab_heading'); ?></h3>

                    </div>

                    <!-- subject areas go here -->

                    <div class="subject-areas">

                        <?php

                        $parent = get_page_by_path('subjects');

                        $subjects = get_pages(

                            array(

                                'parent' => $parent->ID, // subjects

                            )

                        );

                        if(count($subjects))

                        {

                            foreach($subjects as $post)

                            {

                                setup_postdata($post);

                                global $more;

                                $more = 0;

                            ?>

                            <?php DisplayPortfolioItem(); ?>

                            <?php

                            }

                        }

                        ?>

                    </div>

                </div>

                <!-- Slogan -->

                <div class="tab_content" id="tab1">

                    <!-- second tab content goes here -->

                    <!-- Latest Posts -->

                    <?php

                    wp_enqueue_script('flexslider');

                    if(opt('home_latest_portfolio'))

                    {

                        $query = new WP_Query();

                        $args = array(

                            'post_type' => 'portfolio',

                            'posts_per_page' => 6,

                            'orderby' => 'date',

                            'order' => 'DESC'

                        );

                        $query->query($args);



                        if($query->have_posts())

                        {

                        ?>

                        <div id="awards" class="row">

                            <div class="span6">

                                <div class="text-widget">

                                    <div class="heading">

                                        <h3>

                                            <?php eopt('home_latest_portfolio_title'); ?>

                                        </h3>

                                    </div>

                                    <div class="content awards-intro">

                                    <?php echo nl2br(opt('home_latest_portfolio_desc')); ?>

                                    </div>

                                    <div id="awards-text" class="content">

                                        <?php

                                        while ($query->have_posts())

                                        {

                                            $query->the_post();

                                        ?>

                                            <div class="award-text"> <?php the_content(); ?> </div>

                                        <?php

                                        }

                                        ?>

                                    </div>

                                </div>

                            </div>

                            <div class="span6">

                                <?php

                                while ($query->have_posts())

                                {

                                    $query->the_post();

                                ?>

                                    <h3 class="name"><?php the_title(); ?></h3><br/>

                                <?php

                                }

                                ?>

                            </div>

                        </div>

                        <?php

                        }

                        wp_reset_query();

                    }

                    ?>

                    <!--Latest End-->

                </div>



                <div class="tab_content" id="tab2">

                    <div class="span5 col left">

                        <div class="vertical-jagged-divider"></div>

                        <div class="heading">

                            <h3>Gilbert Tours</h3>

                        </div>

                        <!-- second tab content goes here -->

                        <?php

                        $lastposts = get_posts(

                            array(

                                'category' => '92', // 4 for Gilbert Events, 92 for Gilbert Tours

                                'posts_per_page' => 2

                            )

                        );

                        if(count($lastposts))

                        {

                            foreach($lastposts as $post)

                            {

                                setup_postdata($post);

                                global $more;

                                $more = 0;

                            ?>

                            <div class="tab_review">

                                <div class="row">

                                <?php DisplayPostItemDetailEvent(); ?>

                                </div>

                            </div>

                            <div class="row">

                                <div class=" span5">

                                    <div style="height:1px; border-bottom:1px solid transparent;"></div>

                                </div>

                            </div>

                            <?php

                            }

                        }

                        ?>

                        <div class="center">

                             <a class="calendar btn material-btn" href="https://www.eduprizeschools.net/gilbert-tours/">Gilbert Tour Information</a>

                        </div>

                    </div>

                    <div class="span5 col">

                        <div class="heading">

                            <h3>Queen Creek Tours</h3>

                        </div>

                        <!-- second tab content goes here -->

                        <?php

                        $lastposts = get_posts(

                            array(

                                'category' => '93', // 5 for Queen Creek Events, 93 for QC Tours

                                'posts_per_page' => 2

                            )

                        );

                        if(count($lastposts))

                        {

                            foreach($lastposts as $post)

                            {

                                setup_postdata($post);

                                global $more;

                                $more = 0;

                            ?>

                            <div class="tab_review">

                                <div class="row">

                                <?php DisplayPostItemDetailEvent(); ?>

                                </div>

                            </div>

                            <div class="row">

                                <div class=" span5">

                                    <div style="height:1px; border-bottom:1px solid transparent;"></div>

                                </div>

                            </div>

                            <?php

                            }

                        }

                        ?>

                        <div class="center">

                            <a class="calendar btn material-btn" href="https://www.eduprizeschools.net/queen-creek-tours/">Queen Creek Tours</a>

                        </div>

                    </div>

                </div>



                <div class="tab_content" id="tab3">

                <!-- tab 3 content goes here -->

                <h2>Welcome Back Letters</h2>

                <p>
                    We look forward to another exciting year of learning at EDUPRIZE! Please choose your campus to continue:
                </p>

                    <div class="row ">
                        <div class="span4 wpb_column column_container">
                            <div class="wpb_single_image wpb_content_element1 wpb_animate_when_almost_visible wpb_bottom-to-top">
                                <div class="wpb_wrapper">
                                    <a href="https://www.eduprizeschools.net/welcome-back-gilbert">
                                    <h3 class="wpb_heading wpb_singleimage_heading">Gilbert Pre-K - 6th Grade</h3>
                                    <img src="https://www.eduprizeschools.net/wp-content/uploads/2014/06/eduprize.jpg" width="300" height="90" alt="EDUPRIZE Gilbert Banner with Young Students" /></a>
                                </div>
                            </div>
                        </div>

                        <div class="span4 wpb_column column_container">

                            <div class="wpb_single_image wpb_content_element1 wpb_animate_when_almost_visible wpb_bottom-to-top">
                                <div class="wpb_wrapper">
                                    <a href="https://www.eduprizeschools.net/welcome-back">
                                    <h3 class="wpb_heading wpb_singleimage_heading">Gilbert 7th - 12th Grade</h3>
                                    <img src="https://www.eduprizeschools.net/wp-content/uploads/2014/08/EDUPRIZE-Gilbert-Banner2.jpg" width="300" height="90" alt="EDUPRIZE Hight School Banner" /></a>
                                </div>
                            </div>
                        </div>


                        <div class="span4 wpb_column column_container">

                            <div class="wpb_single_image wpb_content_element1 wpb_animate_when_almost_visible wpb_bottom-to-top">
                                <div class="wpb_wrapper">
                                    <a href="https://www.eduprizeschools.net/welcome-back-queen-creek">
                                    <h3 class="wpb_heading wpb_singleimage_heading">Queen Creek Welcome Back Letters</h3>
                                    <img src="https://www.eduprizeschools.net/wp-content/uploads/2014/08/EDUPRIZE-QC12.jpg" width="300" height="90" alt="EDUPRIZE Queen Creek Banner" /></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
