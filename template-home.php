<?php

/*

Template Name: Home

*/

get_header();

//Intro(get_the_title());

?>

	<div class="home-slider">

	<?php 

	if (have_posts()) 

	{ 

		while (have_posts()) 

		{ 

			the_post(); 

			echo RemoveLastMargin(_get_the_content());			

		}

	}

	?>				

	</div>	

    <?php 

        if(opt('home_navigation'))

        {

    ?>

    <!-- Featuretabs inserted here -->

    <?php get_template_part( 'component', 'featuretabs' ); ?>

    <?php 

        }

    ?>

    <div class="container">

        <div class="row">

            <div class="span12">

                <hr id="home-page-divider"/>

            </div>

        </div>

    </div>

	<!--Content-->

	<div id="main">

        <div class="video container">

            <span class="span7">

                <iframe title="Welcome to EDUPRIZE Informational Video" width="640" height="360" src="//www.youtube.com/embed/yylWuklpzNw" frameborder="0" allowfullscreen>Welcome to EDURPIZE</iframe>

					<a href="https://www.eduprizeschools.net/video-transcript-for-accessibility/">Video transcript for accessibility</a>
				

            </span>

            <span class="description span4">

                <h2>The EDUPRIZE Way</h2>

                <p>The EDUPRIZE SCHOOLS mission is to provide educational excellence to our community of learners through the use of project-based, multiple modality instruction, and a strong differentiated curriculum. Students will exceed grade level standards and grow to be global citizens in an academically rigorous environment where both leadership and collaboration are stressed. Students will acquire depth of knowledge and problem-solving skills through our interdisciplinary, thematic approach to applied learning. Students will excel in Arts, and expand their minds through the use of innovative technologies and our science-based instruction. Through our Socratic method and 100% engagement, students will become effective communicators and leaders empowered to establish a sense of community, service to others, and a love of learning.</p>

            </span>

        </div>

		<div class="container">

		<!--Special Intro-->

		<?php 				

			if(opt('home_special_intro'))

			{

				$count = 1;

				$args = 'post_type=special-intro&posts_per_page=-1';

				$query = new WP_Query();

				$query->query($args);

				

				if($query->have_posts())

				{

				?>

				<div class="agency">

					<div class="special_intro">

						<div class="row">

							<div class="span4">

								<!-- Intro Box title -->

								<div class="titles">

									<ul>

										<?php

										while ($query->have_posts()) { $query->the_post(); 

										?>

											<li><a href="#" <?php if($count==1) echo 'class="selected"';?>><?php the_title(); ?></a></li>

										<?php 

											$count++;

										}

										wp_reset_query();

										?>

									</ul>

									<div class="line">&nbsp;</div>

								</div>

							</div>

							<!-- Intro Box dexcriptions -->

							<div class="span8 descriptions">

								<div>

									<?php

									$query = new WP_Query();

									$query->query($args);

									while ($query->have_posts()) { $query->the_post(); 

									?>

										<div class="description">

										<?php echo SpecialIntroModifyHeader(_get_the_content()); ?>

										</div>

									<?php

									}

									?>

								</div>

							</div>														

						</div>

					</div>

				</div>

				<?php 

				}//End if

				wp_reset_query();

				

			//if home_special_intro

			}

			?>				

			<!--Special Intro End-->

			<?php 

			if(opt('home_tagline'))

			{

				?>

				<!-- Tagline -->

				<div class="row">

					<div class="span12">

					<?php TagLine(opt('tagline_title'), opt('tagline_subtitle'), opt('tagline_button_url'), opt('tagline_button')); ?>

					</div>

				</div>

				<!-- Tagline End -->			

			<?php 

			}

			?>				

		</div>

	</div>

    <!--End Content-->



<?php get_footer(); ?>
