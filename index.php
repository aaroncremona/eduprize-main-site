<?php get_header(); ?>

<?php
if(!is_front_page() && is_home())
{
	$pageId    = get_option('page_for_posts');
    $pageTitle = get_the_title($pageId);
}
else
{
    $pageTitle    = __('Modern Blog', TEXTDOMAIN);
    $pageId = NULL;
}

$style = "";
if (!Intro($pageTitle, $pageId))
{
	$style = "style='margin-top: 0px;'";
}
?>
	<div class="container">
        <div id="main" <?php echo $style; ?>>
			<div class="row">
				<?php 
					$blogClass = 'span8';
					
					if(opt('blog_sidebar_position') == 0)
						$blogClass = 'span12';
					if(opt('blog_sidebar_position') == 1)
						$blogClass .= ' blog_right';

					DisplayCurrentBlogPosts($blogClass, opt('blog_sidebar_position') == 0);
					
					if(opt('blog_sidebar_position') != 0)
						get_sidebar(); 
				?>
			</div>			
		</div>
	</div>	
<?php get_footer(); ?>