<?php
/*
Template Name: Portfolio-2
*/

get_header();

$style = "";
if (!Intro(get_the_title()))
{
	$style = "style='margin-top: 0px;'";
}
?>
	<div class="container">	
		<div id="main" <?php echo $style; ?>>
			<div class="row"> 
			<?php 
				$includeCats = get_post_meta(get_the_ID(), 'portfolio_categories', true);
		    	$ppp = intval(get_post_meta(get_the_ID(), 'portfolio_posts_page', true));
	            Portfolio($includeCats, $ppp, 'gallery2', false, NULL, 'DESC', 'slug', false, false);
			?>
			</div>					
		</div>
	</div>	
<?php get_footer(); ?>
