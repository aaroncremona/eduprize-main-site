<?php
/*
Template Name: About
*/
?>
<?php
	function bg_image()
	{?>
	<div class="page-background">
		<?php 
		if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
			the_post_thumbnail('full'); 
		?>
	</div>
	<?php
	}
	add_action('theme_before_header', 'bg_image');
	
	get_header();
	
	$style = "";
	if (!Intro(get_the_title()))
	{
		$style = "style='margin-top: 0px;'";
	}
?>
	<div class="container">
        <div id="main" <?php echo $style; ?>>
			<?php 
			if (have_posts())
			{  
				while (have_posts()) 
				{ 
					the_post();
					$title = get_post_meta(get_the_ID(), 'about_page_title', true);
					$logos = get_post_meta(get_the_ID(), 'about_page_logos', true);
			?>
			<div class="slogan">
				<?php echo $title; ?>				
			</div>
			<div class="about-content">
				<?php the_content(); ?>				
			</div>
			<?php 
				}
			}
			
			$query = new WP_Query();
			$query->query('post_type=team&posts_per_page=-1');				
			
			if($query->have_posts())
			{
			?>
			<div class="row">
				<?php 
				while ($query->have_posts()) 
				{ 
					$query->the_post();
					DisplayMemberItem(get_the_ID(), 'team-member');
				} 
				?>
			</div>
			<?php 
				wp_reset_query();				
			}//If recent posts

			if (is_array($logos) && count($logos) > 0)
			{
			?>			
			<div class="row">
				<div class="separator"></div>
			</div>								
			<div class="row about_page_carousel">
				<ul id="mycarousel" class="jcarousel">				
				<?php 
					foreach ($logos as $logo)
					{
				?>
					<li>							
						<div class="item">
							<a href="#">
								<img src="<?php echo $logo; ?>" alt="" >
							</a>
						</div>
					</li>
				<?php 		
					}
				?>				
				</ul>
			</div>
			<?php 
			}
			?>
		</div>
	</div>
<?php get_footer(); ?>