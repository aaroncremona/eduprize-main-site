<?php get_header(); ?>

<?php 
	$pageTitle = have_posts() ? sprintf(__("Search Results for '%s'", TEXTDOMAIN), $s) : __('No Results Found', TEXTDOMAIN);

	$style = "";
	if (!Intro($pageTitle, NULL, true))
	{
		$style = "style='margin-top: 0px;'";
	}
?>
	<div class="container">
        <div id="main" <?php echo $style; ?>>
			<div class="row">
				<?php 
					$blogClass = 'span8';
					
					if(opt('blog_sidebar_position') == 0)
						$blogClass = 'span12';
					if(opt('blog_sidebar_position') == 1)
						$blogClass .= ' blog_right';

					DisplayCurrentBlogPosts($blogClass);

					if(opt('blog_sidebar_position') != 0)
						get_sidebar(); 
				?>
			</div>			
		</div>
	</div>	
    
<?php get_footer(); ?>