<?php
/*
Template Name: Portfolio
*/

get_header();
	
$style = "";
if (!Intro(get_the_title()))
{
	$style = "style='margin-top: 0px;'";
}
?>
	<div class="container">	
		<div id="main" <?php echo $style; ?>>
		<?php 
			$includeCats = get_post_meta(get_the_ID(), 'portfolio_categories', true);
		    $ppp = intval(get_post_meta(get_the_ID(), 'portfolio_posts_page', true));
            
		    Portfolio($includeCats, $ppp, 'gallery1', true, NULL, 'DESC', 'slug', true, true);
		?>
		</div>
	</div>
<?php get_footer(); ?>