var $ = jQuery;
var flickrHoverHtml = '<img style="display:none" class="hover_image" height="75" width="75" src="'+theme_uri.img+'/flickr_hover.png" />',
	btnAnimatonSpeedDefault = 300;//Buttons Animation Speed

//(Private) Email Regex
var Reg_Email = /^\w+([\-\.]\w+)*@([a-z0-9]+(\-+[a-z0-9]+)?\.)+[a-z]{2,5}$/i;

$(document).ready(function () {

    /***** IE PNG Transparencity ******/
    
	if($.browser.msie)
	{
		$('.pngfix img').each(function(){
			var $this = $(this);
			$this.css({width: $this.width() + 'px', height: $this.height() + 'px', 'background-color':'transparent'});
			this.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + this.src + "', sizingMethod='crop')";
			this.src = 'images/blank.gif';
		});
	}
    

    /***** Back to top ******/

	$('#top_button').click(function () {
	    $('html, body').animate({ scrollTop: 0 }, 400);
	    return false;
	});

    /***** Add input defaults ******/

	$('input[type="text"]').each(function () {

        var $me = $(this),
	    DefaultVal = $me.attr('placeholder');

        if (typeof DefaultVal == 'undefined')
            return;

	    //Set the default value
	    $me.val(DefaultVal);

	    //Control got focus
	    $me.focus(function () {
	        if ($me.val() == DefaultVal) {
	            $me.val('');
	        }
	    }); //$me.focus

	    //Control lost focus
	    $me.blur(function () {
	        var Value = $.trim($me.val());

	        if (Value.length < 1)
	            $me.val(DefaultVal);
	    }); //$me.blur

	});
	
	/***** Navigation ******/

	$('.navigation > ul').superfish();
	
	/***** Navigation ******/

	$('.navigation > ul').superfish();

    /***** Mobile Navigation ******/

	$(document).click(
        function (e) {
            var $mobileNavBtn = $('.mobile-navigation > a');

            if (e.target != $mobileNavBtn.get(0) && $mobileNavBtn.hasClass('active'))
                $mobileNavBtn.click();
        }
    );

	$('.mobile-navigation > a').click(function (e) {
	    var $this = $(this),
	        $menu = $this.parent().find('> ul');
        
	    if ($this.hasClass('active')) {
	        $menu.slideUp('fast');
	        $this.removeClass('active');
	    }
	    else {
	        $menu.slideDown('fast');
	        $this.addClass('active');
	    }

	    e.preventDefault();
	});

	/***** Blog category ******/
	
	$('.blog_category li').each(function(){
		var $this = $(this),
			$link = $this.find('a'),
		//Get the padding value
		padding_left = $this.css('padding-left');

		if($this.hasClass('current')) return;

		$this.hover(function(){
			$link.stop().animate({'padding-left':'+=' + 21}, 300, function(){$this.addClass('hover');} );
		},
		function(){
			$this.removeClass('hover');
			$link.stop().animate({'padding-left':padding_left}, 300);
		});
	});

	/***** Google Map ******/

	var $contactMap = $('.contact_map');

	if($contactMap.length && typeof gmapSettings != 'undefined')
	{
		$contactMap.gMap(gmapSettings);					
	}

    /***** Blog post image hover ******/
    
    var $blog = $('.blog');

    if($blog.length)
    {
        var $postImg = $blog.find('.post_image');
        $postImg.append('<span class="hover"></span>');

        $postImg.hover(function(){
           fadeIn($(this).find('.hover'), 0.4, 500);
        }, function(){
            fadeOut($(this).find('.hover'), 0, 500);
        });
    }

    /***** Blockquotes & Pullquotes ******/

	$('blockquote,.pullquote,.pullquote_right').each(function () {
		$(this).append('<span class="end"></span>');
	});


    /***** Accordion ******/

	$('.accordion').each(function () {
		var $accordion = $(this),
			$title = $accordion.find('.accordion_title'),
			$titleIcon = $title.find('span'),
			$titleLink = $title.find('a'),
			$content = $accordion.find('.accordion_content');
		
		if ($accordion.hasClass('open')) {
			$titleLink.toggleClass('colored');
			$titleIcon.toggleClass('colored');
			
			if ($titleIcon.text() == '+')
				$titleIcon.text('-');
			else 
				$titleIcon.text('+');
		}
		else if (!$accordion.hasClass('open')) {
			if ($titleIcon.text() == '+')
				$titleIcon.text('-');
			else 
				$titleIcon.text('+');

			$content.slideUp();
		}

		$title.click(function (e) {
			$titleIcon.toggleClass('colored');
			$titleLink.toggleClass('colored');
			
			if ($titleIcon.text() == '+')
				$titleIcon.text('-');
			else 
				$titleIcon.text('+');

			$content.slideToggle();
            e.preventDefault();
		});
	});

    /***** Tabs ******/

	if($.fn.idTabs)
	{
		$('.tab .tab_head').idTabs(function(id,list,set){ 
            $("a",set).removeClass("selected") 
            .filter("[href='"+id+"']",set).addClass("selected"); 
            for(i in list) 
              $(list[i]).hide(); 
            $(id).fadeIn(); 
            return false; 
        });
		$('.tab .tab_head li:last-child').addClass('tab_last');
	}

   /***** Twitter Widget ******/

    function Twitter_Widget() {
       
        $('.widget_hx_twitter_widget').each(function () {
            var $widget = $(this),
                $list   = $widget.find('.twitter_update_list'),
                $items  = $list.find('li'),
                $next   = $widget.find('.arrow_next'),
                $prev   = $widget.find('.arrow_previous');

            if (!$widget.length || !$items.length)
                return;

            $list.css({ position: 'relative', overflow: 'hidden' });

            $items.each(function (i) {
                var $item = $(this);

                $item.css({ position: 'absolute', width: $item.parent().width(), top: 0, left: i * $item.parent().width() });
            });

            $items.eq(0).addClass('current');
            $list.height($items.eq(0).height());

            //Arrow keys
            $next.click(function (e) {
                e.preventDefault();

                var curIndx = $items.filter('.current').index(),
                    nextIndx = curIndx + 1;

                if (nextIndx >= $items.length)
                    nextIndx = 0;


                GoTo(nextIndx);
            });

            $prev.click(function (e) {
                e.preventDefault();

                var curIndx = $items.filter('.current').index(),
                    nextIndx = curIndx - 1;

                if (nextIndx < 0)
                    nextIndx = $items.length - 1;


                GoTo(nextIndx);
            });

            function GoTo(indx) {
                var width = $list.width(),
                    $nextItem = $items.eq(indx);

                $items.removeClass('current');
                $nextItem.addClass('current');

                $list.stop().animate({ height: $nextItem.height() }, { speed: 300 });

                $items.each(function (i) {
                    var $item = $(this);

                    $item.stop().animate({ left: (i - indx) * width }, { speed: 300 });
                });

            }

            //Handle resize event
            var resizeTimerId = 0;

            $(window).resize(function () {
                clearTimeout(resizeTimerId);

                resizeTimerId = setTimeout(function () {

                    var $curItem = $items.filter('.current'),
                        curIndx = $curItem.index();

                    $items.each(function (i) {
                        var $item = $(this);

                        $item.css({ width: $item.parent().width(), left: (i - curIndx) * $item.parent().width() });
                    });

                    //Change parent height
                    $list.stop().animate({ height: $curItem.height() }, { speed: 300 });
                }, 100);
            });
        });//End loop


    }
	
	/***** Flickr Widget ******/

	if($('.flickr_widget').length)
	{
		$('.flickr_widget .flickr_container').each(function(){
			var $this = $(this);

			//Detect loading of images
			var intID = setInterval(function()
			{
				var $links = $this.find('.flickr_badge_image a');

				if($links.length < 1)
					return;
				
				//Add hover image to each anchor tag
				$links.append(flickrHoverHtml)
				.hover(
					function(){
                       fadeIn($(this).find('img').eq(1), 1, 300);
					},
					function(){
                       fadeOut($(this).find('img').eq(1), 0, 300);
					}
				);
				

				//Clear the timer event
				clearInterval(intID);
			}, 1000);
		});

	}

	/***** Comment & Contact Forms ******/

	var $respond = $('#respond'), $respondWrap = $('#respond_wrap'), $cancelCommentReply = $respond.find('.cancel_reply'),
		$commentParent  = $respond.find('input[name="parent"]');

	$('.comment_reply_link').each(function() {
		var $this = $(this),
			$parent = $this.parent().parent();
		
		$this.click(function() {
			var commId = $parent.find('.comment_id').html();

			$commentParent.val(commId);
			$respond.insertAfter($parent);
			$cancelCommentReply.show();
			
			return false;
		});
	});

	$cancelCommentReply.click(function(e) {
		$cancelCommentReply.hide();
		
		$respond.appendTo($respondWrap);
		$commentParent.val(0);
		
		e.preventDefault();
	});

	//InitContactForm('#respond');


	/***** Buttons ******/

	$('.button').each(function(){
		var speed = btnAnimatonSpeedDefault,
			$imgs = $(this).find('img');

		if(typeof buttonAnimationSpeed != 'undefined')
		   speed = parseInt(buttonAnimationSpeed);

		if($imgs.length < 2) return;

		$(this).hover(function(){
                       fadeOut($imgs.eq(0), 0, speed);
                       fadeIn($imgs.eq(1), 1, speed);
					},
					function(){
                       fadeOut($imgs.eq(1), 0, speed);
                       fadeIn($imgs.eq(0), 1, speed);
					});
	});
	
	/** Pretty Photo **/
	/* Adding Rel attrebute */
	$('a[data-rel]').each(function() {
		$(this).attr('rel', $(this).data('rel'));
	});
	
	if($.fn.prettyPhoto)
		$("a[rel^='prettyPhoto']").prettyPhoto();
		
	
	//Calling Functions in Document Ready	
	IntroBox();
	Portfolio();
	Flexslider();
	PortfolioSingle();
	AboutCarousel();
	
	Twitter_Widget();
});
//End $(document).ready

	//Portfolio
    function Portfolio() {
        var $container = $('.isotope');

        if ($container.length < 1)
            return;

        $container.isotope({
            // options
            itemSelector: '.item',
            layoutMode: 'masonry',
            animationEngine: 'best-available'
        });


        // filter items when filter link is clicked
        $('.subnavigation li a').click(function (e) {
            e.preventDefault();
            var $this = $(this);

            if ($this.parent().hasClass('current'))
                return;
            
            var $optionSet = $this.parents('.subnavigation');

            $optionSet.find('.current').removeClass('current');
            $this.parent().addClass('current');
            var selector = $(this).attr('data-filter');
            $container.isotope({ filter: selector });
        });
		
		//item hover
		var $gallery = $('.gallery .item_image');

		if($gallery.length)
		{
			$gallery.each(function () {
				var $this   = $(this),
					$hover = $this.find('.frame_hover'),
					 btm = '6px',
					 lft = '0';

				$hover.css({ bottom: -$hover.height(), left: -$hover.width(), display: 'block' });

				$this.hover(function () {

					$hover.stop().animate({ bottom: btm, left: lft }, 300);
				
				},function(){

					$hover.stop().animate({ bottom: -$hover.height(), left: -$hover.width() }, 300);
				});
			});
		}
	}
	
	// Special Intro
	function IntroBox() {
        $(".special_intro .titles a").click(function (e) {
            e.preventDefault();

            // Set variables and index of selected item.
            var $this = $(this);
            var index = $this.parent().index();

            // Set selected item as selected.
            $(".special_intro .titles a").removeClass("selected");
            $this.addClass("selected");

            // Calculate the height of the negative margin needed to show desired text.
            var top = (index * $(".agency .special_intro .descriptions").height());
            $(".agency .special_intro .descriptions > div").animate({ marginTop: -(top) }, 250);

            // Calculate position of arrow to match selected item.
            //top = 18 + (index * ($this.height() + 20));

            //$(".agency .special_intro .line").animate({ backgroundPosition: "0 " + top + "px" }, 400);
        });
    }
	
	// Flexslider
	function Flexslider() {
	
		flexSettings = {
			slideshowSpeed: 5000,
			animationSpeed: 2000,
			controlNav: false
		};
			
        if ($.fn.flexslider) {
			$('.flexslider').flexslider(flexSettings);
		}
	}
	
	//Portfolio Detail
	function PortfolioSingle() {

        var jctimer,  $mycarousel = $('#mycarousel');

        //if ($mycarousel.children().length < 2)
            //return;

        $(window).resize(function () {
            clearTimeout(jctimer);
            jctimer = setTimeout(carouselSetSize, 100);
        }).resize();

        function carouselSetSize() {

            var width = $(window).width(),
                settings = {
                    scroll: 1, visible: 1, animation: 300,
                    easing: 'easeOutCubic',//Wrap mode has bug
                    auto: 0
                };

            // Configuration for screen Size with less than 480px
            if (width < 480) {

            }
            // Configuration for screen Size with less than 768px							
            else if (width < 768) {
                settings.visible = 2;
            }
                // Configuration for screen Size with less than 1024px							
            else if (width < 1024) {
                settings.visible = 3;
            }
            else {
                settings.visible = 4;
                settings.scroll = 4;
            }
			
			if ($.fn.jcarousel) {
				$mycarousel.jcarousel(settings);
			}
        }

    }

	//
	function AboutCarousel() {

        var jctimer,  $mycarousel = $('#mycarousel_1');

        //if ($mycarousel.children().length < 2)
            //return;

        $(window).resize(function () {
            clearTimeout(jctimer);
            jctimer = setTimeout(carouselSetSize, 100);
        }).resize();

        function carouselSetSize() {

            var width = $(window).width(),
                settings = {
                    scroll: 1, visible: 1, animation: 300,
                    easing: 'easeOutCubic',//Wrap mode has bug
                    auto: 0
                };

            // Configuration for screen Size with less than 480px
            if (width < 480) {

            }
            // Configuration for screen Size with less than 768px							
            else if (width < 768) {
                settings.visible = 2;
            }
                // Configuration for screen Size with less than 1024px							
            else if (width < 1024) {
                settings.visible = 3;
            }
            else {
                settings.visible = 4;
                settings.scroll = 4;
            }
			
			if ($.fn.jcarousel) {
				$mycarousel.jcarousel(settings);
			}
        }

    }

    /*** user-added ***/

    // awards
    $('#awards .name').hover(
        function() {
            $('.awards-intro, .award-text').hide();
            var index = ($(this).index() / 2) + 1;
            console.log(index);
            $('#awards-text .award-text:nth-child('+index+')').fadeIn('fast');
        },
        function() {}
    );
	

// Custom sorting plugin
(function($) {
  $.fn.sorted = function(customOptions) {
    var options = {
      reversed: false,
      by: function(a) { return a.text(); }
    };
    $.extend(options, customOptions);
    $data = $(this);
    arr = $data.get();
    arr.sort(function(a, b) {
      var valA = options.by($(a));
      var valB = options.by($(b));
      if (options.reversed) {
        return (valA < valB) ? 1 : (valA > valB) ? -1 : 0;				
      } else {		
        return (valA < valB) ? -1 : (valA > valB) ? 1 : 0;	
      }
    });
    return $(arr);
  };
})(jQuery);

/* Other Functions */

function GetSliderSettingsByKey(key)
{
    var opt = {}, setting = {};
    
    if (typeof sliderSettings != 'undefined')
        setting = FindSettingsByKey(sliderSettings, key);

	//Copy slider defaults to opt
	$.extend(opt, sliderDefaults);
	//Expand settings 
	setting = $.extend(opt, setting);

    return setting;
}

/* Returns object in the settings array that is matched with given key
   Object must contain key property
*/
function FindSettingsByKey(settings, key){
	
	for(i=0; i<settings.length; i++){
		if(typeof settings[i].key === undefined) continue;

		if(settings[i].key == key)
		   return settings[i];
	}

	return {};
}

//Check if the given value is integer
function is_int(value){
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
	  return true;
  } else {
	  return false;
  }
}

function fadeTo($element, opacity, time){
    $element.css({display: 'block'}).stop().animate({opacity:opacity}, time);
}

function fadeIn($element, opacity, time){
    $element.css({opacity:0, display: 'block'}).stop().animate({opacity:opacity}, time);
}

function fadeOut($element, opacity, time){
    $element.stop().animate({opacity:opacity}, time);
}
